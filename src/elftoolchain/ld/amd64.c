/*-
 * Copyright (c) 2012 Kai Wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "ld.h"
#include "ld_arch.h"
#include "ld_input.h"
#include "ld_reloc.h"
#include "amd64.h"

ELFTC_VCSID("$Id: amd64.c 2391 2012-01-31 23:03:32Z kaiwang27 $");

static uint64_t _get_max_page_size(struct ld *ld);
static uint64_t _get_common_page_size(struct ld *ld);
static void _process_reloc(struct ld *ld, struct ld_input_section *is);

static char _amd64_name1[] = "amd64";
static char _amd64_name2[] = "x86-64";

static struct ld_arch amd64 = {
	.get_max_page_size = _get_max_page_size,
	.get_common_page_size = _get_common_page_size,
	.process_reloc = _process_reloc,
};

static uint64_t
_get_max_page_size(struct ld *ld)
{

	(void) ld;
	return (0x100000);
}

static uint64_t
_get_common_page_size(struct ld *ld)
{

	(void) ld;
	return (0x1000);
}

static void
_process_reloc(struct ld *ld, struct ld_input_section *is)
{
	struct ld_reloc_entry *lre;

	assert(is->is_type == SHT_REL || is->is_type == SHT_RELA);
	assert(is->is_reloc != NULL);

	(void) ld;
	STAILQ_FOREACH(lre, is->is_reloc, lre_next) {
		printf("process reloc: %#jx", (uintmax_t) lre->lre_type);
	}
}

void
amd64_register(struct ld *ld)
{

	HASH_ADD_KEYPTR(hh, ld->ld_arch, _amd64_name1, strlen(_amd64_name1),
	    &amd64);
	HASH_ADD_KEYPTR(hh, ld->ld_arch, _amd64_name2, strlen(_amd64_name2),
	    &amd64);
}
