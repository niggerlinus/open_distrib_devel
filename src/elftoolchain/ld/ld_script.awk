# $Id: ld_script.awk 2211 2011-11-26 01:23:58Z kaiwang27 $

BEGIN {
    printf "const char *ldscript_default = ";
}

{
    printf "\"";
    gsub("\"", "\\\"");
    printf "%s\\n\"\n", $0;
}

END {
    print ";";
}
