# $Id: os.DragonFly.mk 2237 2011-11-28 03:49:43Z jkoshy $
#
# Build definitions for DragonFly

MKTESTS?=	yes	# Enable the test suites.
MKDOC?=		yes	# Build documentation.

NOSHARED=	yes	# Link programs statically by default.
