--- strtol.3.bsdnew	2009-11-13 14:11:51.000000000 -0800
+++ strtol.3	2009-11-13 14:33:25.000000000 -0800
@@ -36,7 +36,10 @@
 .Dt STRTOL 3
 .Os
 .Sh NAME
-.Nm strtol , strtoll , strtoimax , strtoq
+.Nm strtoimax ,
+.Nm strtol ,
+.Nm strtoll ,
+.Nm strtoq
 .Nd "convert a string value to a"
 .Vt long , "long long" , intmax_t
 or
@@ -45,26 +48,41 @@ integer
 .Sh LIBRARY
 .Lb libc
 .Sh SYNOPSIS
+.In inttypes.h
+.Ft intmax_t
+.Fo strtoimax
+.Fa "const char *restrict str"
+.Fa "char **restrict endptr"
+.Fa "int base"
+.Fc
 .In stdlib.h
-.In limits.h
 .Ft long
-.Fn strtol "const char * restrict nptr" "char ** restrict endptr" "int base"
+.Fo strtol
+.Fa "const char *restrict str"
+.Fa "char **restrict endptr"
+.Fa "int base"
+.Fc
 .Ft long long
-.Fn strtoll "const char * restrict nptr" "char ** restrict endptr" "int base"
-.In inttypes.h
-.Ft intmax_t
-.Fn strtoimax "const char * restrict nptr" "char ** restrict endptr" "int base"
+.Fo strtoll
+.Fa "const char *restrict str"
+.Fa "char **restrict endptr"
+.Fa "int base"
+.Fc
 .In sys/types.h
 .In stdlib.h
 .In limits.h
 .Ft quad_t
-.Fn strtoq "const char *nptr" "char **endptr" "int base"
+.Fo strtoq
+.Fa "const char *str"
+.Fa "char **endptr"
+.Fa "int base"
+.Fc
 .Sh DESCRIPTION
 The
 .Fn strtol
 function
 converts the string in
-.Fa nptr
+.Fa str
 to a
 .Vt long
 value.
@@ -72,7 +90,7 @@ The
 .Fn strtoll
 function
 converts the string in
-.Fa nptr
+.Fa str
 to a
 .Vt "long long"
 value.
@@ -80,7 +98,7 @@ The
 .Fn strtoimax
 function
 converts the string in
-.Fa nptr
+.Fa str
 to an
 .Vt intmax_t
 value.
@@ -88,7 +106,7 @@ The
 .Fn strtoq
 function
 converts the string in
-.Fa nptr
+.Fa str
 to a
 .Vt quad_t
 value.
@@ -143,11 +161,11 @@ stores the address of the first invalid 
 If there were no digits at all, however,
 .Fn strtol
 stores the original value of
-.Fa nptr
+.Fa str
 in
 .Fa *endptr .
 (Thus, if
-.Fa *nptr
+.Fa *str
 is not
 .Ql \e0
 but
@@ -155,11 +173,17 @@ but
 is
 .Ql \e0
 on return, the entire string was valid.)
+.Pp
+Extended locale versions of these functions are documented in
+.Xr strtol_l 3 .
+See
+.Xr xlocale 3
+for more information.
 .Sh RETURN VALUES
 The
 .Fn strtol ,
 .Fn strtoll ,
-.Fn strtoimax
+.Fn strtoimax ,
 and
 .Fn strtoq
 functions
@@ -177,7 +201,7 @@ is set to
 .Er ERANGE
 and the function return value is clamped according
 to the following table.
-.Bl -column -offset indent ".Fn strtoimax" ".Sy underflow" ".Sy overflow"
+.Bl -column -offset indent ".Fn strtoimax" ".Dv INTMAX_MIN" ".Dv INTMAX_MAX"
 .It Sy Function Ta Sy underflow Ta Sy overflow
 .It Fn strtol Ta Dv LONG_MIN Ta Dv LONG_MAX
 .It Fn strtoll Ta Dv LLONG_MIN Ta Dv LLONG_MAX
@@ -195,14 +219,25 @@ no conversion could be performed
 .It Bq Er ERANGE
 The given string was out of range; the value converted has been clamped.
 .El
+.Sh LEGACY SYNOPSIS
+.Fd #include <stdlib.h>
+.Fd #include <limits.h>
+.Pp
+.In limits.h
+is necessary for the
+.Fn strtol
+and
+.Fn strtoll
+functions.
 .Sh SEE ALSO
 .Xr atof 3 ,
 .Xr atoi 3 ,
 .Xr atol 3 ,
 .Xr strtod 3 ,
-.Xr strtonum 3 ,
+.Xr strtol_l 3 ,
 .Xr strtoul 3 ,
-.Xr wcstol 3
+.Xr wcstol 3 ,
+.Xr compat 5
 .Sh STANDARDS
 The
 .Fn strtol
