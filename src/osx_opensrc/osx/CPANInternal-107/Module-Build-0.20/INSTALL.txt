         Installation instructions for Module::Build

To install this module, just do:

  perl Build.PL
  ./Build test
  ./Build install  (this step may need to be done as the superuser)

For other operating systems that don't like the "./" syntax, like
Windows-based systems, you can do:

  perl Build.PL
  perl Build test
  perl Build install

The important thing is that the "Build" script gets executed and that
you pass it the "test", "install", etc. arguments.

If you really want to, you can use a more traditional Makefile.PL:

  perl Makefile.PL
  make test
  make install     (this step may need to be done as the superuser)

Substitute "nmake" or "gmake" for "make" if you prefer using some
other make-alike on your platform.

I recommend using the Build.PL option.  If you use the Makefile.PL
option, you'll actually be using the Build.PL option under the surface
anyway, with a pass-through Makefile.

There's heaps more information in the README and in the documentation
of the various packages in this distribution.

 -Ken
