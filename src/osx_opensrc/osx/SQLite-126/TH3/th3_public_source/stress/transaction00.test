/*
** Test for transaction processing, especially rollback behavior.
**
** MODULE_NAME:               transaction00
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

int transaction00(th3state *p){
  int i;
  int N = 25;

#if !defined(SQLITE_TEMP_STORE) || SQLITE_TEMP_STORE>=2
  if( p->config.maskProp & TH3_ALT_PCACHE ) return 0;
#endif

  /* Prepare a large database with random content */
  th3testBegin(p, "1.1");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, 
    "BEGIN;"
    "CREATE TABLE t1(x TEXT);"
    "INSERT INTO t1 VALUES(th3randomblob(abs(th3random())%400+10));"
    "INSERT INTO t1 VALUES(th3randomblob(abs(th3random())%400+10));"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "INSERT INTO t1 SELECT th3randomblob(abs(th3random())%400+10) FROM t1;"
    "COMMIT;"
    "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "1024");

  for(i=1; i<=N; i++){
    char zHash[200];    /* The database content signature */

    /* Compute and remember the signature on the database content */    
    th3testResetResult(p);
    th3dbEval(p, 0, "SELECT count(*), md5sum(x) FROM t1");
    assert( strlen(p->zResult)<sizeof(zHash) );
    th3strcpy(zHash, p->zResult);
    th3testResetResult(p);

    /* Make some changes to the database.  Roll those changes back.
    ** verify that the database is not corrupted. 
    */
    th3testBegin(p, th3format(p, "2.%d.1", i));
    th3dbEval(p, 0,
      "BEGIN;"
      "DELETE FROM t1 WHERE th3random()%10!=0;"
      "INSERT INTO t1 SELECT th3randomblob(10)||x FROM t1;"
      "INSERT INTO t1 SELECT th3randomblob(10)||x FROM t1;"
      "ROLLBACK;"
      "PRAGMA integrity_check;"
    );
    th3testCheck(p, "ok");

    /* Verify that the database content signature is unchanged */
    th3testBegin(p, th3format(p, "2.%d.2", i));
    th3dbEval(p, 0, "SELECT count(*), md5sum(x) FROM t1");
    th3testCheck(p, zHash);

    /* Do another round of random changes and roll them back.
    ** Check database integrity. */
    th3testBegin(p, th3format(p, "2.%d.3", i));
    th3dbEval(p, 0, "SELECT count(*), md5sum(x) FROM t1");
    assert( strlen(p->zResult)<sizeof(zHash) );
    th3strcpy(zHash, p->zResult);
    th3testResetResult(p);
    th3dbEval(p, 0,
      "BEGIN;"
      "DELETE FROM t1 WHERE th3random()%10!=0;"
      "INSERT INTO t1 SELECT th3randomblob(10)||x FROM t1;"
      "DELETE FROM t1 WHERE th3random()%10!=0;"
      "INSERT INTO t1 SELECT th3randomblob(10)||x FROM t1;"
      "ROLLBACK;"
      "PRAGMA integrity_check;"
    );
    th3testCheck(p, "ok");

    /* Again, verify that the database signature is unchanged. */
    th3testBegin(p, th3format(p, "2.%d.4", i));
    th3dbEval(p, 0, "SELECT count(*), md5sum(x) FROM t1");
    th3testCheck(p, zHash);

    if( i==N-1 ) break;

    /* Add more content to the database */
    th3testBegin(p, th3format(p, "2.%d.9", i));
    th3dbEval(p, 0, "INSERT INTO t1 "
      "SELECT th3randomblob(abs(th3random())%400+10) FROM t1 "
      " WHERE random()%10==0;"
      "PRAGMA integrity_check;"
    );
    th3testCheck(p, "ok");
  }         
  return 0;
}
