/*
** This module contains white-box corrupt database tests.
**
** This module arranges to have have cells show content flows off the
** end of a page.
**
** SCRIPT_MODULE_NAME:        corrupt05
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/

/* Create a database to work with */
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
INSERT INTO t1 VALUES(1, x'112233445566778899AA');
--result

/* Verify that the database looks like we think it does */
--testcase 110
SELECT rootpage FROM sqlite_master WHERE name='t1';
--store :t1root
PRAGMA page_size;
--store :pgsz
SELECT :t1root*:pgsz - 15;
--store :ofst
seek :ofst read 15 store :res
--edit test.db
SELECT :res;
--result 0d01030020112233445566778899aa
--testcase 111
SELECT x FROM t1;
--result 1

/* Corrupt the header so that the hdr size is negative number */
--testcase 120
seek :ofst write 0xd01030022
seek 24 add32 1
--edit test.db
--testcase 121
SELECT x FROM t1;
--result 1
--testcase 122
SELECT length(y) FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 130
seek :ofst write 0xd0103011e
seek 24 add32 1
--edit test.db
--testcase 131
SELECT x FROM t1;
--result 1
--testcase 132
SELECT length(y) FROM t1;
--result 9
--testcase 133
SELECT hex(y) FROM t1;
--result 2233445566778899AA

--testcase 140
seek :ofst write 0d018004001e
seek 24 add32 1
--edit test.db
--testcase 141
SELECT x FROM t1;
--result 1
--testcase 142
SELECT length(y) FROM t1;
--result 9
--testcase 143
SELECT hex(y) FROM t1;
--result 2233445566778899AA

--testcase 150
seek :ofst write 0d02040010
seek 24 add32 1
--edit test.db
--testcase 151
SELECT x FROM t1;
--result 2
--testcase 152
SELECT length(y) FROM t1;
--result 2
--testcase 153
SELECT hex(y) FROM t1;
--result 2233

--testcase 160
seek :ofst write 0e02040010
seek 24 add32 1
--edit test.db
#if defined(SQLITE_ENABLE_OVERSIZE_CELL_CHECK)
--testcase 161
SELECT x FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}
--testcase 162
SELECT length(y) FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}
--testcase 163
SELECT hex(y) FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}
#endif
--testcase 164
PRAGMA integrity_check;
--store :res
SELECT :res!='ok'
--result 1
