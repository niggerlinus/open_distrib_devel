/*
** Out-of-memory test module for th3.
**
** SCRIPT_MODULE_NAME:        oom130
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 10000
--oom
CREATE TABLE t1(w int, x int, y int, z int);
--result 

--testcase 10010
CREATE UNIQUE INDEX i1w ON t1(w);
--result 

--testcase 10020
CREATE INDEX i1xy ON t1(x,y);
--result 

--testcase 10030
CREATE INDEX i1zyx ON t1(z,y,x);
--result 

--testcase 10040
SELECT * FROM t1 WHERE w=85 AND x=6 AND y=7396;
--result 

--testcase 10050
SELECT * FROM t1 WHERE w=85 AND x=6 AND y=7396 AND rowid=85;
--result 

--testcase 10060
SELECT * FROM t1 WHERE w=85 ORDER BY random();
--result 

--testcase 10070
SELECT * FROM t1 WHERE x=6 AND y=7396 ORDER BY random();
--result 

--testcase 10080
SELECT * FROM t1 WHERE rowid=85 AND x=6 AND y=7396 ORDER BY random();
--result 

--testcase 10090
SELECT * FROM t1 ORDER BY rowid LIMIT 2;
--result 

--testcase 10100
SELECT * FROM t1 ORDER BY rowid DESC LIMIT 2;
--result 

--testcase 10110
SELECT * FROM t1 WHERE z IN (10207,10006) AND y IN (10000,10201) AND x>0 AND x<10 ORDER BY w;
--result 

--testcase 10120
SELECT * FROM t1 WHERE z IN (10207,10006) AND y=10000 AND x>0 AND x<10 ORDER BY w;
--result 

--testcase 10130
SELECT * FROM t1 WHERE z=10006 AND y IN (10000,10201) AND x>0 AND x<10 ORDER BY w;
--result 

--testcase 10140
SELECT * FROM t1 WHERE z IN (SELECT 10207 UNION SELECT 10006) AND y IN (10000,10201) AND x>0 AND x<10 ORDER BY w;
--result 

--testcase 10150
SELECT * FROM t1 WHERE z IN (SELECT 10207 UNION SELECT 10006) AND y IN (SELECT 10000 UNION SELECT 10201) AND x>0 AND x<10 ORDER BY w;
--result 

--testcase 10160
SELECT * FROM t1 WHERE x IN (1,2,3,4,5,6,7,8) AND y IN (10000,10001,10002,10003,10004,10005) ORDER BY 2;
--result 

--testcase 10170
SELECT * FROM t1 WHERE z IN (10207,10006,10006,10207) ORDER BY w;
--result 

--testcase 10180
SELECT * FROM t1 WHERE z IN ( SELECT 10207 UNION ALL SELECT 10006 UNION ALL SELECT 10006 UNION ALL SELECT 10207) ORDER BY w;
--result 

--testcase 10190
SELECT * FROM t1 WHERE w=99 ORDER BY w;
--result 

--testcase 10200
SELECT * FROM t1 WHERE w IN (99) ORDER BY w;
--result 

--testcase 10210
SELECT * FROM t1 WHERE w=99 OR w=100 ORDER BY +w;
--result 

--testcase 10220
SELECT * FROM t1 WHERE 99=w OR 100=w ORDER BY +w;
--result 

--testcase 10230
SELECT * FROM t1 WHERE w=99 OR w=100 OR 6=w ORDER BY +w;
--result 

--testcase 10240
SELECT * FROM t1 WHERE w=99 OR w=100 OR 6=+w ORDER BY +w;
--result 

--testcase 10250
SELECT * FROM t1 WHERE w=99 OR +w=100 OR 6=w ORDER BY +w;
--result 

--testcase 10260
SELECT b.* FROM t1 a, t1 b WHERE a.w=1 AND (a.y=b.z OR b.z=10) ORDER BY +b.w;
--result 

--testcase 10270
SELECT b.* FROM t1 a, t1 b WHERE a.w=1 AND (b.z=10 OR a.y=b.z OR b.z=10) ORDER BY +b.w;
--result 

--testcase 10280
CREATE TABLE t2249a(a TEXT UNIQUE);
--result 

--testcase 10290
CREATE TABLE t2249b(b INTEGER);
--result 

--testcase 10300
INSERT INTO t2249a VALUES('0123');
--result 

--testcase 10310
INSERT INTO t2249b VALUES(123);
--result 

--testcase 10320
-- Because a is type TEXT and b is type INTEGER, both a and b -- will attempt to convert to NUMERIC before the comparison. -- They will thus compare equal. -- SELECT * FROM t2249b CROSS JOIN t2249a WHERE a=b;
--result 

--testcase 10330
-- The + operator removes affinity from the rhs. No conversions -- occur and the comparison is false. The result is an empty set. -- SELECT * FROM t2249b CROSS JOIN t2249a WHERE a=+b;
--result 

--testcase 10340
SELECT * FROM t2249b CROSS JOIN t2249a WHERE +b=a;
--result 

--testcase 10350
-- Use + on both sides of the comparison to disable indices -- completely. Make sure we get the same result. -- SELECT * FROM t2249b CROSS JOIN t2249a WHERE +a=+b;
--result 

--testcase 10360
SELECT * FROM t2249b CROSS JOIN t2249a WHERE a=b OR a='hello';
--result 123 0123

--testcase 10370
SELECT * FROM t2249b CROSS JOIN t2249a WHERE b=a OR a='hello';
--result 123 0123

--testcase 10380
SELECT * FROM t2249b CROSS JOIN t2249a WHERE 'hello'=a OR b=a;
--result 123 0123

--testcase 10390
SELECT * FROM t2249b CROSS JOIN t2249a WHERE a='hello' OR b=a;
--result 123 0123

--testcase 10400
SELECT * FROM t2249b CROSS JOIN t2249a WHERE a=+b OR a='hello';
--result 

--testcase 10410
SELECT * FROM t2249b CROSS JOIN t2249a WHERE a='hello' OR +b=a;
--result 

--testcase 10420
SELECT * FROM t2249b CROSS JOIN t2249a WHERE +b=a OR a='hello';
--result 

--testcase 10430
SELECT * FROM t2249b CROSS JOIN t2249a WHERE a=+b OR +a='hello';
--result 

--testcase 10440
SELECT * FROM t2249a x CROSS JOIN t2249a y WHERE x.a=y.a;
--result 0123 0123

--testcase 10450
SELECT * FROM t2249a x CROSS JOIN t2249a y WHERE x.a=y.a OR y.a='hello';
--result 0123 0123

--testcase 10460
SELECT * FROM t2249a x CROSS JOIN t2249a y WHERE y.a=x.a OR y.a='hello';
--result 0123 0123

--testcase 10470
SELECT * FROM t2249a x CROSS JOIN t2249a y WHERE y.a='hello' OR x.a=y.a;
--result 0123 0123

--testcase 10480
create table t8(a unique, b, c);
--result 

--testcase 10490
insert into t8 values(1,2,3);
--result 

--testcase 10500
insert into t8 values(2,3,4);
--result 

--testcase 10510
create table t9(x,y);
--result 

--testcase 10520
insert into t9 values(2,4);
--result 

--testcase 10530
insert into t9 values(2,3);
--result 

--testcase 10540
select y from t8, t9 where a=1 order by a, y;
--result 3 4

--testcase 10550
select * from t8 where a=1 order by b, c;
--result 1 2 3

--testcase 10560
select * from t8, t9 where a=1 and y=3 order by b, x;
--result 1 2 3 2 3

--testcase 10570
create unique index i9y on t9(y);
--result 

--testcase 10580
select * from t8, t9 where a=1 and y=3 order by b, x;
--result 1 2 3 2 3

--testcase 10590
SELECT * FROM t1 WHERE x IN (20,21) AND y IN (1,2);
--result 

--testcase 10600
SELECT * FROM t1 WHERE x IN (1,2) AND y IN (-5,-6);
--result 

--testcase 10610
CREATE TABLE tx AS SELECT * FROM t1;
--result 

--testcase 10620
SELECT w FROM t1 WHERE x IN (SELECT x FROM tx WHERE rowid<0) AND +y IN (SELECT y FROM tx WHERE rowid=1);
--result 

--testcase 10630
SELECT w FROM t1 WHERE x IN (SELECT x FROM tx WHERE rowid=1) AND y IN (SELECT y FROM tx WHERE rowid<0);
--result 

--testcase 10640
CREATE INDEX tx_xyz ON tx(x, y, z, w);
--result 

--testcase 10650
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 12 AND 14);
--result 

--testcase 10660
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 12 AND 14) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10670
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 12 AND 14) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10680
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10690
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 2 AND 4);
--result 

--testcase 10700
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 2 AND 4) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10710
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 2 AND 4) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10720
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN -4 AND -2);
--result 

--testcase 10730
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN -4 AND -2) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10740
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN -4 AND -2) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10750
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 200 AND 300);
--result 

--testcase 10760
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 200 AND 300) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10770
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE w BETWEEN 200 AND 300) AND y IN (SELECT y FROM t1 WHERE w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE w BETWEEN 10 AND 20);
--result 

--testcase 10780
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE +w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE +w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE +w BETWEEN 200 AND 300);
--result 

--testcase 10790
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE +w BETWEEN 10 AND 20) AND y IN (SELECT y FROM t1 WHERE +w BETWEEN 200 AND 300) AND z IN (SELECT z FROM t1 WHERE +w BETWEEN 10 AND 20);
--result 

--testcase 10800
SELECT w FROM tx WHERE x IN (SELECT x FROM t1 WHERE +w BETWEEN 200 AND 300) AND y IN (SELECT y FROM t1 WHERE +w BETWEEN 10 AND 20) AND z IN (SELECT z FROM t1 WHERE +w BETWEEN 10 AND 20);
--result 

--testcase 10810
CREATE TABLE t10(a,b,c);
--result 

--testcase 10820
INSERT INTO t10 VALUES(1,1,1);
--result 

--testcase 10830
INSERT INTO t10 VALUES(1,2,2);
--result 

--testcase 10840
INSERT INTO t10 VALUES(1,3,3);
--result 

--testcase 10850
INSERT INTO t10 VALUES(1,$i,$i);
--result 

--testcase 10860
CREATE INDEX i10 ON t10(a,b);
--result 

--testcase 10870
SELECT count(*) FROM t10;
--result 4

--testcase 10880
SELECT * FROM t10 WHERE a=1 AND (b=2 OR b=3);
--result 1 2 2 1 3 3
