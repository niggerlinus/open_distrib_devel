#!/bin/sh

# Build the th3.c code for cov1/* tests only.
#
cmd="tclsh mkth3.tcl cfg/*.cfg cov1/*.test"
echo $cmd '>th3.c'
$cmd >th3.c

# Compute test coverage
#
rm *.gcno *.gcda
cmd='gcc -g -o th3-cov -fprofile-arcs -ftest-coverage -DSQLITE_COVERAGE_TEST=1 -DSQLITE_NO_SYNC=1 th3.c sqlite3.c -ldl -lpthread'
echo $cmd
$cmd

cmd="./th3-cov"
echo $cmd
$cmd >th3-cov.txt
tail -40 th3-cov.txt
cmd="gcov -b -c sqlite3.c"
echo $cmd
$cmd
tclsh misc/cov-filter.tcl
cp sqlite3.c.cov sqlite3.c.cov-all
e sqlite3.c.cov-all &
tclsh misc/filter-gcov.tcl | tee cov.txt

# Do a run with SQLITE_DEBUG enabled to look for assertion faults.
#
cmd='gcc -g -o th3-dbg -DSQLITE_DEBUG=1 -DSQLITE_NO_SYNC=1 th3.c sqlite3.c -ldl -lpthread'
echo $cmd
$cmd

cmd="./th3-dbg"
echo $cmd '>th3-dbg.txt'
$cmd >th3-dbg.txt
tail -40 th3-dbg.txt

# Do an optimizied run.
#
cmd='gcc -O3 -o th3-fast -DSQLITE_NO_SYNC=1 th3.c sqlite3.c -ldl -lpthread'
echo $cmd
$cmd

cmd="./th3-fast"
echo $cmd '>th3-fast.txt'
$cmd >th3-fast.txt
tail -40 th3-fast.txt

# Build the th3.c code for tests not in cov1/*
#
cmd="tclsh mkth3.tcl cfg/*.cfg dev/*.test stress/*.test"
echo $cmd '>th3b.c'
$cmd >th3b.c

# Do a run with SQLITE_DEBUG enabled to look for assertion faults.
#
cmd='gcc -g -o th3b-dbg -DSQLITE_DEBUG=1 -DSQLITE_ENABLE_OVERSIZE_CELL_CHECK -DSQLITE_NO_SYNC=1 th3b.c sqlite3.c -ldl -lpthread'
echo $cmd
$cmd

cmd="./th3b-dbg"
echo $cmd '>th3b-dbg.txt'
$cmd >th3b-dbg.txt
tail -40 th3b-dbg.txt
