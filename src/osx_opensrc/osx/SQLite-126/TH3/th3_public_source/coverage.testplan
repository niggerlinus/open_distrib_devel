# This file restricts the set of tests used for full-coverage testing.
# The goal is to run as few tests as possible while still exercising
# every branch in the object code, in order to minimize runtime.

# Only run the mutex_unix01 and pragma18 tests once, using c1.
run c1.mutex_unix01
skip *.mutex_unix01
run c1.pragma18
skip *.pragma18

# Run an abbreviated version of the hash tester (hash02) in c1.
# Run the original full-up hash tester (hash01) in all other 
# configurations.
skip c1.hash01
run c1.hash02
skip *.hash02

# The pager02 module is very slow.  Only run it once, in c4.
run c4.pager02
skip *.pager02

# In c6, omit backup02 and backup04.
skip c6.backup0[24]

# Run analyze01, btree45, and btree21 in c6 only.
run c6.analyze01
skip *.analyze01
run c6.btree45
skip *.btree45
run c6.btree21
skip *.btree21

# Only run btree71 once using c9.
run c9.btree71
skip *.btree71

# The only module that needs to be run in cA is prepare02.
run cA.prepare02
skip cA.*

# The only module that needs to be run in cD is pcache01.
run cD.pcache01
skip cD.*

# Only a handfull of modules are needed out of cE.
run cE.btree*
run cE.hash01
run cE.pager30
run cE.savepoint02
run cE.droptable_oom1
run cE.fkey11
skip cE.*

# Nothing in cF is needed.
skip cF.*

# Only the malloc01 module from oom1 is needed.
run oom1.malloc01
skip oom1.*

# Nothing from u1 is needed.
skip u1.*
