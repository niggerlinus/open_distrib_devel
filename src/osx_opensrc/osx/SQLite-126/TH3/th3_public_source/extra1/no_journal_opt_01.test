/*
** A test to verify that the optimization that avoids writing freelist
** pages to the rollback journal is working correctly.
**
** SCRIPT_MODULE_NAME:        e1_no_journal_opt
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     ZIPVFS
** MINIMUM_HEAPSIZE:          100000
*/

/* A large sector size will mess up the measurements */
--if $sector_size<2048

/* Use the FS table in connection 0 to measure the size of files */
--testcase 100
CREATE VIRTUAL TABLE fs USING vfs;
SELECT zfilename FROM fs WHERE zfilename NOT LIKE '%-journal' ORDER BY 1
--result test.db

/* The database under test is on connection 1.  Make it journal_mode=PERSIST
** so that we can see the maximum size of the journal file.  The goal here
** is to see that the journal file never gets very large, even when doing
** updates on very large BLOBs.
**
** The actual size of the journal file and the database file depends on 
** factors such as the sector size and the pending byte.  Rather than
** compute the exact size, we just round the size down to a multiple of
** 10000 and verify that the size is not zero.
*/
--db 1
--raw-new x1.db
--testcase 110
PRAGMA secure_delete=OFF;
PRAGMA page_size=1024;
PRAGMA journal_mode=PERSIST;
CREATE TABLE t1(a,b);
INSERT INTO t1 VALUES(1, zeroblob(100000));
--result 0 persist
--db 0
--testcase 111
SELECT zfilename, sz/10000, sz>2000 FROM fs
 WHERE zfilename GLOB 'x1*' ORDER BY zfilename;
--result x1.db 10 1 x1.db-journal 0 1

--db 1
--testcase 120
UPDATE t1 SET b=zeroblob(100000) WHERE a=1;
--result
--db 0
--testcase 121
SELECT zfilename, sz/10000, sz>2000 FROM fs
 WHERE zfilename GLOB 'x1*' ORDER BY zfilename;
--result x1.db 20 1 x1.db-journal 0 1

--db 1
--testcase 130
UPDATE t1 SET b=zeroblob(100000) WHERE a=1;
--result
--db 0
--testcase 131
SELECT zfilename, sz/10000, sz>2000 FROM fs
 WHERE zfilename GLOB 'x1*' ORDER BY zfilename;
--result x1.db 20 1 x1.db-journal 0 1

--db 1
--testcase 140
UPDATE t1 SET b=zeroblob(100000) WHERE a=1;
--result
--db 0
--testcase 141
SELECT zfilename, sz/10000, sz>2000 FROM fs
 WHERE zfilename GLOB 'x1*' ORDER BY zfilename;
--result x1.db 20 1 x1.db-journal 0 1
