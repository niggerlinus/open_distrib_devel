/*
** A C-code obfuscator.
*/
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

/*
** Token types:
*/
#define TT_Comment     1
#define TT_Whitespace  2
#define TT_Ident       3
#define TT_Macro       4
#define TT_Other       5
#define TT_String      6
#define TT_Number      7

/*
** Return the length of the first token in string z[].  Write the
** token type in to *pType.
*/
static int findToken(const char *z, int *pType){
  int i;
  if( z[0]=='/' && z[1]=='*' && z[2]!=0 ){
    *pType = TT_Comment;
    for(i=3; z[i] && (z[i]!='/' || z[i-1]!='*'); i++){}
    return i+1;
  }
  if( isspace(z[0]) ){
    *pType = TT_Whitespace;
    for(i=1; isspace(z[i]); i++){}
    return i;
  }
  if( isalpha(z[0]) || z[0]=='_' ){
    *pType = TT_Ident;
    for(i=1; isalnum(z[i]) || z[i]=='_'; i++){}
    return i;
  }
  if( z[0]=='"' || z[0]=='\'' ){
    *pType = TT_String;
    for(i=1; z[i]; i++){
      if( z[i]=='\\' ){
        i++;
      }else if( z[i]==z[0] ){
        return i+1;
      }
    }
    return i;
  }
  if( z[0]=='0' && z[1]=='x' ){
    *pType = TT_Number;
    for(i=2; isxdigit(z[i]); i++){}
    return i;
  }
  if( isdigit(z[0]) ){
    *pType = TT_Number;
    for(i=1; isdigit(z[i]) || z[i]=='.'; i++){}
    if( z[i]=='e' || z[i]=='E'
      && (isdigit(z[i+1]) || z[i+1]=='+' || z[i+1]=='-')
    ){
      i += 2;
      while( isdigit(z[i]) ){ i++; }
    }
    return i;
  }
  if( z[0]=='#' ){
    *pType = TT_Macro;
    for(i=1; z[i] && z[i]!='\n'; i++){}
    return i;
  }
  for(i=1; z[i]; i++){
    int c = z[i];
    if( isspace(c) ) break;
    if( isalnum(c) ) break;
    if( c=='/' ) break;
    if( c=='"' ) break;
    if( c=='\'' ) break;
  }
  *pType = TT_Other;
  return i;
}

/*
** Hash table for symbols.
*/
struct HashEntry {
  int id;                   /* The id number for this entry */
  unsigned h;               /* Hash for this entry */
  char *zName;              /* Text of this entry */
  struct HashEntry *pNext;  /* next in the collision chain */
};
#define NHASH 3001
static struct HashEntry *aHash[NHASH];  /* The hash table */
static int nEntry = 0;                  /* Number of hash entries */

/*
** The hash function
*/
static unsigned hash(const char *z){
  unsigned h = 0;
  while( *z ){
    h = h ^ (h>>23) ^ (h<<5) ^ *z;
    z++;
  }
  return h;
}

/*
** Locate any entry in the hash table, or return NULL if not found
*/
static struct HashEntry *hash_find(const char *z){
  unsigned h = hash(z);
  struct HashEntry *p = aHash[h%NHASH];
  while( p && strcmp(p->zName, z)!=0 ){ p = p->pNext; }
  return p;
}

/*
** Insert a new entry in the hash table.  Return a pointer to the new
** entry.
*/
static struct HashEntry *hash_insert(const char *z){
  unsigned h = hash(z);
  int n = strlen(z);
  struct HashEntry *p = malloc( sizeof(*p)+n+1 );
  if( p==0 ){ fprintf(stderr, "out of memory\n"); exit(1); }
  p->id = ++nEntry;
  p->h = h;
  p->zName = (char*)&p[1];
  memcpy(p->zName, z, n+1);
  p->pNext = aHash[h%NHASH];
  aHash[h%NHASH] = p;
  return p;
}


/*
** Identifiers that must not be obfuscated.
*/
static const char *azExclude[] = {
  "aConstraint",
  "aConstraintUsage",
  "aOrderBy",
  "argvIndex",
  "assert",
  "atoi",
  "break",
  "case",
  "char",
  "const",
  "continue",
  "default",
  "desc",
  "do",
  "double",
  "else",
  "enum",
  "estimatedCost",
  "exit",
  "extern",
  "fclose",
  "fflush",
  "float",
  "fopen",
  "for",
  "fprintf",
  "fputs",
  "free",
  "fwrite",
  "getrlimit",
  "gettimeofday",
  "gmtime",
  "goto",
  "iColumn",
  "iTermOffset",
  "iVersion",
  "idxNum",
  "idxStr",
  "if",
  "int",
  "localtime",
  "long",
  "main",
  "malloc",
  "memcmp",
  "memcpy",
  "memmove",
  "memset",
  "mxPathname",
  "nConstraint",
  "nOrderBy",
  "nRef",
  "needToFreeIdxStr",
  "omit",
  "op",
  "orderByConsumed",
  "pAppData",
  "pArg",
  "pMethods",
  "pModule",
  "pNext",
  "pVtab",
  "pthread_create",
  "pthread_join",
  "pthread_t",
  "realloc",
  "register",
  "return",
  "rlim_cur",
  "rlimit",
  "sched_yield",
  "setrlimit",
  "short",
  "signed",
  "size_t",
  "sizeof",
  "static",
  "stderr",
  "stdout",
  "strcasecmp",
  "strcat",
  "strcmp",
  "strcpy",
  "strlen",
  "strncmp",
  "struct",
  "switch",
  "szOsFile",
  "th3covImp",
  "th3coverage",
  "th3reqImp",
  "th3require",
  "time_t",
  "timeval",
  "tm",
  "tv_sec",
  "tv_usec",
  "typedef",
  "union",
  "unsigned",
  "usable",
  "va_arg",
  "va_end",
  "va_list",
  "va_start",
  "void",
  "volatile",
  "vsnprintf",
  "vsprintf",
  "while",
  "zErrMsg",
  "zName",
};

/*
** Check to see if a label is one that should not be obscured.
*/
static int doNotObscure(const char *z){
  int first, last, mid, c;

  if( z[0]>='A' && z[0]<='Z'
   && ((z[1]>='A' && z[1]<='Z') || (z[1]>='0' && z[1]<='9'))
  ){
    return 1;
  }
  if( z[0]=='x' && z[1]>='A' && z[1]<='Z' ) return 1;
  if( memcmp(z, "sqlite", 6)==0 ) return 1;
  if( memcmp(z, "tm_", 3)==0 ) return 1;
  first = 0;
  last = sizeof(azExclude)/sizeof(azExclude[0]) - 1;
  while( first<=last ){
    mid = (first+last)/2;
    c = strcmp(z, azExclude[mid]);
    if( c==0 ) return 1;
    if( c<0 ){
      last = mid-1;
    }else{
      first = mid+1;
    }
  }
  return 0;
}

int cmp(const void *a, const void *b){
  return strcmp(*(const char**)a, *(const char**)b);
}

int main(int argc, char **argv){
  FILE *f;
  size_t size;
  char *zTxt;
  int i, n;
  int type, prevType;
  int iCol = 0;
  int idList = 0;

  for(i=0; i<sizeof(azExclude)/sizeof(azExclude[0])-1; i++){
    if( strcmp(azExclude[i], azExclude[i+1])>=0 ){
      fprintf(stderr, "azExclude[%d] (%s) is out of order\n",
              i+1, azExclude[i+1]);
      exit(1);
    }
  }

  if( (argc!=2 && argc!=3) || (argc==3 && strcmp(argv[1],"-idlist")) ){
    fprintf(stderr, "Usage: %s [-idlist] FILENAME\n", argv[0]);
    return 1;
  }
  idList = argc==3;
  f = fopen(argv[argc-1], "rb");
  if( f==0 ){
    fprintf(stderr, "%s: cannot open %s\n", argv[0], argv[argc-1]);
    return 1;
  }
  fseek(f, 0, SEEK_END);
  size = ftell(f);
  fseek(f, 0, SEEK_SET);
  zTxt = malloc( size+1 );
  fread(zTxt, 1, size, f);
  zTxt[size] = 0;
  fclose(f);
  i = 0;
  prevType = TT_Macro;
  if( !idList ){
    while( zTxt[i] ){
      n = findToken(&zTxt[i], &type);
      if( i==0 && type==TT_Comment ) type = TT_Other;
      if( type==TT_Macro ){
        if( iCol ){
          printf("\n");
        }
        if( memcmp(&zTxt[i], "#define TH3", 11)==0 ){
          int ii, nn;
          char *zLine;
          char cLast;
          struct HashEntry *p;
          zLine = &zTxt[i];
          nn = findToken(&zLine[8], &type);
          cLast = zLine[8+nn];
          zLine[8+nn] = 0;
          p = hash_insert(&zLine[8]);
          zLine[8+nn] = cLast;
          printf("#define _%d ", p->id);
          zLine += 8+nn;
          while( zLine[0]!='\n' && isspace(zLine[0]) ){ zLine++; }
          ii = 0;
          while( zLine[ii]!='\n' && (zLine[ii]!='/' || zLine[ii+1]!='*') ){
            ii++;
          }
          while( ii>0 && isspace(zLine[ii-1]) ){ ii--; }
          printf("%.*s\n", ii, zLine);
        }else if( memcmp(&zTxt[i], "#line ", 6)!=0 ){
          printf("%.*s\n", n, &zTxt[i]);
        }
        iCol = 0;
      }else if( type==TT_Ident ){
        struct HashEntry *p;
        char *zToken = &zTxt[i];
        int nToken;
        char cNext = zToken[n];
        char zBuf[50];

        zToken[n] = 0;
        p = hash_find(zToken);
        if( p==0 && !doNotObscure(zToken) ){
          p = hash_insert(zToken);
        }
        zToken[n] = cNext;
        if( p ){
          sprintf(zBuf, "_%d", p->id);
          zToken = zBuf;
          nToken = strlen(zToken);
        }else{
          nToken = n;
        }
        if( iCol+nToken+1>=80 ){
          printf("\n%.*s", nToken, zToken);
          iCol = nToken;
        }else{
          if( iCol && (prevType==TT_Ident || prevType==TT_Number)  ){
            printf(" "); iCol++;
          }
          printf("%.*s", nToken, zToken);
          iCol += nToken;
        }
      }else if( type==TT_Other || type==TT_String || type==TT_Number ){
        if( iCol+n+1>=80 ){
          printf("\n%.*s", n, &zTxt[i]);
          iCol = n;
        }else{
          if( iCol && (prevType==type
                        || (type==TT_Number && prevType==TT_Ident)) ){
            printf(" "); iCol++;
          }
          printf("%.*s", n, &zTxt[i]);
          iCol += n;
        }
      }else{
        type = prevType;
      }
      prevType = type;
      i += n;
    }
    if( iCol ) printf("\n");
  }else{
    while( zTxt[i] ){
      n = findToken(&zTxt[i], &type);
      if( type==TT_Ident ) printf("%.*s\n", n, &zTxt[i]);
      i += n;
    }
  }
  return 0;
}
