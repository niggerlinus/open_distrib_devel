/*
** This module contains tests of WHERE clause handling.
**
** The focus of this module is VIEWs and ephemeral tables.
**
** SCRIPT_MODULE_NAME:        where23
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x,y,z);
CREATE INDEX t1y ON t1(y);
CREATE INDEX t1z ON t1(z);
INSERT INTO t1 VALUES(1,11,111);
INSERT INTO t1 VALUES(2,22,222);
INSERT INTO t1 VALUES(3,33,333);
INSERT INTO t1 VALUES(4,44,444);
INSERT INTO t1 VALUES(5,55,555);

CREATE VIEW v1 AS SELECT max(y) AS a, min(y) AS b FROM t1;
SELECT * FROM v1 JOIN t1 ON a=y OR b=y ORDER BY x;
--result 55 11 1 11 111 55 11 5 55 555

--testcase 110
SELECT * FROM (SELECT max(y) AS a, min(y) AS b FROM t1) AS e1
              JOIN t1 ON a=y OR b=y
 ORDER BY x;
--result 55 11 1 11 111 55 11 5 55 555

/*
** LEFT JOIN with indices that cover all terms of the table.
*/
--testcase 200
CREATE TABLE t2(z,a);
INSERT INTO t2 VALUES(222,'two');
INSERT INTO t2 VALUES(444,'four');
CREATE INDEX t2za ON t2(z,a);
CREATE INDEX t1xyz ON t1(x,y,z);

SELECT x, a FROM t1 LEFT NATURAL JOIN t2 ORDER BY x;
--result 1 nil 2 two 3 nil 4 four 5 nil

/*
** LEFT JOIN with an OR clause
*/
--testcase 300
INSERT INTO t2 VALUES(33,'three');
INSERT INTO t2 VALUES(99,'nine');
SELECT x, a FROM t2 LEFT JOIN t1
    ON (y=t2.z AND x NOT NULL) OR (t1.z=t2.z AND x>0) ORDER BY x;
--result nil nine 2 two 3 three 4 four
