/*
** Module for testing the journal_mode pragma.
**
** SCRIPT_MODULE_NAME:        pragma23
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB JOURNAL_WAL SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
PRAGMA journal_mode=PERSIST;
CREATE TABLE t1(x);
--result persist

--db 1
--raw-open test.db
--testcase 110
BEGIN EXCLUSIVE;
CREATE TABLE t2(x);
--result

/* The change to DELETE mode will attempt to delete the rollback journal.
** but that is not possible due to the EXCLUSIVE lock held by database
** connection #1.  Nevertheless, the journal_mode change occurs as
** requested.
*/
--db 0
--testcase 120
PRAGMA journal_mode=DELETE;
--result delete

--db 1
--testcase 130
COMMIT;
SELECT name FROM sqlite_master ORDER BY 1;
--result t1 t2

--db 0
--testcase 140
SELECT name FROM sqlite_master ORDER BY 1;
--result t1 t2

--close all
--new test.db
--testcase 200
PRAGMA journal_mode=PERSIST;
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
INSERT INTO t1 VALUES(1,2);
BEGIN IMMEDIATE;
SELECT * FROM t1;
--result persist 1 2

/* dotlock.cfg does not support concurrent access */
--if $configuration<>'dotlock'
--db 1
--raw-open test.db
--testcase 210
PRAGMA journal_mode=PERSIST;
SELECT * FROM t1;
BEGIN;
SELECT * FROM t1;
PRAGMA journal_mode=DELETE;
COMMIT;
--result persist 1 2 1 2 delete
--endif
