/*
** Module for testing vdbeapi.c.  Test the sqlite3_stmt_status()
** interface.
**
** MODULE_NAME:               vdbeapi12
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

int vdbeapi12(th3state *p){
  int n;
  sqlite3_stmt *pStmt;

  /* Test the FULLSCAN_STEP counter.  Do not reset. */
  th3testBegin(p, "1");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE TABLE t1(x);"
    "INSERT INTO t1 VALUES(1);"
    "INSERT INTO t1 VALUES(2);"
    "INSERT INTO t1 SELECT x+2 FROM t1;"
  );
  pStmt = th3dbPrepare(p, 0, "SELECT x FROM t1");
  assert( pStmt );
  while( sqlite3_step(pStmt)==SQLITE_ROW ){}
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_FULLSCAN_STEP, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "3");

  /* The FULLSCAN_STEP counter is unchanged because the previous inquery
  ** did not reset it.  Reset it this time. */
  th3testBegin(p, "2");
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_FULLSCAN_STEP, 1);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "3");

  /* The FULLSCAN_STEP counter was reset and is now zero */
  th3testBegin(p, "3");
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_FULLSCAN_STEP, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "0");

  /* The SORT counter has always been zero because there was no
  ** ORDER BY clause */
  th3testBegin(p, "4");
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_SORT, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "0");

  /* Test the SORT counter.  Do not reset. */
  th3testBegin(p, "101");
  sqlite3_finalize(pStmt);
  pStmt = th3dbPrepare(p, 0, "SELECT x FROM t1 ORDER BY +x DESC");
  assert( pStmt );
  while( sqlite3_step(pStmt)==SQLITE_ROW ){}
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_SORT, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "1");

  /* The SORT counter is unchanged because the previous inquery
  ** did not reset it.  Reset it this time. */
  th3testBegin(p, "102");
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_SORT, 1);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "1");

  /* The SORT counter was reset and is now zero */
  th3testBegin(p, "103");
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_SORT, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "0");

  /* Resetting SORT does not chnges FULLSCAN_STEP */
  /* The FULLSCAN_STEP counter was reset and is now zero */
  th3testBegin(p, "104");
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_FULLSCAN_STEP, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "3");

  /* Run it again.  SORT increments.  FULLSCAN_STEP keeps growing. */
  th3testBegin(p, "105");
  sqlite3_reset(pStmt);
  while( sqlite3_step(pStmt)==SQLITE_ROW ){}
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_SORT, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "1");

  th3testBegin(p, "106");
  n = sqlite3_stmt_status(pStmt, SQLITE_STMTSTATUS_FULLSCAN_STEP, 0);
  th3testAppendResult(p, th3format(p, "%d", n));
  th3testCheck(p, "6");

  sqlite3_finalize(pStmt);

  return 0; 
}
