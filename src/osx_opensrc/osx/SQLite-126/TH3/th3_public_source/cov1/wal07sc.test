/*
** Tests for the OP_JournalMode opcode switching in and out of WAL.
**
** SCRIPT_MODULE_NAME:        wal07sc
** REQUIRED_PROPERTIES:       JOURNAL_WAL SHARED_CACHE
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(123);
--result

--db 1
--open test.db
--testcase 110
SELECT * FROM t1;
--result 123
--testcase 111
PRAGMA wal_checkpoint;
PRAGMA integrity_check;
--glob # # # ok

--db 0
--testcase 120
--open test.db
--oom
PRAGMA journal_mode=DELETE;
--result delete

--db 2
--new test2.db
--convert-filename test.db
--testcase 130
PRAGMA journal_mode=DELETE;
ATTACH :filename AS testdb;
PRAGMA testdb.journal_mode;
--result delete delete

--db 0
--close all
--raw-new test.db
--testcase 140
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(12345);
BEGIN IMMEDIATE;
--result

--db 1
--raw-open test.db
--testcase 150
SELECT * FROM t1;
--result 12345
--testcase 151
PRAGMA journal_mode=WAL;
--result SQLITE_LOCKED {database table is locked}
--testcase 152
PRAGMA main.journal_mode;
--result delete

--open test2.db
--convert-filename test.db
--testcase 160
PRAGMA journal_mode;
--result wal
--testcase 161
ATTACH :filename AS tx;
--result
--testcase 162
PRAGMA tx.journal_mode;
--result delete


--db 0
--testcase 170
COMMIT;
--result
--db 1
--testcase 171
DETACH tx;
ATTACH :filename AS tx;
PRAGMA tx.journal_mode;
--result delete

--db 0
--close 1
--testcase 180
PRAGMA journal_mode=WAL;
INSERT INTO t1 VALUES(23456);
SELECT * FROM t1 ORDER BY 1;
PRAGMA journal_mode;
--result wal 12345 23456 wal

--db 1
--raw-open test2.db
--testcase 190
SELECT * FROM sqlite_master;
PRAGMA journal_mode=DELETE;
--result delete
--testcase 191
PRAGMA journal_mode;
PRAGMA main.journal_mode;
--result delete delete
--testcase 192
ATTACH :filename AS tx;
SELECT * FROM tx.t1 ORDER BY 1;
--result 12345 23456
--testcase 193
PRAGMA journal_mode;
PRAGMA main.journal_mode;
PRAGMA tx.journal_mode;
--result delete delete wal
