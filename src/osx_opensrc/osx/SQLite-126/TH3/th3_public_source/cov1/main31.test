/*
** This module contains tests of the C-language interfaces found in
** the main.c source file. 
**
** MODULE_NAME:               main31
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** The focus of this test module is the sqlite3_create_function_v2()
** interface with destructors.
*/


/*
** Implement a puts(...) SQL function.  The arguments are appending
** to the th3state output as separate elements.
*/
static void main31_puts(
  sqlite3_context *context,
  int argc,
  sqlite3_value **argv
){
  th3state *p = (th3state*)sqlite3_user_data(context);
  int i;
  for(i=0; i<argc; i++){
    th3testAppendResultTerm(p, (char*)sqlite3_value_text(argv[i]));
  }
}

/*
** Implement a noop SQL function.
*/
static void main31_noop(
  sqlite3_context *context,
  int argc,
  sqlite3_value **argv
){
  return;
}

/*
** Used as a destructor:
** The pointer is to an integer.  Simply increment the integer
** so that we know the destructor has been called.
*/
static void main31_destructor(void *pArg){
  int *pInt = (int*)pArg;
  ++*pInt;
}


int main31(th3state *p){
  sqlite3 *db;
  int rc;
  int nDestruct = 0;   /* Number of calls to the destructor */

  /* Register a function using the _v2 destrutor interface */
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  rc = sqlite3_create_function_v2(db, "puts", -1, SQLITE_ANY, &nDestruct,
                                  main31_noop, 0, 0, main31_destructor);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "101");
  th3testCheckInt(p, 0, nDestruct);

  th3testBegin(p, "110");
  rc = sqlite3_create_function_v2(db, "puts", -1, SQLITE_ANY, p,
                                  main31_puts, 0, 0, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "111");
  th3testCheckInt(p, 1, nDestruct);

  th3oomBegin(p, "200");
  while( th3oomNext(p) ){
    nDestruct = 0;
    sqlite3_create_function_v2(db, "puts", -1, SQLITE_ANY, &nDestruct,
                                    main31_noop, 0, 0, main31_destructor);
    th3oomEnable(p, 1);
    rc = sqlite3_create_function_v2(db, "puts", -1, SQLITE_ANY, p,
                                    main31_puts, 0, 0, 0);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      th3oomCheck(p, 3, nDestruct==1);
    }
  }
  th3oomEnd(p);

  th3oomBegin(p, "300");
  while( th3oomNext(p) ){
    th3dbNew(p, 0, ":memory:");
    db = th3dbPointer(p, 0);
    nDestruct = 0;
    th3oomEnable(p, 1);
    sqlite3_create_function_v2(db, "puts", -1, SQLITE_ANY, &nDestruct,
                                    main31_noop, 0, 0, main31_destructor);
    th3dbClose(p, 0);
    th3oomEnable(p, 0);
    if( !th3oomHit(p) ){
      th3oomCheck(p, 1, nDestruct==1);
    }
  }
  th3oomEnd(p);

  return 0;
}
