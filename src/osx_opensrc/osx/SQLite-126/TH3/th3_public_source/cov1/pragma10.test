/*
** Module for testing pragmas.  Tests are written by hand from
** the documentation in pragma.html
**
** SCRIPT_MODULE_NAME:        pragma10
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
*/

/**************************************************************************
** PRAGMA max_page_count;
** PRAGMA max_page_count=N;
*/
--testcase mpc-1
CREATE TABLE t1(x);
PRAGMA max_page_count=12345;
PRAGMA max_page_count;
PRAGMA max_page_count=10;
PRAGMA max_page_count;
--result 12345 12345 10 10

--testcase mpc-2
PRAGMA page_size;
--store :pagesize
INSERT INTO t1 values(zeroblob(:pagesize*10));
--result SQLITE_FULL {database or disk is full}

--testcase mpc-3
PRAGMA max_page_count=20;
INSERT INTO t1 values(zeroblob(:pagesize*10));
--result 20

--testcase mpc-4
PRAGMA max_page_count=5;
SELECT * FROM t1;
--run 0
PRAGMA max_page_count;
--store :maxsize
PRAGMA page_count;
--store :actualsize
SELECT :maxsize=:actualsize
--result 1


--testcase mpc-9
PRAGMA page_count;
--glob 1[23]

--db 1
--open test.db
--testcase mpc-10
PRAGMA page_count;
INSERT INTO t1 values(zeroblob(:pagesize*10));
PRAGMA page_count;
--glob 1[23] 2[23]

--db 0
--testcase mpc-11
SELECT count(*) FROM t1;
PRAGMA page_count;
PRAGMA max_page_count;
--glob 2 2[23] 2[23]
