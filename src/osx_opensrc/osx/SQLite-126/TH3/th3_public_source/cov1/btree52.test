/*
** Test module for btree.c.  
**
** Test of btreeAllocatePage().  When the PENDING_BYTE page is on a
** page immediately following a PTRMAP page, make sure this is handled
** correctly.
**
** MODULE_NAME:               btree52
** REQUIRED_PROPERTIES:       AUTOVACUUM INITIALIZE_OK
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/
int btree52(th3state *p){
  int i;

  /* Set the pending byte so that it is on the page following a ptrmap
  ** page when the page size is 512.
  */
  sqlite3_shutdown();
  sqlite3_test_control(SQLITE_TESTCTRL_PENDING_BYTE, 53760);
  sqlite3_initialize();
   
  /* Verify that allocation works */
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
    "CREATE TABLE t1(a INTEGER PRIMARY KEY, b);"
    "INSERT INTO t1 VALUES(1, zeroblob(60000));"
    "SELECT a, length(b) FROM t1;"
    "PRAGMA integrity_check;"
  );
  th3testCheck(p, "1 60000 ok");

  th3dbCacheSetSize(p, 3);
  for(i=60000-508; i>40000; i-=508){
    th3testBegin(p, th3format(p, "110.%d", i));
    th3bindInt(p, "$i", i);
    th3dbEval(p, 0, "UPDATE t1 SET b=zeroblob($i)");
    th3dbEval(p, 0, "SELECT a, length(b) FROM t1; PRAGMA integrity_check;");
    th3testCheck(p, th3format(p, "1 %d ok", i));
  }

  /* Restore the pending byte for other test cases */
  th3dbCloseAll(p);
  sqlite3_shutdown();
  sqlite3_test_control(SQLITE_TESTCTRL_PENDING_BYTE, p->config.pendingByte);
  return 0;
}
