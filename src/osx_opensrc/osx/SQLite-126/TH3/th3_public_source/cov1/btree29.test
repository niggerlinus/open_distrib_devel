/*
** Test module for btree.c.  DROP TABLE with rootpage==1.
**
** SCRIPT_MODULE_NAME:        btree29
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x);
--result

--testcase 110
PRAGMA writable_schema=ON;
UPDATE sqlite_master SET rootpage=1 WHERE name='t1';
PRAGMA main.wal_checkpoint;
--run 0
incr-schema
--edit test.db
DROP TABLE t1;
--result

--open test.db
--testcase 120
PRAGMA integrity_check;
--result SQLITE_CORRUPT {database disk image is malformed}

/*
** Verify that calls to checkTreePage() with iPage==0 work correctly.
*/
--new test.db
--testcase 200
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, zeroblob($pgsz/3));
INSERT INTO t1 VALUES(2, zeroblob($pgsz/3));
INSERT INTO t1 VALUES(3, zeroblob($pgsz/3));
INSERT INTO t1 VALUES(4, zeroblob($pgsz/3));
PRAGMA wal_checkpoint;
--run 0
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
seek $root move 8 write32 0 incr-chng
--edit test.db
PRAGMA integrity_check;
--store-raw $result
SELECT substr($result,1,24) == '*** in database main ***';
--result 1
