/*
** This module contains tests of vdbeaux.c source module.
**
** SCRIPT_MODULE_NAME:        vdbeaux06
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB UTF16BE UTF16LE
** MINIMUM_HEAPSIZE:          100000
**
** This module focuses on corruption of index entries and detection
** of that corruption by sqlite3VdbeIdxRowid().
*/
--new test.db
--autocheckpoint 1
--testcase 100
CREATE TABLE t1(x);
INSERT INTO t1(rowid,x) VALUES(9223372036854775807, 'abcdefghijklmnop');
CREATE INDEX t1x ON t1(x);
SELECT rowid FROM t1 WHERE x='abcdefghijklmnop';
--result 9223372036854775807

--testcase 110
PRAGMA page_size;
--store $pgsz
SELECT rootpage*$pgsz - 28 FROM sqlite_master WHERE name='t1x';
--store $ofst
seek $ofst read 28
--edit test.db
--result 1b032d066162636465666768696a6b6c6d6e6f707fffffffffffffff

--testcase 120
seek $ofst 
write 1b032d006162636465666768696a6b6c6d6e6f707fffffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 WHERE x='abcdefghijklmnop';
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 130
seek $ofst 
write 1b032d076162636465666768696a6b6c6d6e6f707fffffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 WHERE x='abcdefghijklmnop';
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 140
seek $ofst 
write 1b032d0a6162636465666768696a6b6c6d6e6f707fffffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 WHERE x='abcdefghijklmnop';
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 150
seek $ofst 
write 1b022d066162636465666768696a6b6c6d6e6f707fffffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 ORDER BY x;
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 160
seek $ofst 
write 1bff7f2d066162636465666768696a6b6c6d6e6f707fffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 ORDER BY x;
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 170
seek $ofst 
write 1b042d80086162636465666768696a6b6c6d6e6f707fffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 ORDER BY x;
--result 0

--testcase 180
seek $ofst 
write 1b032d80096162636465666768696a6b6c6d6e6f707fffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 ORDER BY x;
--result 1

--testcase 190
seek $ofst 
write 05030006162636465666768696a6b6c6d6e6f707fffffffffffff
incr-chng
--edit test.db
SELECT rowid FROM t1 ORDER BY x;
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 200
DELETE FROM t1;
INSERT INTO t1(rowid, x) VALUES(32, zeroblob(1000));
SELECT rowid FROM t1 ORDER BY x;
--result 32

--testcase 210
SELECT (rootpage - 1)*$pgsz FROM sqlite_master WHERE name='t1x';
--store $ofst
seek $ofst move 8  read16 store $cellofst
seek $ofst move $cellofst move 2 write 02
incr-chng
--edit test.db
SELECT rowid FROM t1 ORDER BY x;
--result SQLITE_CORRUPT {database disk image is malformed}
