/*
** This module contains tests for the INSERT statement.
**
** These test are written from user documentation.
**
** SCRIPT_MODULE_NAME:        insert01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 1
CREATE TABLE t1(a,b,c,d,e);
INSERT INTO t1 VALUES(1,2,3,4,5);
SELECT * FROM t1;
--result 1 2 3 4 5

--testcase 2
INSERT INTO t1([b],`C`,"a") VALUES(6,7,8);
SELECT * FROM t1 ORDER BY rowid;
--result 1 2 3 4 5 8 6 7 nil nil

--testcase 3
--oom
INSERT INTO t1 VALUES(11,12,13,14);
--result SQLITE_ERROR {table t1 has 5 columns but 4 values were supplied}

--testcase 4
INSERT INTO t1 VALUES(11,12,13,14,15,16);
--result SQLITE_ERROR {table t1 has 5 columns but 6 values were supplied}

--testcase 5
INSERT INTO t1(c,b,a) VALUES(11,12);
--result SQLITE_ERROR {2 values for 3 columns}

--testcase 6
INSERT INTO t1(e,d,c) VALUES(11,12,13,14);
--result SQLITE_ERROR {4 values for 3 columns}

--testcase 7
--oom
INSERT INTO t1(a,b,f) VALUES('a','b','c');
--result SQLITE_ERROR {table t1 has no column named f}

--testcase 8
INSERT INTO sqlite_master(name) VALUES('hello');
--result SQLITE_ERROR {table sqlite_master may not be modified}

--testcase 9
--oom
INSERT INTO no_such_table(name) VALUES('hello');
--result SQLITE_ERROR {no such table: no_such_table}

--testcase 10
INSERT INTO t1 VALUES(1,2,3,4,xyz);
--result SQLITE_ERROR {no such column: xyz}

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 100
CREATE TABLE t2(
  a INTEGER PRIMARY KEY AUTOINCREMENT,
  b INT DEFAULT 999,
  c REAL DEFAULT 88.75,
  d TEXT DEFAULT 'dflt-text',
  e BLOB DEFAULT x'426c6f62'
);
INSERT INTO t2 DEFAULT VALUES;
SELECT a,b,c,d,lower(quote(e)) FROM t2;
--result 1 999 88.75 dflt-text x'426c6f62'

--testcase 101
DELETE FROM t2;
INSERT INTO t2(e,d,c) VALUES(1,2,3);
SELECT * FROM t2;
--result 2 999 3.0 2 1
#endif

--testcase 201
INSERT INTO t1
   SELECT a+100, b+100, c+100, d+100, e+100 FROM t1
    WHERE e NOT NULL;
SELECT * FROM t1 ORDER BY rowid;
--result 1 2 3 4 5 8 6 7 nil nil 101 102 103 104 105

--testcase 203
INSERT INTO main.t1(a,c,e)
   SELECT a+200, b+200, c+200 FROM t1 WHERE e NOT NULL AND e<10;
SELECT * FROM t1 ORDER BY rowid;
--result 1 2 3 4 5 8 6 7 nil nil 101 102 103 104 105 201 nil 202 nil 203

/* Create a table with constraints for testing the ON CONFLICT logic
*/
--testcase 301
--oom
CREATE TABLE t3(
  a INTEGER PRIMARY KEY,
  b TEXT UNIQUE,
  c INT,
  d INT NOT NULL,
  e INT NOT NULL DEFAULT 99,
  UNIQUE(c,d)
);
INSERT INTO t3 SELECT a, 'hello', b, c, 5 FROM t1;
--result SQLITE_CONSTRAINT {column b is not unique}

--testcase 302
SELECT * FROM t3;
--result

/***** INSERT OR IGNORE *****/
--testcase 311
--oom
INSERT INTO t3 VALUES(2,'hello',3,4,5);
INSERT OR IGNORE INTO t3 VALUES(3, 'hello', 4, 5, 6);
INSERT OR IGNORE INTO t3 VALUES(4, 'hi', 4, 5, null);
INSERT OR IGNORE INTO t3 VALUES(5, 'hi',3,4,8);
SELECT * FROM t3;
--result 2 hello 3 4 5

/****** INSERT OR REPLACE ******/
--testcase 321
INSERT INTO t3 VALUES(3,'hi',4,5,6);
INSERT OR REPLACE INTO t3 VALUES(4,'hello',4,5,6);
SELECT * FROM t3;
--result 4 hello 4 5 6

--testcase 322
INSERT OR REPLACE INTO t3 VALUES(3, 'hi', 1, 2, null);
SELECT * FROM t3 ORDER BY a;
--result 3 hi 1 2 99 4 hello 4 5 6

--testcase 323
INSERT OR REPLACE INTO t3 VALUES(2, 'howdy', 2, null, 8);
--result SQLITE_CONSTRAINT {t3.d may not be NULL}

--testcase 324
SELECT * FROM t3 ORDER BY a;
--result 3 hi 1 2 99 4 hello 4 5 6

--testcase 325
REPLACE INTO t3(e,d,c,b,a) VALUES(3,2,1,'hello',4);
SELECT * FROM t3;
--result 4 hello 1 2 3

--testcase 326
--oom
REPLACE INTO t3 VALUES(4,'hello',4,5,6);
SELECT * FROM t3;
--result 4 hello 4 5 6

/****** INSERT OR ROLLBACK ******/
--testcase 331
BEGIN;
INSERT INTO t3 VALUES(5,'hi',5,6,7);
INSERT INTO t3 VALUES(6,'goodbye','x','y',9);
INSERT OR ROLLBACK INTO t3 VALUES(7,'hi',101,102,103);
--result SQLITE_CONSTRAINT {column b is not unique}

--testcase 332
COMMIT;
--result SQLITE_ERROR {cannot commit - no transaction is active}

--testcase 333
SELECT * FROM t3;
--result 4 hello 4 5 6

--testcase 334
BEGIN;
INSERT INTO t3 VALUES(5,'hi',5,6,7);
INSERT INTO t3 VALUES(6,'goodbye','x','y',9);
INSERT OR ROLLBACK INTO t3 VALUES(7,'howdy',101,102,null);
--result SQLITE_CONSTRAINT {t3.e may not be NULL}

--testcase 334
COMMIT;
--result SQLITE_ERROR {cannot commit - no transaction is active}

--testcase 335
SELECT * FROM t3;
--result 4 hello 4 5 6

/******* INSERT OR FAIL ******/
--testcase 341
DELETE FROM t1;
INSERT INTO t1 VALUES(5,'hi',1,2,3);
INSERT INTO t1 VALUES(6,'hello',2,3,4);
INSERT OR FAIL INTO t3 SELECT * FROM t1 ORDER BY a;
--result SQLITE_CONSTRAINT {column b is not unique}

--testcase 342
SELECT * FROM t3 ORDER BY a;
--result 4 hello 4 5 6 5 hi 1 2 3

/******* INSERT OR ABORT ******/
--testcase 341
DELETE FROM t3 WHERE a=5;
INSERT OR ABORT INTO t3 SELECT * FROM t1 ORDER BY a;
--result SQLITE_CONSTRAINT {column b is not unique}

--testcase 342
SELECT * FROM t3 ORDER BY a;
--result 4 hello 4 5 6
