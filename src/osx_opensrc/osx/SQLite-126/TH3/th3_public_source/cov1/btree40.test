/*
** Test module for btree.c.  
**
** sqlite3BtreeTripAllCursors().
**
** The UPDATE statement within the th3eval() will fail on an OOM forcing
** a rollback.  That rollback invokes sqlite3BtreeTripAllCursors() which
** invalids the cursors used by the outer query.  The next call to to
** btreeRestoreCursorPosition will see the cursor state as CURSOR_FAULT
** and will fail.
**
** The 3rd argument to th3eval() means that th3eval() does not fail even
** though its command did fail.  This is important to make this test work.
**
** Because th3eval() swallows many SQLITE_NOMEM errors, we use the --oom-ck
** variant of the OOM loop which only checks to make sure the result string
** is something different from "ck".
**
** SCRIPT_MODULE_NAME:        btree40
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY, y, z);
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, 100, zeroblob($pgsz/2));
INSERT INTO t1 VALUES(2, 200, zeroblob($pgsz/2));
INSERT INTO t1 VALUES(3, 300, zeroblob($pgsz/2));
SELECT count(*) FROM t1;
PRAGMA vdbe_trace=off;
--result 3

--testcase 200
--oom-ck
SELECT th3eval('UPDATE t1 SET y=y+1 WHERE x=2',0,x), y>=x*100
  FROM t1 ORDER BY y;
--result nil 1 nil 1 nil 1

--testcase 300
UPDATE t1 SET y = x*100;
SELECT CASE WHEN x!=2 THEN y ELSE (y/100)*100 END,
       th3eval('DELETE FROM t1 WHERE x=1',0,x), y>=x*100
  FROM t1 ORDER BY y;
--result 100 nil nil 200 nil 1 300 nil 1
