/*
** This module contains tests for the UPDATE statement.
**
** Virtual tables updates.
**
** SCRIPT_MODULE_NAME:        update05
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE VIRTUAL TABLE vt1 USING bvs;
SELECT 'hello'
--store x1
SELECT * FROM vt1;
--result x1 hello

--testcase 110
UPDATE vt1 SET value='howdy' WHERE name='x1';
SELECT * FROM vt1;
--result x1 howdy

--testcase 200
INSERT INTO vt1(rowid,name,value) VALUES(5,'x2','xyzzy');
SELECT rowid, * FROM vt1 ORDER BY rowid
--result 0 x1 howdy 5 x2 xyzzy

/* The BVS virtual table implementation does not allow the rowid to be
** modified.  This is a limitation of BVS, not of virtual tables */
--testcase 201
UPDATE vt1 SET rowid=1 WHERE rowid=5;
--result SQLITE_ERROR {cannot change the rowid}
