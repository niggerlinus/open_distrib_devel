/*
** Test module for btree.c.
**
** OOM or I/O error during incremental blob I/O.
** The sqlite3BtreeData() routine.
**
** MODULE_NAME:               btree56
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     NO_OOM
** MINIMUM_HEAPSIZE:          100000
*/
int btree56(th3state *p){
  sqlite3 *db;
  sqlite3_blob *pBlob = 0;
  int rc;
  unsigned char pBuf[20];
  static unsigned const char zPattern[] = {
       0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88,
       0x99, 0xaa, 0xbb, 0xcc, 0xdd, 0xee, 0xff
  };

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  th3dbEval(p, 0,
    "CREATE TABLE t1(x INTEGER PRIMARY KEY, y BLOB, z BLOB);"
    "INSERT INTO t1 VALUES(1, x'00112233445566778899aabbccddeeff',"
                   " zeroblob(700));"
    "INSERT INTO t1 VALUES(234, null, zeroblob(700));"
    "SELECT count(*) FROM t1;"
    "CREATE TABLE t2(a);"
    "PRAGMA cache_size=2;"
  );
  th3testCheck(p, "2");

  th3oomBegin(p, "110");
  while( th3oomNext(p) ){
    rc = sqlite3_blob_open(db, "main", "t1", "y", 1, 1, &pBlob);
    assert( rc==SQLITE_OK );
    th3dbEval(p, 0, "BEGIN;");
    rc = sqlite3_blob_write(pBlob, "abcdefg", 7, 0);
    assert( rc==SQLITE_OK );
    th3oomEnable(p, 1);
    th3dbEval(p, 0, 
       "INSERT INTO t2 VALUES(zeroblob(20000));"
    );
    th3oomEnable(p, 0);
    rc = sqlite3_blob_read(pBlob, pBuf, 16, 0);
    th3dbEval(p, 0, "ROLLBACK");
    if( rc!=SQLITE_OK ){
      th3oomCheck(p, 1, rc==SQLITE_ABORT);
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      th3oomCheck(p, 3, 0==memcmp(pBuf, "abcdefg", 7));
      th3oomCheck(p, 4, 0==memcmp(&pBuf[7], &zPattern[7], 9));
    }
    sqlite3_blob_close(pBlob);
  }
  th3oomEnd(p);
  return 0;
}
