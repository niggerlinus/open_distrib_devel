/*
** This module contains tests for the VACUUM statement.
**
** Try to vacuum while a SELECT is in process.
**
** MODULE_NAME:               vacuum05
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
int vacuum05(th3state *p){
  sqlite3_stmt *pStmt;
  int rc;

  th3dbNew(p, 0, "test.db");
  th3testBegin(p, "100");
  th3dbEval(p, 0, 
     "CREATE TABLE t1(a);"
     "INSERT INTO t1 VALUES(1);"
     "INSERT INTO t1 VALUES(2);"
     "INSERT INTO t1 SELECT a+2 FROM t1;"
     "INSERT INTO t1 SELECT a+4 FROM t1;"
     "INSERT INTO t1 SELECT a+8 FROM t1;"
     "SELECT count(*), sum(a) FROM t1;"
  );
  th3testCheck(p, "16 136");

  th3testBegin(p, "110");
  pStmt = th3dbPrepare(p, 0, "SELECT a FROM t1 ORDER BY a");
  rc = sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_ROW, rc);
  th3testBegin(p, "111");
  th3testCheckInt(p, 1, sqlite3_column_int(pStmt,0));

  th3testBegin(p, "120");
  th3dbEval(p, 0, "VACUUM");
  th3testCheck(p, "SQLITE_ERROR {cannot VACUUM - SQL statements in progress}");

  th3testBegin(p, "130");
  sqlite3_finalize(pStmt);
  th3dbEval(p, 0, "VACUUM");
  th3testCheck(p, "");

  return 0;
}
