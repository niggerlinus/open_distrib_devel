/*
** Test module for btree.c and especially the ability of freeSpace() to
** handle database corruption.
**
** SCRIPT_MODULE_NAME:        btree06
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
PRAGMA page_size;
--store $pgsz
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
INSERT INTO t1 VALUES(0, zeroblob($pgsz-400));
INSERT INTO t1 VALUES(1, x'010203');
INSERT INTO t1 VALUES(2, NULL);
INSERT INTO t1 VALUES(3, NULL);
UPDATE t1 SET y=x'0102' WHERE x=1;
--result
--checkpoint

/* By replacing the x=1 entry with a new entry that is 1 byte smaller,
** we should have created a 1-byte fragment in between 2 and 1.  Verify this.
*/
--testcase 110
seek $root move 7 read 1
--edit test.db
--result 01

/* Change the fragment count to zero.  Then do so deletes.  Verify that
** the corruption is detected.
*/
--testcase 120
seek $root move 7 write 00 incr-chng
--edit test.db
SELECT x FROM t1;
--result 0 1 2 3

/* Delete x=2 to cause a corruption detection in freeSpace() */
--testcase 130
DELETE FROM t1 WHERE x=1;
DELETE FROM t1 WHERE x=2;
--result SQLITE_CORRUPT {database disk image is malformed}
