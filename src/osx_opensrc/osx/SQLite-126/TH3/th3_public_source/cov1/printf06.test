/*
** Module for testing printf.c.
** 
** These tests, derived from code analysis, test printf interfaces
** following a failure of sqlite3_initialize().
**
** MODULE_NAME:               printf06
** REQUIRED_PROPERTIES:       TEST_MUTEX INITIALIZE_OK
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/
#include <stdarg.h>

/*
** Run a single PRINTF test case.
*/
static void printf06_test(
  th3state *p,             /* Testing context */
  int testId,              /* Test id */
  const char *zCorrect,    /* Expected result */
  const char *zFormat,     /* Format string */
  ...                      /* Additional arguments */
){
  va_list ap;
  char *z;
  th3testBegin(p, th3format(p, "%d", testId));
  va_start(ap, zFormat);
  z = sqlite3_vmprintf(zFormat, ap);
  va_end(ap);
  if( z ){
    th3testAppendResult(p, z);
    sqlite3_free(z);
  }else{
    th3testAppendResult(p, "nil");
  }
  th3testCheck(p, zCorrect);
}

int printf06(th3state *p){
#ifndef SQLITE_OMIT_AUTOINIT
  char *z;

  th3testBegin(p, "1");
  sqlite3_shutdown();
  th3mutexGlobal.mutexInitReturn = SQLITE_NOMEM;
  z = sqlite3_mprintf("format string");
  th3testAppendResult(p, z ? z : "nil");
  sqlite3_free(z);
  th3testCheck(p, "nil");

  printf06_test(p, 2, "nil", "format string");

  th3testBegin(p, "11");
  sqlite3_shutdown();
  th3mutexGlobal.mutexInitReturn = SQLITE_OK;
  z = sqlite3_mprintf("format string");
  th3testAppendResult(p, z ? z : "nil");
  sqlite3_free(z);
  th3testCheck(p, "format string");

  sqlite3_shutdown();
  printf06_test(p, 2, "format string", "format string");
#endif /* SQLITE_OMIT_AUTOINIT */

  return 0;
}
