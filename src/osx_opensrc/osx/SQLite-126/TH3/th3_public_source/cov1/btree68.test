/*
** Test module for btree.c.
**
** Provoke the various SQLITE_CORRUPT returns in the allocateSpace() routine.
**
** SCRIPT_MODULE_NAME:        btree68
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT ALT_PCACHE
** DISALLOWED_PROPERTIES:     NO_OOM MEMDB JOURNAL_WAL
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
PRAGMA cache_size=8888;
CREATE TABLE t1(x INTEGER PRIMARY KEY, y);
INSERT INTO t1 VALUES(1,zeroblob(100));
INSERT INTO t1 VALUES(2,zeroblob(100));
INSERT INTO t1 VALUES(3,zeroblob(100));
INSERT INTO t1 VALUES(4,zeroblob(100));
DELETE FROM t1 WHERE x=1;
SELECT count(*) FROM t1;
--result 3
--checkpoint

/* There is at least one free block on the page */
--testcase 110
PRAGMA page_size;
--store $pgsz
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
seek $root move 1 read16 store $ofst
--edit test.db
SELECT $ofst+0>4 AND $ofst+0<$pgsz-4
--result 1

/* Change the address of the first freeblock to 3 in the page cache,
** then provoke a call to allocateSpace().  3 is an illegal freeblock
** address.  allocateSpace() should detect that and return SQLITE_CORRUPT.
*/
--testcase 120
BEGIN;
INSERT INTO t1 VALUES(5,null);
SELECT count(*) FROM t1;
--result 4
--checkpoint
--testcase 121
seek $root move 1 alt_pcache 0003
--edit test.db
INSERT INTO t1 VALUES(6,zeroblob(50));
--result SQLITE_CORRUPT {database disk image is malformed}

/* Restore the page cache to the original database content */
--testcase 130
ROLLBACK;
--run 0
incr-chng
--edit test.db
BEGIN;
SELECT count(*) FROM t1;
INSERT INTO t1 VALUES(5,zeroblob(50));
SELECT count(*) FROM t1;
ROLLBACK;
--result 3 4

/* Change the address of the first freeblock in the page cache to be
** 3 from the end of the page, then provoke a call to allocateSpace().
** The freeblock address is invalid.  allocateSpace() should detect 
** that and return SQLITE_CORRUPT.
*/
--testcase 140
BEGIN;
INSERT INTO t1 VALUES(5,null);
SELECT count(*) FROM t1;
--result 4
--checkpoint
--testcase 141
seek $root move 1 alt_pcache16 $pgsz
--edit test.db
INSERT INTO t1 VALUES(6,zeroblob(50));
--result SQLITE_CORRUPT {database disk image is malformed}

/* Restore the page cache to the original database content */
--testcase 150
ROLLBACK;
--run 0
incr-chng
--edit test.db
BEGIN;
SELECT count(*) FROM t1;
INSERT INTO t1 VALUES(5,zeroblob(50));
SELECT count(*) FROM t1;
ROLLBACK;
--result 3 4

/* Change the size of the first freeblock in the page cache so that it
** extends off the end of the page, then provoke a call to allocateSpace().
** allocateSpace() should detect  the problem and return SQLITE_CORRUPT.
*/
--testcase 160
BEGIN;
INSERT INTO t1 VALUES(5,null);
SELECT count(*) FROM t1;
--result 4
--checkpoint
--testcase 161
seek $root move $ofst move 2 alt_pcache16 110
--edit test.db
INSERT INTO t1 VALUES(6,zeroblob(50));
--result SQLITE_CORRUPT {database disk image is malformed}
