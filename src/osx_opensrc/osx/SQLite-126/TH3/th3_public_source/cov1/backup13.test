/*
** Backup from a normal database file into a TEMP database file.
**
** MODULE_NAME:               backup13
** REQUIRED_PROPERTIES:       CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
int backup13(th3state *p){
  sqlite3 *pSrc;    /* Source database */
  sqlite3 *pDest;   /* Destination database */
  char zCksum[100]; /* Database checksum */

  sqlite3_backup *pBackup;
  int rc;

#if defined(SQLITE_TEMP_STORE) && SQLITE_TEMP_STORE==3
  return 0;
#endif
  
  /* Create a source database in the TEMP table */
  th3testBegin(p, "1");
  th3dbNew(p, 0, "src.db");
  th3dbEval(p, 0,
     "PRAGMA temp_store=1;"
     "PRAGMA page_size=1024;"
     "CREATE TABLE t1(a,b);"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(801));"
     "INSERT INTO t1 VALUES(th3random(), th3randomBlob(802));"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(803) FROM t1;"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(804) FROM t1;"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(805) FROM t1;"
     "INSERT INTO t1 SELECT th3random(), th3randomBlob(806) FROM t1;"
     "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "32");

  /* Compute a checksum over the source database */
  th3testResetResult(p);
  th3dbEval(p, 0, "SELECT count(*), md5sum(a,b) FROM t1");
  assert( strlen(p->zResult)<sizeof(zCksum) );
  th3strcpy(zCksum, p->zResult);
  th3testResetResult(p);

  /* Create the destination database and do the copy.  We are
  ** copying from MAIN to TEMP.
  */
  th3testBegin(p, "2");
  pSrc = th3dbPointer(p, 0);
  th3dbNew(p, 1, "dest.db");
  if( (p->config.maskProp & TH3_TEMPSTORE_MEM)==0 ){
    th3dbEval(p, 1, "PRAGMA page_size=2048; CREATE TEMP TABLE bogus(x);");
  }
  pDest = th3dbPointer(p, 1);
  pBackup = sqlite3_backup_init(pDest, "temp", pSrc, "main");
  if( pBackup==0 ){
    th3testFailed(p, sqlite3_errmsg(pDest));
    return 0;
  }
  rc = sqlite3_backup_step(pBackup, 9999);
  th3testAppendResult(p, th3format(p, "%d", rc));
  th3testCheck(p, "101");


  /* Verify that the destination is identical to the source */
  th3testBegin(p, "3");
  rc = sqlite3_backup_finish(pBackup);
  if( rc!=SQLITE_OK ){
    th3testFailed(p, sqlite3_errmsg(pDest));
    return 0;
  }
  th3dbEval(p, 1, "SELECT count(*), md5sum(a,b) FROM t1");
  th3testCheck(p, zCksum);

  /* Destination is still writable */
  th3testBegin(p, "4");
  th3dbEval(p, 1, 
     "INSERT INTO t1 VALUES(123,456);"
     "SELECT count(*) FROM t1;"
  );
  th3testCheck(p, "33");

  /* IOERR during copy from MAIN to TEMP */
  th3ioerrBegin(p, "5");
  while( th3ioerrNext(p) ){
    th3dbNew(p, 1, "dest.db");
    if( (p->config.maskProp & TH3_TEMPSTORE_MEM)==0 ){
      th3dbEval(p, 1, "PRAGMA page_size=2048; CREATE TEMP TABLE bogus(x);");
    }
    pDest = th3dbPointer(p, 1);
    p->fsysTest.ioerr.mask = TH3VFS_IOERR_FILESIZE;
    th3ioerrEnable(p, 1);
    pBackup = sqlite3_backup_init(pDest, "temp", pSrc, "main");
    if( pBackup ){
      rc = sqlite3_backup_step(pBackup, 9999); 
      sqlite3_backup_finish(pBackup);
      if( !th3ioerrHit(p) ){
        th3dbEval(p, 1, "SELECT count(*), md5sum(a,b) FROM t1");
      }
    }
    th3ioerrEnable(p, 0);
    if( th3ioerrHit(p) ){
      th3ioerrCheck(p, 1, pBackup==0 || rc!=SQLITE_OK);
    }else{
      th3ioerrCheck(p, 2, rc==SQLITE_DONE);
      th3ioerrResult(p, 3, zCksum);
    }
  }
  th3ioerrEnd(p);   
 
  /* OOM after copy from MAIN to TEMP */
  th3oomBegin(p, "6");
  while( th3oomNext(p) ){
    th3dbNew(p, 1, "dest.db");
    if( (p->config.maskProp & TH3_TEMPSTORE_MEM)==0 ){
      th3dbEval(p, 1, "PRAGMA page_size=2048; CREATE TEMP TABLE bogus(x);");
    }
    pDest = th3dbPointer(p, 1);
    pBackup = sqlite3_backup_init(pDest, "temp", pSrc, "main");
    sqlite3_backup_step(pBackup, 9999); 
    sqlite3_backup_finish(pBackup);
    th3oomEnable(p, 1);
    rc = th3dbEval(p, 1, "SELECT count(*), md5sum(a,b) FROM t1");
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      th3oomResult(p, 3, zCksum);
    }
  }
  th3oomEnd(p);   
 
  return 0;
}
