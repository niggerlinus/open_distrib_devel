/*
** Test module for btree.c.  PRAGMA integrity_check.
**
** SCRIPT_MODULE_NAME:        btree26
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x);
PRAGMA auto_vacuum=INCREMENTAL;
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(zeroblob($pgsz*10));
DELETE FROM t1;
--result
--checkpoint

/* Corrupt the freelist in various ways.  Make sure PRAGMA integrity_check
** finds the corruption.
*/
--testcase 110
seek 32 read32 store $trunk1
--edit test.db
SELECT ($trunk1-1)*$pgsz;
--store $trunk1
seek $trunk1 move 4 write32 123456
incr-chng
--edit test.db
PRAGMA integrity_check;
--store $msg
SELECT $msg LIKE '%freelist leaf count too big on page %';
--result 1

--testcase 120
PRAGMA integrity_check(1);
--store $msg
SELECT $msg NOT LIKE 'ok';
--result 1

--new test.db
--testcase 200
CREATE TABLE t1(x);
PRAGMA auto_vacuum=INCREMENTAL;
INSERT INTO t1 VALUES(zeroblob($pgsz*10));
DELETE FROM t1;
PRAGMA page_size;
--store $pgsz
--checkpoint
seek 32 read32 store $trunk1
--edit test.db
SELECT ($trunk1-1)*$pgsz;
--store $trunk1
seek $trunk1 move 16 write32 123456 write32 23456 write32 0
incr-chng
--edit test.db
PRAGMA integrity_check;
--store $msg
SELECT $msg LIKE '%invalid page number 123456%';
--result 1

--testcase 210
PRAGMA integrity_check(1);
--store $msg
SELECT $msg NOT LIKE 'ok';
--result 1

--testcase 230
seek 32 write32 54321
incr-chng
--edit test.db
PRAGMA integrity_check;
--store $msg
SELECT $msg LIKE '%invalid page number 54321%';
--result 1

--testcase 240
seek 32 write32 0
incr-chng
--edit test.db
PRAGMA integrity_check;
--store $msg
SELECT $msg NOT LIKE 'ok'
--result 1
