/*
** Module for testing WAL with read-only WAL and/or SHM files.
**
** MIXED_MODULE_NAME:         wal36
** REQUIRED_PROPERTIES:       TEST_VFS URI
** DISALLOWED_PROPERTIES:     SHARED_CACHE MEMDB
** MINIMUM_HEAPSIZE:          100000
*/

/* Set the readonly flag of the "main" file for the database according
** to zArg.  0=read/write.  1=read-only.   2=read-only-SHM.
*/
static void wal36_set_readonly(th3state *p, int iDb, char *zArg){
  int v = (int)th3atoi64(zArg);
  sqlite3_file_control(th3dbPointer(p,iDb), "main", TH3_FCNTL_SET_READONLY, &v);
}

/* Get the extended error code.
*/
static void wal36_extended_errcode(th3state *p, int iDb, char *zArg){
  int rc = sqlite3_extended_errcode(th3dbPointer(p,iDb));
  th3testAppendResultTerm(p, th3format(p, "0x%x", rc));
}

/*************************************************************************/
--testcase 100
PRAGMA journal_mode=WAL;
PRAGMA wal_autocheckpoint=0;
CREATE VIRTUAL TABLE nums USING wholenumber;
CREATE TABLE t1(x,y);
INSERT INTO t1(x,y)
  SELECT value, hex(randomblob(50)) FROM nums WHERE value BETWEEN 1 AND 100;
UPDATE t1 SET y='x'||y;
CREATE TABLE t2(z);
SELECT sum(length(y)) FROM t1;
--result wal 0 10100

--call wal36_set_readonly 2
--db 1
--open test.db
--testcase 200
SELECT sum(length(y)) FROM t1;
--result SQLITE_CANTOPEN {unable to open database file}

--open file:test.db?readonly_shm=1
--testcase 210
SELECT sum(length(y)) FROM t1;
--result 10100
--testcase 220
PRAGMA wal_checkpoint
--result SQLITE_READONLY {attempt to write a readonly database}
--testcase 230
INSERT INTO t1 VALUES(1,2);
--result SQLITE_READONLY {attempt to write a readonly database}

--db 0
--testcase 240
seek 96 read32_shm read32_shm read32_shm read32_shm read32_shm read32_shm
--shmedit test.db
--glob 0 0 # 4294967295 4294967295 4294967295
--testcase 241
seek 104 write32_shm -1
--shmedit test.db
--result
--db 1
--testcase 242
SELECT count(*), sum(length(y)) FROM t1;
--result SQLITE_READONLY {attempt to write a readonly database}

/* The extended result code SQLITE_READONLY_CANTLOCK is 0x208 */
--testcase 243
--call wal36_extended_errcode
--result 0x208

--testcase 300
--open file:test.db?readonly_shm=1
--ioerr shmlockbusy
SELECT count(x), sum(length(y)) FROM t1;
--result SQLITE_READONLY {attempt to write a readonly database}
--testcase 301
--call wal36_extended_errcode
--result 0x108
