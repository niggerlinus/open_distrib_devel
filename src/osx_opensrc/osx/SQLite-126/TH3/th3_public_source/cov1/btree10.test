/*
** Test module for btree.c and especially the ability of modifyPagePointer()
** to detect database corruption.
**
** SCRIPT_MODULE_NAME:        btree10
** REQUIRED_PROPERTIES:       TEST_VFS AUTOVACUUM CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY,y);
PRAGMA page_size;
--store $pgsz
--autocheckpoint 1
INSERT INTO t1 VALUES(1, zeroblob($pgsz*5));
INSERT INTO t1 VALUES(2, zeroblob($pgsz*5));
PRAGMA page_count;
SELECT x FROM t1;
--result 13 1 2

/* The x=2 entry should have a sequence of overflow pages.  The last page
** in the file will be the end page of the overflow chain.  The
** penultimate page of the file will be the penultimate page of the
** overflow chain.
**
** We will corrupt the chaining pointer for the penultimate overflow
** page.  Then delete the first entry.  The resulting auto_vacuum will
** detect the corruption while trying to rewrite pages.
*/
--testcase 110
SELECT $pgsz*11;
--store $ofst
seek $ofst read32
--edit test.db
--result 13

--testcase 120
seek $ofst write32 12 incr-chng
--edit test.db
DELETE FROM t1 WHERE x=1;
--result SQLITE_CORRUPT {database disk image is malformed}


/* Create a new database, autovacuumed, with one overflow cell but many
** other smaller cells.  Cause an autovacuum to have to remap the overflow
** chain.
*/
--new test.db
--testcase 200
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob($pgsz*5));
CREATE TABLE t2(y, z);
INSERT INTO t2 VALUES(1, null);
INSERT INTO t2 VALUES(2, null);
INSERT INTO t2 VALUES(3, null);
INSERT INTO t2 VALUES(4, zeroblob($pgsz*5));
INSERT INTO t2 VALUES(5, null);
INSERT INTO t2 VALUES(6, null);
INSERT INTO t2 VALUES(7, null);
DELETE FROM t1;
PRAGMA integrity_check;
--result ok

/*
** Create a new database such that an cell overflow pointer is at the very
** end of the page (where it is easy to locate.)  Corrupt that overflow
** pointer.  Provoke the autovacuum.  Verify that corruption is detected.
*/
--new test.db
--testcase 300
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob($pgsz*5));
CREATE TABLE t2(y, z);
INSERT INTO t2 VALUES(1, zeroblob($pgsz*4));
--result
--checkpoint

--testcase 301
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t2';
--store $root
seek $root move $pgsz move -4 add32 1 incr-chng
--edit test.db
DELETE FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}

/*
** Create a new database with child pages  Corrupt that right-child pointer
** Provoke the autovacuum.  Verify that corruption is detected.
*/
--new test.db
--testcase 400
--autocheckpoint 1
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(zeroblob($pgsz*5));
CREATE TABLE t2(y, z);
INSERT INTO t2 VALUES(1, zeroblob($pgsz/2));
INSERT INTO t2 VALUES(2, zeroblob($pgsz/2));
--result

--testcase 401
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t2';
--store $root
seek $root move 8 add32 1 incr-chng
--edit test.db
DELETE FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}
