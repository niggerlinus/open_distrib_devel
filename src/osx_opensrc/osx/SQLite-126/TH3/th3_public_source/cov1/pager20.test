/*
** Test module for pager.c.
**
** Corrupt the checksum on the master journal record of a rollback journal.
** Verify that this prevents consultation of the master journal file.
**
** SCRIPT_MODULE_NAME:        pager20
** REQUIRED_PROPERTIES:       TEST_VFS JOURNAL_DELETE CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB 
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a);
INSERT INTO t1 VALUES(1);
ATTACH DATABASE 'aux2.db' AS aux2;
CREATE TABLE aux2.t2(b);
INSERT INTO t2 VALUES(2);
SELECT * FROM t1, t2;
--result 1 2

--commit-snapshot
--testcase 110
BEGIN;
UPDATE t1 SET a=a+10;
UPDATE t2 SET b=b+20;
COMMIT;
--result

--revert
--open :memory:
--testcase 120
delete  # Causes a commit
--edit test.db-mj*
--open test.db
SELECT * FROM t1;
--result 11

--revert
--open :memory:
--testcase 130
end move -12 add32 1
--edit test.db-journal
delete  # does not cause commit because test.db-journal is corrupted
--edit test.db-mj*
--open test.db
SELECT * FROM t1;
--result 1

--revert
--open :memory:
--testcase 140
truncate 15  # causes a commit
--edit test.db-journal
--open test.db
SELECT * FROM t1;
--result 11
