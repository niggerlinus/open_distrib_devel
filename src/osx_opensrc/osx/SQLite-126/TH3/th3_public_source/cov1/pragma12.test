/*
** Module for testing pragmas.  Tests are written by hand from
** the documentation in pragma.html
**
** SCRIPT_MODULE_NAME:        pragma12
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
**
**************************************************************************
** PRAGMA reverse_unordered_selects;
** PRAGMA reverse_unordered_selects=BOOLEAN;
*/
--testcase rus-1
PRAGMA reverse_unordered_selects;
--result 0

--testcase rus-2
CREATE TABLE t1(x);
INSERT INTO t1 VALUES('a');
INSERT INTO t1 VALUES('z');
INSERT INTO t1 VALUES('r');
INSERT INTO t1 VALUES('w');
SELECT x FROM t1 ORDER BY x;
--result a r w z

--prepare_v2 1
--testcase rus-3
SELECT x FROM t1;
--store :u1
PRAGMA reverse_unordered_selects=ON;
SELECT x FROM t1;
--store :u2
SELECT :u1!=:u2
--result 1

--testcase rus-4
PRAGMA reverse_unordered_selects=OFF;
SELECT x FROM t1 WHERE x>'0';
--store :u1
PRAGMA reverse_unordered_selects=ON;
SELECT x FROM t1 WHERE x>'0';
--store :u2
SELECT :u1!=:u2
--result 1

--testcase rus-5
CREATE INDEX i1 ON t1(x);
PRAGMA reverse_unordered_selects=OFF;
SELECT x FROM t1 WHERE x>'0';
--store :u1
PRAGMA reverse_unordered_selects=ON;
SELECT x FROM t1 WHERE x>'0';
--store :u2
SELECT :u1!=:u2
--result 1

/*************************************************************************
** PRAGMA schema_version;
** PRAGMA schema_version=N;
** PRAGMA user_version;
** PRAGMA user_version=N;
*/
--new
--testcase sv-1
PRAGMA schema_version;
PRAGMA user_version;
--result 0 0

--testcase sv-2
CREATE TABLE t1(x);
PRAGMA schema_version;
PRAGMA user_version;
--result 1 0

--testcase sv-3
PRAGMA schema_version=123;
PRAGMA user_version=456;
--result

--testcase sv-4
PRAGMA schema_version;
PRAGMA user_version;
--result 123 456
