/*
** This module contains tests of the SELECT statement.
**
** The test cases in this module are prepared directly from high-level
** documentation and without reference to the underlying source code.
**
** The focus of this module is aggregate query statements.
**
** SCRIPT_MODULE_NAME:        select02
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a,b,c,d);
INSERT INTO t1 VALUES(1,'abc',9,5);
INSERT INTO t1 VALUES(2,'BCD',8,7);
INSERT INTO t1 VALUES(3,'cde',7,3);
INSERT INTO t1 VALUES(4,'DEF',6,5);
INSERT INTO t1 VALUES(5,'efg',5,3);
INSERT INTO t1 VALUES(6,'fgh',4,2);
SELECT count(*) FROM t1;
--result 6
--testcase 101
SELECT count(DISTINCT a), count(DISTINCT d) FROM t1;
--result 6 4

--testcase 110
SELECT d, count(a) FROM t1 GROUP BY d HAVING count(a)<100 ORDER BY 1, 2
--result 2 1 3 2 5 2 7 1
--testcase 111
SELECT d, count(a) AS cnt FROM t1 GROUP BY d HAVING cnt>1 ORDER BY 1, 2
--result 3 2 5 2

--testcase 120
SELECT d, count(DISTINCT (c%2)) AS cnt FROM t1 GROUP BY d ORDER BY d, cnt
--result 2 1 3 1 5 2 7 1
--testcase 121
SELECT d, count(DISTINCT (c%2)) AS cnt FROM t1 GROUP BY d ORDER BY cnt DESC, d
--result 5 2 2 1 3 1 7 1

--testcase 130
SELECT DISTINCT count(*) FROM t1 GROUP BY d ORDER BY 1;
--result 1 2
--testcase 131
SELECT DISTINCT count(*) AS cnt FROM t1;
--result 6
--testcase 132
SELECT DISTINCT count(*) AS cnt FROM t1 GROUP BY a;
--result 1
