/*
** Test module for btree.c.  
**
** Database corruption detected by moveToChild().
**
** Construct databases in which non-root pages have a cell count less
** than 1 or where child pages of a INTKEY table are non-INTKEY.
**
** SCRIPT_MODULE_NAME:        btree55
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB NO_OOM
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
PRAGMA page_size;
--store $pgsz
INSERT INTO t1 VALUES(1, zeroblob($pgsz/2));
INSERT INTO t1 VALUES(2, zeroblob($pgsz/2));
INSERT INTO t1 VALUES(3, zeroblob($pgsz/2));
--result
--checkpoint

/* The root is either page 1 or 2 (depending on whether or not AUTOVACUUM
** is on.)  Page 3 will always be a leaf page. */
--testcase 110
SELECT 2*$pgsz;
--store $root
seek $root move 3 write16 0 incr-chng
--edit test.db
SELECT a FROM t1 ORDER BY a;
--result SQLITE_CORRUPT {database disk image is malformed}

--testcase 120
seek $root write8 10 move 2 write16 1 incr-chng
--edit test.db
SELECT a FROM t1 ORDER BY a;
--result SQLITE_CORRUPT {database disk image is malformed}
