/*
** Test module for pager.c.
**
** OOM during the rollback of a transaction that was almost complete
** and which would have changed the page size had it completed.
**
** SCRIPT_MODULE_NAME:        pager37
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB JOURNAL_MEMORY JOURNAL_WAL ALT_PCACHE
** DISALLOWED_PROPERTIES:     AUTOVACUUM
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
INSERT INTO t1 VALUES(1, zeroblob(5000));
INSERT INTO t1 VALUES(2, zeroblob(10000));
INSERT INTO t1 VALUES(3, zeroblob(15000));
INSERT INTO t1 VALUES(4, zeroblob(20000));
INSERT INTO t1 VALUES(5, zeroblob(25000));
SELECT a, length(b) FROM t1 ORDER BY a;
--result 1 5000 2 10000 3 15000 4 20000 5 25000
--store $pgsz PRAGMA page_size

--commit-snapshot
--testcase 110
PRAGMA page_size=16384;
VACUUM;
SELECT a, length(b) FROM t1 ORDER BY a;
PRAGMA integrity_check;
PRAGMA page_size;
--result 1 5000 2 10000 3 15000 4 20000 5 25000 ok 16384

--testcase 200
--oom
--revert
--raw-open test.db
SELECT a, length(b) FROM t1 ORDER BY a;
PRAGMA integrity_check;
--result 1 5000 2 10000 3 15000 4 20000 5 25000 ok


/* The following sequence of tests attempts to get the pager_truncate()
** routine to extend a file by an amount that is less than the current
** page size.
*/
--raw-new test.db
--testcase 400
PRAGMA page_size=2048;
CREATE TABLE t1(x,y);
INSERT INTO t1 VALUES('abcde',randomblob(1200));
--store $md5sum
--result

--commit-snapshot
--testcase 410
PRAGMA page_size=1024;
VACUUM;
--result

--testcase 420
--oom
--revert
--raw-open test.db
SELECT x, length(y) FROM t1;
PRAGMA integrity_check;
--result abcde 1200 ok

--testcase 430
--ioerr
--revert
--raw-open test.db
SELECT x, length(y) FROM t1;
PRAGMA integrity_check;
--result abcde 1200 ok
