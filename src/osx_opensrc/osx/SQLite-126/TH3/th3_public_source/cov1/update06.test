/*
** This module contains tests for the UPDATE statement.
**
** Updates of columns added by ALTER TABLE.
**
** SCRIPT_MODULE_NAME:        update06
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 100
CREATE TABLE t1(a);
INSERT INTO t1 VALUES(1);
INSERT INTO t1 VALUES(2);
INSERT INTO t1 SELECT a+2 FROM t1;
INSERT INTO t1 SELECT a+4 FROM t1;
INSERT INTO t1 SELECT a+8 FROM t1;
ALTER TABLE t1 ADD COLUMN b INTEGER DEFAULT 999;
ALTER TABLE t1 ADD COLUMN c TEXT DEFAULT 'empty';
ALTER TABLE t1 ADD COLUMN d REAL DEFAULT 9e99;
ALTER TABLE t1 ADD COLUMN e BLOB DEFAULT x'012345';
SELECT b, c, d, typeof(d), hex(e) FROM t1 WHERE a=7;
--result 999 empty 9.0e+99 real 012345

--testcase 101
UPDATE t1 SET b=2 WHERE a=5;
SELECT b, c, d, hex(e) FROM t1 WHERE a=5;
--result 2 empty 9.0e+99 012345

--testcase 102
UPDATE t1 SET c='hello' WHERE a=5;
SELECT b, c, d, hex(e) FROM t1 WHERE a=5;
--result 2 hello 9.0e+99 012345

--testcase 103
UPDATE t1 SET d=123.5 WHERE a=5;
SELECT b, c, d, hex(e) FROM t1 WHERE a=5;
--result 2 hello 123.5 012345

--testcase 104
UPDATE t1 SET e=x'abcdef' WHERE a=5;
SELECT b, c, d, hex(e) FROM t1 WHERE a=5;
--result 2 hello 123.5 ABCDEF
#endif
