/*
** This module contains tests for the DELETE statement.
**
** These test are written from user documentation.
**
** SCRIPT_MODULE_NAME:        delete02
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* The combination of ALT_PCACHE_STRESS and JOURNAL_WAL results in 
** excess TEST_VFS disk-space usage.  So disable that case.
*/
--if $property_ALT_PCACHE_STRESS AND $property_JOURNAL_WAL
--testcase setup-1
PRAGMA journal_mode=DELETE;
--result delete
--endif


/* Verify that trigger fire before and after each row
*/
--testcase 100
CREATE TABLE log(x);
CREATE TABLE t1(a INTEGER PRIMARY KEY,b,c);
INSERT INTO t1 VALUES(1,2,3);
INSERT INTO t1 VALUES(7,8,9);
CREATE TRIGGER r1 BEFORE DELETE ON t1 BEGIN
  INSERT INTO log VALUES('before:' || old.rowid);
END;
CREATE TRIGGER r2 AFTER DELETE ON t1 BEGIN
  INSERT INTO log VALUES('after:' || old.rowid);
END;
--result

--testcase 105
DELETE FROM t1 WHERE a>55;
SELECT x FROM log ORDER BY rowid;
SELECT changes();
--result 0

--testcase 110
DELETE FROM t1 WHERE a>5;
SELECT x FROM log ORDER BY rowid;
SELECT changes();
--result before:7 after:7 1


--testcase 120
DELETE FROM log;
DELETE FROM t1;
SELECT x FROM log ORDER BY rowid;
SELECT changes();
--result before:1 after:1 1


/* Verify that DELETEs that occur within a trigger also work.  Here we
** create a large table t1 (2048 entries) and a mirror table named "other".
** Whenever a row is deleted from t1, the corresponding row is automatically
** deleted from "other".
**
** The change count only applies to the deletes in the outer DELETE statement,
** not to the deletes that occur within the trigger.  But
** total_changes is cummulative.  Note that total_changes() also counts
** the INSERTs into the log table.
*/
--testcase 200
DELETE FROM log;
CREATE TABLE other(x INTEGER PRIMARY KEY);
CREATE TRIGGER r3 BEFORE DELETE ON t1 BEGIN
  DELETE FROM other WHERE x=old.rowid;
END;  
CREATE INDEX i1ab ON t1(a,b);
INSERT INTO t1 VALUES(11,22,33);
INSERT INTO t1 VALUES(12,34,56);
INSERT INTO t1 SELECT a+2, b+100, c+200 FROM t1;
INSERT INTO t1 SELECT a+4, b+200, c+400 FROM t1;
INSERT INTO t1 SELECT a+8, b+400, c+800 FROM t1;
INSERT INTO t1 SELECT a+16, b+800, c+1600 FROM t1;
INSERT INTO t1 SELECT a+32, b+1600, c+3200 FROM t1;
INSERT INTO t1 SELECT a+64, b+3200, c+6400 FROM t1;
INSERT INTO t1 SELECT a+128, b+6400, c+12800 FROM t1;
INSERT INTO t1 SELECT a+256, b+12800, c+25600 FROM t1;
INSERT INTO t1 SELECT a+512, b+25600, c+51200 FROM t1;
INSERT INTO t1 SELECT a+1024, b+51200, c+102400 FROM t1;
INSERT INTO other SELECT a FROM t1;
CREATE TABLE saved_t1 AS SELECT * FROM t1;
SELECT total_changes();
--store :baseline
PRAGMA count_changes=ON;
DELETE FROM t1;
PRAGMA count_changes=OFF;
SELECT count(*) FROM log;
SELECT changes();
SELECT count(*) FROM other;
SELECT total_changes()-:baseline;
--result 2048 4096 2048 0 8192

/* DROP statements, even though they are translated into a DELETE
** against the sqlite_master table, do not modify any of the change
** counters.
*/
--testcase 210
DELETE FROM t1;  -- reset the changes() counter to zero.
SELECT total_changes();
--store :baseline
PRAGMA count_changes=ON;
DROP TRIGGER r3;
SELECT changes(), total_changes()-:baseline;
DROP TABLE other;
SELECT changes(), total_changes()-:baseline;
PRAGMA count_changes=OFF;
--result 0 0 0 0

/* Create a view of table t1
*/
--testcase 300
CREATE VIEW v1 AS SELECT a, b+c FROM t1;
INSERT INTO t1 SELECT * FROM saved_t1;
SELECT count(*) FROM v1;
--result 2048

/* We cannot delete from a view
*/
--testcase 310
DELETE FROM v1 WHERE a=123;
--result SQLITE_ERROR {cannot modify v1 because it is a view}

/* But, if we can create an INSTEAD OF trigger against a view that
** will do the delete for us.
*/
--testcase 320
CREATE TRIGGER r4 INSTEAD OF DELETE ON v1 BEGIN
  DELETE FROM t1 WHERE a=old.a;
END;
DELETE FROM v1 WHERE a=123;
SELECT count(*) FROM t1;
--result 2047

/* Here we modify the INSTEAD OF trigger so that whenever a row
** is deleted from the view, two rows are deleted from the underlying
** table.  Verify that count_changes pragma only count the view deletes,
** that changes() only counts top-level table row deletes, and that
** that total_changes() sees all real table deletes (but not view row
** deletes).
*/
--testcase 330
DROP TRIGGER r1;
DROP TRIGGER r2;
DROP TRIGGER r4;
CREATE TRIGGER r4 INSTEAD OF DELETE ON v1 BEGIN
  DELETE FROM t1 WHERE a=old.a OR a=old.a+1;
END;
SELECT total_changes();
--store :baseline
PRAGMA count_changes=ON;
DELETE FROM v1 WHERE a=124;
PRAGMA count_changes=OFF;
SELECT changes(), total_changes()-:baseline;
SELECT count(*) FROM t1;
--result 1 0 2 2045
