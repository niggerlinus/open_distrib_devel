/*
** Test module for btree.c.  
**
** Database corruption detected by moveToChild().  In this instance, construct
** a btree that is deeper than BTCURSOR_MAX_DEPTH (22) and verify that that
** moveToChild recognizes that the maximum depth is exceeded.
**
** SCRIPT_MODULE_NAME:        btree54
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB NO_OOM AUTOVACUUM INCRVACUUM JOURNAL_WAL
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
INSERT INTO t1 VALUES(1, 1);
PRAGMA page_size=512;
VACUUM;
PRAGMA page_size;
--result 512

--testcase 110
INSERT INTO t1 VALUES(2, zeroblob(512*30));
DELETE FROM t1 WHERE a=2;
PRAGMA freelist_count;
SELECT rootpage FROM sqlite_master WHERE name='t1';
--result 30 2

--testcase 120
seek 512 write 0500000002003400 write32 3 
move 500 write 0500000002003400 write32 4
move 500 write 0500000002003400 write32 5
move 500 write 0500000002003400 write32 6
move 500 write 0500000002003400 write32 7
move 500 write 0500000002003400 write32 8
move 500 write 0500000002003400 write32 9
move 500 write 0500000002003400 write32 10
move 500 write 0500000002003400 write32 11
move 500 write 0500000002003400 write32 12
move 500 write 0500000002003400 write32 13
move 500 write 0500000002003400 write32 14
move 500 write 0500000002003400 write32 15
move 500 write 0500000002003400 write32 16
move 500 write 0500000002003400 write32 17
move 500 write 0500000002003400 write32 18
move 500 write 0500000002003400 write32 19
move 500 write 0500000002003400 write32 20
move 500 write 0500000002003400 write32 21
move 500 write 0500000002003400 write32 22
move 500 write 0500000002003400 write32 23
move 500 write 0500000002003400 write32 24
move 500 write 0500000002003400 write32 25
move 500 write 0500000002003400 write32 26
move 500 write 0500000002003400 write32 27
move 500 write 0500000002003400 write32 28
incr-chng
--edit test.db
SELECT max(a) FROM t1;
--result SQLITE_CORRUPT {database disk image is malformed}
