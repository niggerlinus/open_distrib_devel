/*
** Test module for pager.c.
**
** MIXED_MODULE_NAME:         pager19
** REQUIRED_PROPERTIES:       TEST_VFS JOURNAL_DELETE CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB 
** MINIMUM_HEAPSIZE:          100000
*/

/* The soft heap limit can cause premature cache spills which will
** mess up this test.  So disable it.
*/
void pager19_disable_heap_limit(th3state *p, int iDb, char *zArg){
  sqlite3_soft_heap_limit(0);
}
--call pager19_disable_heap_limit
--testcase 100
CREATE TABLE t1(x INTEGER PRIMARY KEY, y);
INSERT INTO t1 VALUES(1, th3randomBlob(140000));
--result

--commit-snapshot
--testcase 110
UPDATE t1 SET y=NULL;
--result

--revert
--open :memory:
--testcase 120
read 8  # Verify that the journal header exists
--edit test.db-journal
--result d9d505f920a163d7

--testcase 130
seek 1 add8 1     # Corrupt the rollback journal header
--edit test.db-journal
--open test.db
SELECT x, length(y) FROM t1;   -- Rollback does not occur
--result 1 nil

--revert
--open :memory:
--testcase 200
read 8
--edit test.db-journal
--result d9d505f920a163d7

--testcase 210
--open test.db
SELECT x, length(y) FROM t1;   -- Header unmolested.  Rollback does occur
--result 1 140000

/* Change the pagesize field at offset 24 of the rollback journal to
** zero.  The rollback should still occur.
*/
--revert
--open :memory:
--testcase 220
seek 24 write32 0
--edit test.db-journal
--open test.db
SELECT x, length(y) FROM t1;
--result 1 140000

/*
** Tests to insure that truncating an incomplete (and unsynchronized)
** rollback journal does not cause corruption.  A powerloss can cause
** a rollback journal to be truncated if the powerloss occurs before
** the journal is synchronized.  But before the journal is synchronized,
** no changes are written to the database.  So simplying ignoring the
** journal is sufficient recovery.  That is what SQlite does.  Verify
** that this works.
**
** If ALT_PCACHE_STRESS is enabled, the journal might be synchronized
** sooner than we expect, so disable this test for that case.
*/
--if NOT $property_ALT_PCACHE_STRESS
--testcase 300
BEGIN;
UPDATE t1 SET y=zeroblob(139999);
SELECT x, length(y) FROM t1;
--result 1 139999

--snapshot
--open :memory:
--revert
--open :memory:
--testcase 310
CREATE VIRTUAL TABLE fs USING vfs;
SELECT sz FROM fs WHERE zfilename='test.db-journal';
--store $jsize
--store $newsize SELECT $jsize-1
write d9d505f920a163d7000000ff
truncate $newsize
--edit test.db-journal
--open test.db
SELECT x, length(y) FROM t1;  -- No changes applied to the database
PRAGMA integrity_check;       -- Everything still valid.
--result 1 140000 ok

--revert
--open :memory:
--testcase 320
--store $newsize SELECT $jsize-10
write d9d505f920a163d7000000ff
truncate $newsize
--edit test.db-journal
--open test.db
SELECT x, length(y) FROM t1;  -- No changes applied to the database
PRAGMA integrity_check;       -- Everything still valid.
--result 1 140000 ok

--revert
--open :memory:
--testcase 330
--store $newsize SELECT $jsize-100
write d9d505f920a163d7000000ff
truncate $newsize
--edit test.db-journal
--open test.db
SELECT x, length(y) FROM t1;  -- No changes applied to the database
PRAGMA integrity_check;       -- Everything still valid.
--result 1 140000 ok

--revert
--open :memory:
--testcase 340
write d9d505f920a163d7000000ff
truncate 2048
--edit test.db-journal
--open test.db
SELECT x, length(y) FROM t1;  -- No changes applied to the database
PRAGMA integrity_check;       -- Everything still valid.
--result 1 140000 ok
