/*
** Module for testing pragmas.  Tests are written by hand from
** the documentation in pragma.html
**
** This test module focuses on the temp_store pragma.
**
** MODULE_NAME:               pragma13
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/


int pragma13(th3state *p){
  sqlite3_stmt *pStmt;  /* A prepared statement */
  int rc;               /* return code from SQLite or TH3 interface */

  th3dbNew(p, 0, "test.db");
  th3testBegin(p, "1");
  th3dbEval(p, 0, "PRAGMA temp_store=DEFAULT; PRAGMA temp_store;");
  th3testCheck(p, "0");

  th3testBegin(p, "2");
  th3dbEval(p, 0, "PRAGMA temp_store=FILE; PRAGMA temp_store;");
  th3testCheck(p, "1");

  th3testBegin(p, "3");
  th3dbEval(p, 0, "PRAGMA temp_store=MEMORY; PRAGMA temp_store;");
  th3testCheck(p, "2");

  th3testBegin(p, "4");
  th3dbEval(p, 0, "PRAGMA temp_store=0; PRAGMA temp_store;");
  th3testCheck(p, "0");

  th3testBegin(p, "5");
  th3dbEval(p, 0, "PRAGMA temp_store(1); PRAGMA temp_store;");
  th3testCheck(p, "1");

  th3testBegin(p, "6");
  th3dbEval(p, 0, "PRAGMA temp_store=2; PRAGMA temp_store;");
  th3testCheck(p, "2");

  th3testBegin(p, "7");
  th3dbEval(p, 0, "PRAGMA temp_store=3; PRAGMA temp_store;");
  th3testCheck(p, "0");

  th3testBegin(p, "8");
  th3dbEval(p, 0, "PRAGMA temp_store(unknown); PRAGMA temp_store;");
  th3testCheck(p, "0");

  th3testBegin(p, "9");
  th3dbEval(p, 0, "PRAGMA temp_store(-1); PRAGMA temp_store;");
  th3testCheck(p, "0");

#if !defined(SQLITE_TEMP_STORE) || SQLITE_TEMP_STORE>0
  th3testBegin(p, "11");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, "CREATE TABLE tmain(a);");
  p->fsysTest.nOpen = 0;
  th3dbEval(p, 0, "PRAGMA temp_store=MEMORY; CREATE TEMP TABLE t1(x);");
  th3testCheckInt(p, 0, p->fsysTest.nOpen);
#endif

#if !defined(SQLITE_TEMP_STORE) || SQLITE_TEMP_STORE<3
  th3testBegin(p, "12");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, "CREATE TABLE tmain(a);");
  p->fsysTest.nOpenDb = 0;
  th3dbEval(p, 0, "PRAGMA temp_store=FILE; CREATE TEMP TABLE t1(x);");
  th3testCheckInt(p, 1, p->fsysTest.nOpenDb);
#endif

#if !defined(SQLITE_TEMP_STORE) || SQLITE_TEMP_STORE==1 || SQLITE_TEMP_STORE==2
  th3testBegin(p, "13");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, "CREATE TABLE tmain(a);");
  p->fsysTest.nOpenDb = 0;
  th3dbEval(p, 0, "PRAGMA temp_store=DEFAULT; CREATE TEMP TABLE t1(x);");
#if !defined(SQLITE_TEMP_STORE) || SQLITE_TEMP_STORE==1
  th3testCheckInt(p, 1, p->fsysTest.nOpenDb);
#else
  th3testCheckInt(p, 0, p->fsysTest.nOpenDb);
#endif
#endif

  th3testBegin(p, "21");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0,
     "CREATE TEMP TABLE x(a);"
     "PRAGMA temp_store=1;"
     "SELECT name FROM sqlite_temp_master;"
  );
  th3testCheck(p, "");

  th3testBegin(p, "22");
  th3dbEval(p, 0,
     "BEGIN;"
     "CREATE TEMP TABLE x(a);"
     "PRAGMA temp_store=2;"
     "SELECT name FROM sqlite_temp_master;"
  );
  th3testCheck(p, "SQLITE_ERROR {temporary storage cannot be changed from within a transaction}");

  th3dbNew(p, 0, "test.db");
  th3testBegin(p, "23");
  th3dbEval(p, 0,
     "CREATE TEMP TABLE x(a);"
     "INSERT INTO x VALUES(1);"
     "INSERT INTO x VALUES(2);"
  );
  pStmt = th3dbPrepare(p, 0, "SELECT a FROM x");
  assert( pStmt );
  rc = sqlite3_step(pStmt);
  assert( rc==SQLITE_ROW );
  th3dbEval(p, 0,
     "PRAGMA temp_store=2;"
  );
  sqlite3_finalize(pStmt);
  th3testCheck(p, "SQLITE_ERROR {temporary storage cannot be changed from within a transaction}");

  return 0;
}
