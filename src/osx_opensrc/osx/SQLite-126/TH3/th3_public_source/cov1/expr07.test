/*
** This module contains tests of expression handling.
**
** The focus of this module is sqlite3ExpDup() and its subroutines.
**
** SCRIPT_MODULE_NAME:        expr07
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(x,y);
CREATE TRIGGER r1 AFTER INSERT ON t1
WHEN new.x+new.y>10
BEGIN
  SELECT raise(abort,'cannot insert');
END;
INSERT INTO t1 VALUES(5,6);
--result SQLITE_CONSTRAINT {cannot insert}

#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 110
DROP TRIGGER r1;
CREATE TRIGGER r1 AFTER INSERT ON t1
WHEN CAST(new.x AS integer)=new.y
BEGIN
  SELECT raise(abort,'cannot insert');
END;
INSERT INTO t1 VALUES(5.5,5);
--result SQLITE_CONSTRAINT {cannot insert}
#endif

--testcase 120
DROP TRIGGER r1;
CREATE TRIGGER r1 AFTER INSERT ON t1
WHEN new.x=new.y COLLATE nocase
BEGIN
  SELECT raise(abort,'cannot insert');
END;
INSERT INTO t1 VALUES('hello','Hello');
--result SQLITE_CONSTRAINT {cannot insert}

--testcase 130
CREATE TABLE translate(x,y);
INSERT INTO translate VALUES(11,13);
CREATE TABLE log(n);
DROP TRIGGER r1;
CREATE TRIGGER r1 AFTER INSERT ON t1
BEGIN
  INSERT INTO log VALUES((SELECT y FROM translate WHERE x=new.x));
END;
INSERT INTO t1 VALUES(11,22);
SELECT * FROM log;
--result 13

--testcase 140
DELETE FROM log;
DELETE FROM t1;
DROP TRIGGER r1;
INSERT INTO t1 VALUES('abc',111);
INSERT INTO t1 VALUES('DEF',222);
CREATE TRIGGER r1 AFTER INSERT ON t1
BEGIN
  INSERT INTO log SELECT x FROM t1 ORDER BY 1 COLLATE binary LIMIT 1;
END;
INSERT INTO t1 VALUES('xyz',333);
SELECT * FROM log;
--result DEF
--testcase 141
DELETE FROM log;
DELETE FROM t1;
DROP TRIGGER r1;
INSERT INTO t1 VALUES('abc',111);
INSERT INTO t1 VALUES('DEF',222);
CREATE TRIGGER r1 AFTER INSERT ON t1
BEGIN
  INSERT INTO log SELECT x FROM t1 ORDER BY 1 COLLATE nocase LIMIT 1;
END;
INSERT INTO t1 VALUES('xyz',333);
SELECT * FROM log;
--result abc

--testcase 150
DELETE FROM log;
DELETE FROM t1;
DROP TRIGGER r1;
--run 0
--oom
CREATE TRIGGER r1 AFTER INSERT ON t1
BEGIN
  INSERT INTO log SELECT CASE WHEN new.x=1 THEN 'one' ELSE 'other' END;
END;
--result
--testcase 151
INSERT INTO t1 VALUES(1,'xyzzy');
INSERT INTO t1 VALUES(2,'fuzzy');
SELECT * FROM log;
--result one other
