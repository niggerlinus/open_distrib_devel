/*
** This module contains tests of the sqlite3_status() interface for
** structural test coverage.
**
** MODULE_NAME:               status01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
int status01(th3state *p){
  int iCurrent;
  int iHighwater;
  int iCur2, iHi2, iHi3;
  int rc;
  sqlite3 *db;
  char *zBuf;

  zBuf = th3malloc(p, 1000);

  th3testBegin(p, "1");
  iCurrent = -1;
  iHighwater = -1;
  rc = sqlite3_status(-1, &iCurrent, &iHighwater, 1);
  th3testAppendResult(p, th3format(p, "%d %d %s",
    iCurrent, iHighwater, th3errorCodeName(rc)
  ));
  th3testCheck(p, "-1 -1 SQLITE_MISUSE");

  th3testBegin(p, "2");
  iCurrent = -1;
  iHighwater = -1;
  rc = sqlite3_status(10, &iCurrent, &iHighwater, 1);
  th3testAppendResult(p, th3format(p, "%d %d %s",
    iCurrent, iHighwater, th3errorCodeName(rc)
  ));
  th3testCheck(p, "-1 -1 SQLITE_MISUSE");

  th3testBegin(p, "11");
  th3dbNew(p, 0, "test.db");
  th3dbOpen(p, 0, "test.db", TH3_OPEN_RAW);
  db = th3dbPointer(p, 0);
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_LOOKASIDE-1);
  th3testAppendResult(p, th3errorCodeName(rc));
  th3testCheck(p, "SQLITE_ERROR");

  th3testBegin(p, "12");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_TRIGGER+1);
  th3testAppendResult(p, th3errorCodeName(rc));
  th3testCheck(p, "SQLITE_ERROR");

  th3testBegin(p, "13");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_LOOKASIDE, zBuf, 100, 10);
  th3testAppendResult(p, th3errorCodeName(rc));
  th3testCheck(p, "SQLITE_OK");

  th3testBegin(p, "101");
  iCurrent = -1;
  iHighwater = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_USED-1, 
                         &iCurrent, &iHighwater, 1);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCurrent, iHighwater, th3errorCodeName(rc)
  ));
  th3testCheck(p, "-1 -1 SQLITE_ERROR");

  th3testBegin(p, "102");
  iCurrent = -1;
  iHighwater = -1;
  th3dbEval(p, 0, "CREATE TABLE t1(x); DROP TABLE t1;");
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_USED, 
                         &iCurrent, &iHighwater, 0);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCurrent>=0, iHighwater>iCurrent, th3errorCodeName(rc)
  ));
  th3testCheck(p, "1 1 SQLITE_OK");
  th3testBegin(p, "102b");
  iCur2 = -1;
  iHi2 = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_HIT, &iCur2, &iHi2, 0);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCur2, iHi2>0, th3errorCodeName(rc)
  ));
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_HIT, &iCur2, &iHi3, 1);
  th3testAppendResult(p, th3format(p, " %d %d %s", 
     iCur2, iHi2==iHi3, th3errorCodeName(rc)
  ));
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_HIT, &iCur2, &iHi2, 0);
  th3testAppendResult(p, th3format(p, " %d %d %s", 
     iCur2, iHi2, th3errorCodeName(rc)
  ));
  th3testCheck(p, "0 1 SQLITE_OK 0 1 SQLITE_OK 0 0 SQLITE_OK");
  th3testBegin(p, "102c");
  iCur2 = -1;
  iHi2 = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_MISS_SIZE,
                         &iCur2, &iHi2, 0);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCur2, iHi2>0, th3errorCodeName(rc)
  ));
  th3testCheck(p, "0 1 SQLITE_OK");
  th3testBegin(p, "102d");
  iCur2 = -1;
  iHi2 = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_MISS_FULL,
                         &iCur2, &iHi2, 0);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCur2, iHi2>0, th3errorCodeName(rc)
  ));
  th3testCheck(p, "0 1 SQLITE_OK");

  th3testBegin(p, "103");
  iCurrent = -1;
  iHighwater = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_USED,
                         &iCurrent, &iHighwater, 1);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCurrent>=0, iHighwater>iCurrent, th3errorCodeName(rc)
  ));
  th3testCheck(p, "1 1 SQLITE_OK");

  th3testBegin(p, "104");
  iCurrent = -1;
  iHighwater = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_LOOKASIDE_USED,
                         &iCurrent, &iHighwater, 0);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCurrent>=0, iHighwater==iCurrent, th3errorCodeName(rc)
  ));
  th3testCheck(p, "1 1 SQLITE_OK");

#ifdef SQLITE_DBSTATUS_CACHE_USED
  th3testBegin(p, "110");
  iCurrent = -1;
  iHighwater = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_CACHE_USED,
                         &iCurrent, &iHighwater, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "111");
  th3testCheckInt(p, 1, iCurrent>100);
  th3testBegin(p, "112");
  th3testCheckInt(p, 0, iHighwater);

  th3testBegin(p, "120");
  th3dbEval(p, 0,
     "CREATE TABLE t1(x); INSERT INTO t1 VALUES(123);"
  );
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_CACHE_USED,
                         &iCurrent, &iHighwater, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "121");
  th3testCheckInt(p, 1, iCurrent>300);
  th3testBegin(p, "122");
  th3testCheckInt(p, 0, iHighwater);

  th3testBegin(p, "130");
  th3dbEval(p, 0,
     "CREATE TEMP TABLE ttemp(a,b,c);"
     "ATTACH ':memory:' AS aux1;"
     "CREATE TABLE aux1.taux(d,e,f);"
  );
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_CACHE_USED,
                         &iCurrent, &iHighwater, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "131");
  th3testCheckInt(p, 1, iCurrent>1000);
  th3testBegin(p, "132");
  th3testCheckInt(p, 0, iHighwater);
#endif /* SQLITE_DBSTATUS_CACHE_USED */

/* Prior to the introduction of the SQLITE_DBSTATUS_MAX macro, the maximum
** value was SQLITE_DBSTATUS_LOOKASIDE_USED
*/
#ifndef SQLITE_DBSTATUS_MAX
# define SQLITE_DBSTATUS_MAX SQLITE_DBSTATUS_LOOKASIDE_USED
#endif


  th3testBegin(p, "201");
  iCurrent = -1;
  iHighwater = -1;
  rc = sqlite3_db_status(db, SQLITE_DBSTATUS_MAX+1, 
                         &iCurrent, &iHighwater, 1);
  th3testAppendResult(p, th3format(p, "%d %d %s", 
     iCurrent==(-1), iHighwater==(-1), th3errorCodeName(rc)
  ));
  th3testCheck(p, "1 1 SQLITE_ERROR");

  th3dbCloseAll(p);
  th3free(p, zBuf);
  return 0;
}
