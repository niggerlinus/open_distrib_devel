/*
** This module contains tests of vdbeaux.c source module.
**
** SCRIPT_MODULE_NAME:        vdbeaux01
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
**
** The output of EXPLAIN is not defined and it shifts from one release to
** the next.  So we cannot really make persistent tests for the EXPLAIN
** output other than to simply run some EXPLAINs and make sure there are
** no memory leaks or assertion faults.
*/
--testcase 100
CREATE TABLE output(rownum,colname,value);
--run 0
EXPLAIN CREATE TABLE t1(x);
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 110
CREATE TABLE t1(a,b,c);
CREATE INDEX t1abc ON t1(a COLLATE NOCASE ASC, b DESC, c ASC);
EXPLAIN
   SELECT * FROM t1
    WHERE a='xyz' COLLATE nocase
      AND b=12345678901234
    ORDER BY c;
--table output
SELECT count(*)>0 FROM output WHERE colname='p4' AND value LIKE 'keyinfo%';
DELETE FROM output;
--result 1
--testcase 111
EXPLAIN
   SELECT max(a,b,c) FROM t1
    WHERE a='xyz'
#ifndef SQLITE_OMIT_FLOATING_POINT
      AND b=12345678.901234
#endif
#ifdef SQLITE_OMIT_FLOATING_POINT
      AND b=12345678
#endif
    ORDER BY c;
--table output
SELECT count(*)>0 FROM output WHERE colname='p4' AND value LIKE 'keyinfo%';
DELETE FROM output;
--result 1

--testcase 120
CREATE TABLE t2(a,b,c,d,e,f,g,h,i,j,k,l,m INTEGER PRIMARY KEY);
CREATE INDEX t2all ON t2(a,b,m,c,d,e,f,g,m,h,i,j,k,l,a,b,c,d,e,f,g,h);
EXPLAIN
  SELECT * FROM t2 WHERE a=?;
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 130
CREATE VIRTUAL TABLE t3 USING bvs;
EXPLAIN SELECT * FROM t3;
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 140
CREATE TABLE t4(
  a DEFAULT 'text',
  b DEFAULT 123,
#ifdef SQLITE_OMIT_FLOATING_POINT
  c DEFAULT 45,
#endif
#ifndef SQLITE_OMIT_FLOATING_POINT
  c DEFAULT 4.5,
#endif
  d DEFAULT x'b10b'
);
EXPLAIN UPDATE t4 SET rowid=rowid+1;
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 150
CREATE TABLE t5(a, b, UNIQUE(a,b));
CREATE INDEX t5ba ON t5(b,a);
EXPLAIN
  SELECT a, b FROM t5
  UNION 
  SELECT b, a FROM t5
  ORDER BY 1, 2;
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 160
EXPLAIN SELECT rowid FROM t1 WHERE rowid IN (SELECT rowid+0 FROM t1);
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 170
EXPLAIN SELECT 1,2,3,4 UNION SELECT 5,6,7,8;
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 180
EXPLAIN
   SELECT DISTINCT a,b,1,c FROM t2
   UNION ALL
   SELECT d,e,2,f FROM t2
   ORDER BY 1,2,3,4;
--table output
SELECT count(*)>0 FROM output;
DELETE FROM output;
--result 1

--testcase 200
CREATE TABLE t200a(a,b,c);
CREATE TABLE t200b(x,y,z);
CREATE TABLE t200c(p,q,r);
CREATE TRIGGER t200r1 AFTER INSERT ON t200a BEGIN
  INSERT INTO t200b VALUES(new.b,new.c,new.a);
END;
CREATE TRIGGER t200r2 AFTER INSERT ON t200b BEGIN
  INSERT INTO t200c VALUES(new.y,new.z,new.x);
END;
--result

--testcase 201
EXPLAIN INSERT INTO t200a VALUES(1,2,3);
--table output
SELECT count(*) FROM output WHERE colname='addr' AND value=0;
DELETE FROM output;
--result 3

--testcase 202
EXPLAIN QUERY PLAN INSERT INTO t200a VALUES(10,20,30);
--result

--testcase 210
DROP TRIGGER t200r1;
CREATE TRIGGER t200r1 AFTER INSERT ON t200a BEGIN
  INSERT INTO t200b VALUES(new.a,new.b,new.c);
  INSERT INTO t200b VALUES(new.b,new.c,new.a);
  INSERT INTO t200b VALUES(new.c,new.a,new.b);
END;
--result

--testcase 211
EXPLAIN INSERT INTO t200a VALUES(1,2,3);
--table output
SELECT count(*) FROM output WHERE colname='addr' AND value=0;
DELETE FROM output;
--result 3
