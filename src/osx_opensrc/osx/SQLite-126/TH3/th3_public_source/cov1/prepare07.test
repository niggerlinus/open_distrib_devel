/*
** This module contains tests of prepare.c source module.
**
** MODULE_NAME:               prepare07
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
int prepare07(th3state *p){
  int rc;
  sqlite3 *db;
  sqlite3_stmt *pStmt;
  unsigned short int *pBuf;
  unsigned short int *pTail;
  unsigned short int *pTrueTail;

  th3testBegin(p, "1");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  sqlite3_limit(db, SQLITE_LIMIT_SQL_LENGTH, 200);
  rc = sqlite3_prepare(db,   
    "SELECT '9 123456789 123456789 123456789 123456789 123456789 "
    "123456789 123456789 123456789 123456789 123456789 123456789 "
    "123456789 123456789 123456789 123456789 123456789 123456789 "
    "123456789 12345678'       ",
    200, &pStmt, 0
  );
  th3testCheckInt(p, SQLITE_OK, rc);

  th3testBegin(p, "2");
  sqlite3_finalize(pStmt);
  rc = sqlite3_prepare(db,   
    "SELECT '9 123456789 123456789 123456789 123456789 123456789 "
    "123456789 123456789 123456789 123456789 123456789 123456789 "
    "123456789 123456789 123456789 123456789 123456789 123456789 "
    "123456789 12345678'       ",
    201, &pStmt, 0
  );
  th3testCheckInt(p, SQLITE_TOOBIG, rc);
  sqlite3_finalize(pStmt);

  th3oomBegin(p, "3");
  while( th3oomNext(p) ){
    const char *zTail;
    th3oomEnable(p, 1);
                          /*  123456789 */
    rc = sqlite3_prepare(db, "SELECT 1234", 9, &pStmt, &zTail);
    th3oomEnable(p, 0);
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM );
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      rc = sqlite3_step(pStmt);
      th3oomCheck(p, 3, rc==SQLITE_ROW);
      rc = sqlite3_column_int(pStmt, 0);
      th3oomCheck(p, 4, rc==12);
      th3oomCheck(p, 5, strcmp(zTail, "34")==0);
    }
    sqlite3_finalize(pStmt);
  }
  th3oomEnd(p);

  th3testBegin(p, "100");
  rc = sqlite3_prepare(db, "SELECT 1", 0, &pStmt, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  sqlite3_finalize(pStmt);

  th3testBegin(p, "200");
  rc = sqlite3_prepare(db, "SELECT 12", 9, &pStmt, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "201");
  rc = sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_ROW, rc);
  th3testBegin(p, "202");
  rc = sqlite3_column_int(pStmt, 0);
  th3testCheckInt(p, 12, rc);
  sqlite3_finalize(pStmt);

  th3testBegin(p, "210");
  rc = sqlite3_prepare(db, "SELECT 12", 10, &pStmt, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "211");
  rc = sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_ROW, rc);
  th3testBegin(p, "212");
  rc = sqlite3_column_int(pStmt, 0);
  th3testCheckInt(p, 12, rc);
  sqlite3_finalize(pStmt);

  th3testBegin(p, "300");
  rc = sqlite3_prepare(0, "SELECT 12", 10, &pStmt, 0);
  th3testCheckInt(p, SQLITE_MISUSE, rc);
  th3testBegin(p, "301");
  th3testCheckInt(p, 1, pStmt==0);

  th3testBegin(p, "310");
  rc = sqlite3_prepare_v2(0, "SELECT 12", 10, &pStmt, 0);
  th3testCheckInt(p, SQLITE_MISUSE, rc);
  th3testBegin(p, "311");
  th3testCheckInt(p, 1, pStmt==0);

  th3testBegin(p, "400");
  pBuf = th3malloc(p, 100*sizeof(pBuf[0]));
  pTrueTail = th3malloc(p, 10*sizeof(pBuf[0]));
  th3_ascii_to_utf16("SELECT 1234", pBuf);
  rc = sqlite3_prepare16_v2(db, pBuf, 18, &pStmt, (const void**)(char*)&pTail);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "401");
  rc = sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_ROW, rc);
  th3testBegin(p, "402");
  rc = sqlite3_column_int(pStmt, 0);
  th3testCheckInt(p, 12, rc);
  th3testBegin(p, "403");
  th3_ascii_to_utf16("34", pTrueTail);
  th3testCheckInt(p, 0, memcmp(pTail, pTrueTail, 6));
  sqlite3_finalize(pStmt);

  th3testBegin(p, "410");
  th3_ascii_to_utf16("SELECT 1234", pBuf);
  rc = sqlite3_prepare16(db, pBuf, 18, &pStmt, (const void**)(char*)&pTail);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "411");
  rc = sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_ROW, rc);
  th3testBegin(p, "412");
  rc = sqlite3_column_int(pStmt, 0);
  th3testCheckInt(p, 12, rc);
  th3testBegin(p, "413");
  th3_ascii_to_utf16("34", pTrueTail);
  th3testCheckInt(p, 0, memcmp(pTail, pTrueTail, 6));
  sqlite3_finalize(pStmt);

  th3testBegin(p, "420");
  th3_ascii_to_utf16("SELECT 1234", pBuf);
  rc = sqlite3_prepare16_v2(db, pBuf, 18, &pStmt, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "421");
  rc = sqlite3_step(pStmt);
  th3testCheckInt(p, SQLITE_ROW, rc);
  th3testBegin(p, "422");
  rc = sqlite3_column_int(pStmt, 0);
  th3testCheckInt(p, 12, rc);
  sqlite3_finalize(pStmt);


  th3testBegin(p, "500");
  rc = sqlite3_prepare16(0, pBuf, 18, &pStmt, 0);
  th3testCheckInt(p, SQLITE_MISUSE, rc);
  th3testBegin(p, "501");
  th3testCheckInt(p, 1, pStmt==0);

  th3testBegin(p, "510");
  rc = sqlite3_prepare16_v2(0, pBuf, 18, &pStmt, 0);
  th3testCheckInt(p, SQLITE_MISUSE, rc);
  th3testBegin(p, "511");
  th3testCheckInt(p, 1, pStmt==0);

  th3free(p, pBuf);
  th3free(p, pTrueTail);
  return 0;
}
