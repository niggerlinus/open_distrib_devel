/*
** This module contains tests of WHERE clause handling.
**
** The focus of this module is WHERE clauses containing virtual tables
** where the virtual table contains an usable index.
**
** MODULE_NAME:               where17
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     SHARED_CACHE THREADS
** MINIMUM_HEAPSIZE:          100000
*/

/*
** Global variable in which to record information about the
** most recent call to where17BestIndex() and where17Filter()
*/
static struct {
  int nameIndex;      /* The argvIndex value to assign to "name" */
  int nameOmit;       /* True to omit tests against "name" */
  char zName[20];     /* Last xFilter() value of "name" */
  int valueIndex;     /* The argvIndex for "value" */
  int valueOmit;      /* True to omit tests against "value" */
  char zValue[20];    /* Last xFilter() value for "value" */
} where17g;

/*
** A substitute xBestIndex function for BVS that remembers information
** constraints and sets the where06_match_seen global if found.
*/
static int where17BestIndex(sqlite3_vtab *pVTab, sqlite3_index_info *pInfo){
  th3bvsvtabBestIndex(pVTab, pInfo);

  if( where17g.nameIndex>0 ){
    pInfo->aConstraintUsage[0].argvIndex = where17g.nameIndex;
    if( where17g.nameOmit ){
      pInfo->aConstraintUsage[0].omit = 1;
    }
  }
  if( where17g.valueIndex>0 ){
    pInfo->aConstraintUsage[1].argvIndex = where17g.valueIndex;
    if( where17g.valueOmit ){
      pInfo->aConstraintUsage[1].omit = 1;
    }
  }
  return SQLITE_OK;
}

/* A substitute xFilter function for BVS that records parameters
** then passes control to the default implementation.
*/
static int where17Filter(
  sqlite3_vtab_cursor *pCur,   /* The cursor to rewind */
  int idxNum,                   /* Not used */
  const char *idxStr,           /* Not used */
  int argc,                     /* Not used */
  sqlite3_value **argv          /* Not used */
){
  if( where17g.nameIndex>0 ){
    const char *z =
         (const char*)sqlite3_value_text(argv[where17g.nameIndex-1]);
    assert( strlen(z)<sizeof(where17g.zName) );
    th3strcpy(where17g.zName, z);
  }
  if( where17g.valueIndex>0 ){
    const char *z =
         (const char*)sqlite3_value_text(argv[where17g.valueIndex-1]);
    assert( strlen(z)<sizeof(where17g.zValue) );
    th3strcpy(where17g.zValue, z);
  }

  return th3bvsvtabFilter(pCur, idxNum, idxStr, argc, argv);
}



/* The main testing routine */
int where17(th3state *p){
  sqlite3 *db;
  sqlite3_module where17module;

  where17module = th3bvsvtabModule;
  where17module.xBestIndex = where17BestIndex;
  where17module.xFilter = where17Filter;
  memset(&where17g, 0, sizeof(where17g));

  th3bindInt(p, ":abc", 123);
  th3bindInt(p, ":xyz", 456);
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  sqlite3_create_module(db, "where17", &where17module, (void*)p);
  th3dbEval(p, 0,
     "CREATE VIRTUAL TABLE vt1 USING where17;"
     "SELECT name, value FROM vt1 ORDER BY name;"
  );
  th3testCheck(p, ":abc 123 :xyz 456");

  th3testBegin(p, "110");
  where17g.nameIndex = 1;
  th3dbEval(p, 0,
     "SELECT * FROM vt1 WHERE name=':abc'"
  );
  th3testCheck(p, ":abc 123");
  th3testBegin(p, "111");
  th3testAppendResult(p, where17g.zName);
  th3testCheck(p, ":abc");

  th3testBegin(p, "120");
  where17g.zName[0] = 0;
  where17g.nameIndex = 1;
  where17g.nameOmit = 1;
  th3dbEval(p, 0,
     "SELECT * FROM vt1 WHERE name=':abc'"
  );
  th3testCheck(p, ":abc 123 :xyz 456");
  th3testBegin(p, "121");
  th3testAppendResult(p, where17g.zName);
  th3testCheck(p, ":abc");

  th3dbCloseAll(p);
  return 0;
}
