/*
** This module contains tests of the C-language interfaces found in
** the main.c source file.  The focus of this module is the
** SQLITE_DBCONFIG_ENABLE_FKEY and SQLITE_DBCONFIG_ENABLE_TRIGGER
** features.
**
** MODULE_NAME:               main34
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     FOREIGN_KEYS
** MINIMUM_HEAPSIZE:          100000
*/

int main34(th3state *p){
  int rc;              
  sqlite3 *db;
  int isOn;

  /* Check appropriate response to unknown opcodes to sqlite3_db_config */  
  th3dbNew(p, 0, "test.db");
  db = th3dbPointer(p, 0);
  th3dbEval(p, 0, "CREATE TABLE t1(a,b,c);  INSERT INTO t1 VALUES(1,22,333)");
  th3testBegin(p, "100");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_FKEY, -1, &isOn);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testCheckInt(p, 0, isOn);
  th3testBegin(p, "101");
  th3dbEval(p, 0, "PRAGMA foreign_keys");
  th3testCheck(p, "0");

  th3testBegin(p, "110");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_FKEY, 1, &isOn);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testCheckInt(p, 1, isOn);
  th3testBegin(p, "111");
  th3dbEval(p, 0, "PRAGMA foreign_keys");
  th3testCheck(p, "1");

  th3testBegin(p, "120");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_FKEY, 0, &isOn);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testCheckInt(p, 0, isOn);
  th3testBegin(p, "121");
  th3dbEval(p, 0, "PRAGMA foreign_keys");
  th3testCheck(p, "0");

  th3testBegin(p, "130");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_FKEY, 1, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "131");
  th3dbEval(p, 0, "PRAGMA foreign_keys");
  th3testCheck(p, "1");

  th3testBegin(p, "140");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_FKEY, 0, 0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "141");
  th3dbEval(p, 0, "PRAGMA foreign_keys");
  th3testCheck(p, "0");

  th3testBegin(p, "200");
  th3dbEval(p, 0, 
    "CREATE TABLE t2(x);"
    "CREATE TRIGGER r1 AFTER INSERT ON t1 BEGIN"
    "  INSERT INTO t2 VALUES('insert ' || new.rowid);"
    "END;"
    "CREATE TRIGGER r2 AFTER DELETE ON t1 BEGIN"
    "  INSERT INTO t2 VALUES('delete ' || old.rowid);"
    "END;"
    "CREATE TRIGGER r3 AFTER UPDATE ON t1 BEGIN"
    "  INSERT INTO t2 VALUES('update ' || old.rowid);"
    "END;"
  );
  th3dbEval(p, 0,
    "INSERT INTO t1(rowid,a) VALUES(100,200);"
    "UPDATE t1 SET b=a WHERE rowid=100;"
    "DELETE FROM t1 WHERE rowid=100;"
    "SELECT * FROM t2;"
    "SELECT * FROM t1;"
  );
  th3testCheck(p, "{insert 100} {update 100} {delete 100} 1 22 333");

  th3testBegin(p, "210");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_TRIGGER, -1, &isOn);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testCheckInt(p, 1, isOn);

  th3testBegin(p, "220");
  th3dbEval(p, 0, "DELETE FROM t2");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_TRIGGER, 0, &isOn);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testCheckInt(p, 0, isOn);
  th3testBegin(p, "221");
  th3dbEval(p, 0,
    "INSERT INTO t1(rowid,a) VALUES(100,200);"
    "UPDATE t1 SET b=a WHERE rowid=100;"
    "DELETE FROM t1 WHERE rowid=100;"
    "SELECT * FROM t2;"
    "SELECT * FROM t1;"
  );
  th3testCheck(p, "1 22 333");

  th3testBegin(p, "230");
  th3dbEval(p, 0, "DELETE FROM t2");
  rc = sqlite3_db_config(db, SQLITE_DBCONFIG_ENABLE_TRIGGER, 1, &isOn);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testCheckInt(p, 1, isOn);
  th3testBegin(p, "231");
  th3dbEval(p, 0,
    "INSERT INTO t1(rowid,a) VALUES(100,200);"
    "UPDATE t1 SET b=a WHERE rowid=100;"
    "DELETE FROM t1 WHERE rowid=100;"
    "SELECT * FROM t2;"
    "SELECT * FROM t1;"
  );
  th3testCheck(p, "{insert 100} {update 100} {delete 100} 1 22 333");

  return 0;
}
