/*
** This module tests the SQLITE_OPEN_PRIVATECACHE option to
** sqlite3_open_v2() to make sure that it overrides a global
** sqlite3_enable_shared_cache() setting.
**
** MODULE_NAME:               sharedcache01
** REQUIRED_PROPERTIES:       SHARED_CACHE
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
int sharedcache01(th3state *p){
  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, "CREATE TABLE t1(x);");
  th3dbOpen(p, 1, "test.db", 
               SQLITE_OPEN_READWRITE | SQLITE_OPEN_PRIVATECACHE);
  th3dbEval(p, 1, "PRAGMA read_uncommitted = ON;");
  th3dbEval(p, 0, "BEGIN; INSERT INTO t1 VALUES(123);");
  th3dbEval(p, 1, "SELECT * FROM t1;");
  th3testCheck(p, "");

  th3testBegin(p, "110");
  th3dbEval(p, 0, "ROLLBACK");
  th3dbOpen(p, 1, "test.db", 
               SQLITE_OPEN_READWRITE | SQLITE_OPEN_SHAREDCACHE);
  th3dbEval(p, 1, "PRAGMA read_uncommitted = ON;");
  th3dbEval(p, 0, "BEGIN; INSERT INTO t1 VALUES(123);");
  th3dbEval(p, 1, "SELECT * FROM t1;");
  th3testCheck(p, "123");

  return 0;
}
