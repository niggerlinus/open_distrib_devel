/*
** Test module for pager.c.
**
** Verify that the nReserve size is set correctly on a rollback.
**
** SCRIPT_MODULE_NAME:        pager35
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB JOURNAL_MEMORY JOURNAL_WAL ALT_PCACHE
** MINIMUM_HEAPSIZE:          100000
*/
--reserve 10
--new test.db
--testcase 100
PRAGMA cache_size=10;
CREATE TABLE t1(a,b);
INSERT INTO t1 VALUES(1, 'string');
INSERT INTO t1 VALUES(2, zeroblob(800));
INSERT INTO t1 VALUES(3, zeroblob(800));
#ifndef SQLITE_OMIT_FLOATING_POINT
INSERT INTO t1 VALUES(4, 3.141592658);
#else
INSERT INTO t1 VALUES(4, '3.141592658');
#endif
INSERT INTO t1 VALUES(5, zeroblob(800));
INSERT INTO t1 VALUES(6, zeroblob(800));
INSERT INTO t1 VALUES(7, 777777);
SELECT a, b FROM t1 WHERE length(b)<800 ORDER BY a;
--result 1 string 4 3.141592658 7 777777
--store $pgsz PRAGMA page_size

# This block generates a rollback journal which include a correct
# copy of page 1.  It then corrupts the "reserved" byte count on
# page 1 of the database file.
#
--testcase 110
BEGIN;
CREATE TABLE t2(y);
INSERT INTO t2 VALUES(randomblob($pgsz*90));
--run 0
seek 20 add8 1
--edit test.db
--result
--snapshot
--close all
--revert

# Force a rollback and verify that the "reserved" byte count is
# correctly restored.
--open test.db
--testcase 120
PRAGMA integrity_check;
SELECT a, b FROM t1 WHERE length(b)<800 ORDER BY a
--result ok 1 string 4 3.141592658 7 777777

# Repeat the above test but with a different corruption on the
# reserved byte count.
#
--reserve 10
--new test.db
--testcase 200
PRAGMA cache_size=10;
CREATE TABLE t1(a,b);
INSERT INTO t1 VALUES(1, 'string');
INSERT INTO t1 VALUES(2, zeroblob(800));
INSERT INTO t1 VALUES(3, zeroblob(800));
#ifndef SQLITE_OMIT_FLOATING_POINT
INSERT INTO t1 VALUES(4, 3.141592658);
#else
INSERT INTO t1 VALUES(4, '3.141592658');
#endif
INSERT INTO t1 VALUES(5, zeroblob(800));
INSERT INTO t1 VALUES(6, zeroblob(800));
INSERT INTO t1 VALUES(7, 777777);
SELECT a, b FROM t1 WHERE length(b)<800 ORDER BY a;
--result 1 string 4 3.141592658 7 777777
--testcase 210
PRAGMA page_cache=10;
BEGIN;
CREATE TABLE t2(y);
INSERT INTO t2 VALUES(randomblob($pgsz*90));
--run 0
seek 20 add8 -1
--edit test.db
--result
--snapshot
--close all
--revert
--open test.db
--testcase 220
PRAGMA integrity_check;
SELECT a, b FROM t1 WHERE length(b)<800 ORDER BY a
--result ok 1 string 4 3.141592658 7 777777
