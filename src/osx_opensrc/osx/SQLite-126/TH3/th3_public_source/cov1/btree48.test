/*
** Test module for btree.c.  
**
** This is a corrupt database test.
**
** This module attempts to make page 1 (the sqlite_master root page) become
** an leaf in a multi-page table.  Then we force a balance on that table
** to verify that balance_nonroot detects the problem.
**
** SCRIPT_MODULE_NAME:        btree48
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1 (
  id INTEGER PRIMARY KEY,
  type text,
  name text,
  tbl_name text,
  rootpage integer,
  sql text
);
PRAGMA page_size;
--store $pgsz
/* Insert entries into t1 such that there are 3 leaf pages.  The left-most
** leaf holds the "2" entry.  The center leaf holds "100" and "102" and the
** right leaf holds "200"
*/
INSERT INTO t1 VALUES(2, 'type0', 'name0', 'tbl0', 0, zeroblob($pgsz*3/4));
INSERT INTO t1 VALUES(100, 'type1a', 'name1a', 'tbl1a', 0, zeroblob($pgsz/4));
INSERT INTO t1 VALUES(102, 'type1b', 'name1b', 'tbl1b', 0, zeroblob($pgsz/4));
INSERT INTO t1 VALUES(200, 'type2', 'name2', 'tbl2', 0, zeroblob($pgsz/2));
SELECT id FROM t1 ORDER BY id;
--result 2 100 102 200

/* Replace the left-most page of the t1 btree with the sqlite_master root
** page.  This should cause the id==2 entry to be replaced by the id==1
** entry.
*/
--checkpoint
--testcase 110
SELECT (rootpage-1)*$pgsz FROM sqlite_master WHERE name='t1';
--store $root
seek $root move 12 read16 store $ofst
seek $root move $ofst write32 1 incr-chng
--edit test.db
SELECT id FROM t1 ORDER BY id;
--result 1 100 102 200

/*
** Insert a new row that causes the center leaf to split.  The balancer
** should detect the corrupt at this point.
*/
--testcase 120
INSERT INTO t1 VALUES(101, null, null, null, null, zeroblob($pgsz*3/4));
--result SQLITE_CORRUPT {database disk image is malformed}
