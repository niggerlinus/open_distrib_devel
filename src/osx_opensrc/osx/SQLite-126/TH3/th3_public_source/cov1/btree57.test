/*
** Test module for btree.c.
**
** Database corruption detected by incrVacuumStep().  Also, I/O errors
** in incrVacuumStep().
**
** SCRIPT_MODULE_NAME:        btree57
** REQUIRED_PROPERTIES:       TEST_VFS AUTOVACUUM CLEARTEXT
** DISALLOWED_PROPERTIES:     NO_OOM MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
PRAGMA page_size;
--store $pgsz
PRAGMA auto_vacuum = INCREMENTAL;
INSERT INTO t1 VALUES(1, zeroblob($pgsz*20));
DELETE FROM t1 WHERE a=1;
PRAGMA freelist_count;
--result 20

--checkpoint
--testcase 110
seek $pgsz move 25 write8 1 incr-chng
--edit test.db
PRAGMA incremental_vacuum(100);
--result SQLITE_CORRUPT {database disk image is malformed}


--new test.db
--testcase 200
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
PRAGMA auto_vacuum = INCREMENTAL;
INSERT INTO t1 VALUES(1, zeroblob($pgsz*20));
DELETE FROM t1 WHERE a=1;
PRAGMA freelist_count;
--result 20

--checkpoint
--testcase 210
seek 36 add32 5 incr-chng
--edit test.db
PRAGMA incremental_vacuum(100);
--result SQLITE_CORRUPT {database disk image is malformed}

--new test.db
--testcase 300
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
PRAGMA auto_vacuum = INCREMENTAL;
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz*3));
INSERT INTO t1 VALUES(3, zeroblob($pgsz*3));
INSERT INTO t1 VALUES(4, zeroblob($pgsz*3));
INSERT INTO t1 VALUES(5, zeroblob($pgsz));
DELETE FROM t1 WHERE a=4;
DELETE FROM t1 WHERE a=3;
DELETE FROM t1 WHERE a=2;
DELETE FROM t1 WHERE a=1;
PRAGMA incremental_vacuum(1000);
PRAGMA freelist_count;
--result 0

--testcase 310
DELETE FROM t1;
PRAGMA incremental_vacuum(1000);
PRAGMA freelist_count;
--result 0

--testcase 320
--ioerr
--open test.db
PRAGMA cache_size=2;
PRAGMA auto_vacuum = INCREMENTAL;
INSERT INTO t1 VALUES(1, zeroblob(55000));
DELETE FROM t1;
PRAGMA incremental_vacuum(1000);
--result

--new test.db
--testcase 400
CREATE TABLE t1(a INTEGER PRIMARY KEY, b);
PRAGMA auto_vacuum = INCREMENTAL;
INSERT INTO t1 VALUES(1, zeroblob($pgsz));
INSERT INTO t1 VALUES(2, zeroblob($pgsz*3));
INSERT INTO t1 VALUES(3, zeroblob($pgsz*3));
INSERT INTO t1 VALUES(4, zeroblob($pgsz*3));
INSERT INTO t1 VALUES(5, zeroblob($pgsz));
DELETE FROM t1 WHERE a=4;
DELETE FROM t1 WHERE a=3;
DELETE FROM t1 WHERE a=2;
DELETE FROM t1 WHERE a=1;
PRAGMA freelist_count;
--result 10

--checkpoint
--testcase 410
SELECT 13*$pgsz;
--store $page14ofst
seek $page14ofst write32 2 incr-chng
--edit test.db
PRAGMA incremental_vacuum(1000);
--result SQLITE_CORRUPT {database disk image is malformed}
