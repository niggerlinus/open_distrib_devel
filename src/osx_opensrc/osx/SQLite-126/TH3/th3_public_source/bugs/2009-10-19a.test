/*
** This module checks that the bug reported by ticket 
** [d82e3f3721296e905d7e8c9dee718f71a826b0eb] has been fixed.
**
** SCRIPT_MODULE_NAME:        bug_2009_10_19a
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY AUTOINCREMENT, b TEXT);
INSERT INTO t1 VALUES(null,'abc');
DELETE FROM t1;
INSERT INTO t1 VALUES(null,'def');
SELECT * FROM t1;
SELECT * FROM main.sqlite_sequence;
--result 2 def t1 2

--testcase 110
CREATE TEMP TABLE t2(a INTEGER PRIMARY KEY AUTOINCREMENT, b TEXT);
INSERT INTO t2 VALUES(null,'ghi');
DELETE FROM t2;
INSERT INTO t2 VALUES(null,'jkl');
SELECT * FROM t2;
SELECT * FROM temp.sqlite_sequence;
--result 2 jkl t2 2

--testcase 120
SELECT 'main', * FROM main.sqlite_sequence;
SELECT 'temp', * FROM temp.sqlite_sequence;
--result main t1 2 temp t2 2

--testcase 130
VACUUM;
SELECT 'main', * FROM main.sqlite_sequence;
SELECT 'temp', * FROM temp.sqlite_sequence;
--result main t1 2 temp t2 2

--db 1
--open test.db
--testcase 200
CREATE TEMP TABLE t3(x);
INSERT INTO t3 VALUES('hello');
--result

--db 0
--testcase 210
CREATE TABLE t3(y,z);
INSERT INTO t3 VALUES('this is','a test');
--result

--db 1
--testcase 220
SELECT * FROM temp.t3 JOIN main.t3;
--result hello {this is} {a test}

--testcase 230
VACUUM;
SELECT * FROM temp.t3 JOIN main.t3;
--result hello {this is} {a test}

--db 1
--testcase 300
CREATE TEMP TABLE t4(x);
INSERT INTO t4 VALUES('temp-data');
--result

--db 0
--testcase 310
CREATE TABLE t4(y);
INSERT INTO t4 VALUES('main-data');
--result

--db 1
--testcase 320
SELECT * FROM temp.t4 JOIN main.t4;
--result temp-data main-data

--testcase 330
VACUUM;
SELECT * FROM temp.t4 JOIN main.t4;
--result temp-data main-data
