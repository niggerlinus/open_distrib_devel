/*
** Module for testing the OOM testing logic fault.c.
** These are structural tests, written with an eye on the code.
**
** MODULE_NAME:               fault01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     NO_OOM MEMDB
** MINIMUM_HEAPSIZE:          100000
*/
int fault01(th3state *p){
  int rc;
  sqlite3 *db;
  int okCnt;

  th3dbNew(p, 0, "test.db");
  th3dbEval(p, 0, "CREATE TABLE t1(x); INSERT INTO t1 VALUES('hello')");
  th3dbClose(p, 0);
  th3oomBegin(p, "1");
  while( th3oomNext(p) ){
    th3dbOpen(p, 0, "test.db", TH3_OPEN_RAW);
    db = th3dbPointer(p, 0);
    th3oomEnable(p, 1);
    rc = sqlite3_db_config(db, SQLITE_DBCONFIG_LOOKASIDE, 0, 100, 100);
    th3oomEnable(p, 0);
    th3dbEval(p, 0, "SELECT * FROM t1");
    if( th3oomHit(p) ){
      th3oomCheck(p, 1, rc==SQLITE_NOMEM);
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      th3oomResult(p, 3, "hello");
    }
  }
  th3oomEnd(p);

  th3oomBegin(p, "2");
  sqlite3_test_control(SQLITE_TESTCTRL_BENIGN_MALLOC_HOOKS, 0, 0);
  okCnt = 0;
  while( th3oomNext(p) ){
    th3dbOpen(p, 0, "test.db", TH3_OPEN_RAW);
    db = th3dbPointer(p, 0);
    th3oomEnable(p, 1);
    rc = sqlite3_db_config(db, SQLITE_DBCONFIG_LOOKASIDE, 0, 100, 100);
    th3oomEnable(p, 0);
    th3dbEval(p, 0, "SELECT * FROM t1");
    if( th3oomHit(p) ){
      if( rc==SQLITE_OK ) okCnt++;
      th3oomCheck(p, 1, rc==SQLITE_NOMEM || rc==SQLITE_OK);
    }else{
      th3oomCheck(p, 2, rc==SQLITE_OK);
      th3oomResult(p, 3, "hello");
    }
  }
  th3oomEnd(p);
  sqlite3_test_control(SQLITE_TESTCTRL_BENIGN_MALLOC_HOOKS, 
      th3oomBeginBenign, th3oomEndBenign
  );

  th3testBegin(p, "3");
  th3testAppendResult(p, th3format(p, "%d", okCnt>0));
  th3testCheck(p, "1");  

  return 0;
}
