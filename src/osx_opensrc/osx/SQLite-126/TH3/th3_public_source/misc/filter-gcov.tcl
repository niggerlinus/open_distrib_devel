#!/usr/bin/tclsh
#
# Run this script over the output of gcov -b -c to generate
# coverage statistics for the SQLite amalgamation source file.
#
# We look for both statement coverage and branch coverage.  Lines
# that contain "/*NO_TEST*/" and lines that contain "va_arg" are
# omitted from consideration.
#
# See also the cov-filter.tcl script
#

set gcov-core 1
set gcov-vfs 0
set argc [llength $argv]
for {set i 0} {$i<$argc-1} {incr i} {
  if {[string trimleft [lindex $argv $i] -]=="core"} {
    incr i
    set gcov-core [lindex $argv $i]
  }
  if {[string trimleft [lindex $argv $i] -]=="vfs"} {
    incr i
    set gcov-vfs [lindex $argv $i]
  }
}
set in [open sqlite3.c.gcov]
set out [open filtered.gcov w]
set line_run 0
set line_missed 0
set br_taken 0
set br_missed 0
set restrict [expr {!${gcov-core}}]
set filename {}
set totals {}
proc save_totals {} {
  global filename
  if {$filename==""} return
  global totals br_taken br_missed line_run line_missed
  set key [expr {(9999-$br_missed)*10000 + (9999-$line_missed)}]
  lappend totals [list $filename $line_run $line_missed $br_taken $br_missed \
              [format {%015d%s} $key $filename]]
  set line_run 0
  set line_missed 0
  set br_taken 0
  set br_missed 0
}
set notest 0
while {![eof $in]} {
  set line [gets $in]
  if {[regexp {\*\*\* Begin file ([a-zA-Z0-9_]+\.c)} $line all fn]} {
    save_totals
    if {$fn=="os_unix.c"} {
      set restrict [expr {!${gcov-vfs}}]
    } else {
      set restrict [expr {!${gcov-core}}]
    }
    set filename $fn
    set notest 0
  }
  if {$restrict} continue
  puts $out $line
  if {[regexp {^branch} $line]} {
    if {!$notest &&
           (([regexp { taken 0} $line] && !$isvaarg)
               || [regexp { never exec} $line])} {
      incr br_missed
    } else {
      incr br_taken
    }
  } elseif {[regexp {/\*NO_TEST\*/} $line]} {
    set notest 1
  } elseif {[regexp {^    #####:} $line]} {
    incr line_missed
    set notest 0
  } elseif {[regexp {^ *[0-9]+:} $line]} {
    if {[regexp {va_arg\(.+,.+\)} $line]} {
      set isvaarg 1
    } else {
      set isvaarg 0
    }
    incr line_run
    set notest 0
  }
}
close $out
save_totals
foreach entry $totals {
  foreach {a b c d e} $entry break
  incr line_run $b
  incr line_missed $c
  incr br_taken $d
  incr br_missed $e
}
set line_total [expr {$line_run+$line_missed}]
puts [format {Statement coverage: %6.2f%%  hit %6d  missed %6d  total %6d} \
   [expr {$line_run*100.0/$line_total}] $line_run $line_missed $line_total]
set br_total [expr {$br_taken+$br_missed}]
puts [format {Branch coverage:    %6.2f%%  hit %6d  missed %6d  total %6d} \
   [expr {$br_taken*100.0/$br_total}] $br_taken $br_missed $br_total]
puts {}
foreach entry [lsort -index 5 $totals] {
  foreach {a b c d e} $entry break
  if {$b>0 || $c>0 || $d>0 || $e>0} {
    set total [expr {$b+$c}]
    set pctline ([expr {$total ? int($c*100.0/$total) : 0}]%)
    set total [expr {$d+$e}]
    set pctbr ([expr {$total ? int($e*100.0/$total) : 0}]%)
    if {$c==0 && $e==0} continue
    puts [format {%14s: %6d %6s missed-lines  %6d %6s missed-branches} \
         $a $c $pctline $e $pctbr]
  }
}
puts {}
