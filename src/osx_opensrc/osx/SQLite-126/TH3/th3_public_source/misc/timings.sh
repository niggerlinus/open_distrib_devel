#!/bin/sh
#
# After a test run with -DTH3_UNIX_TIMER, run this script to analyze the
# th3-out.txt output file and generate reports on the times required for
# various test modules and configurations.
#
# The times0.txt file contains lines with three fields:
#
#     CONFIG MODULE TIME
#
grep '^End' th3-out.txt | grep -v MISUSE | sed 's/\./ /' |
  awk '{print $2, $3, $4}' >times0.txt

# The times1.txt file contains summary statistics for configurations:
#
#     CONFIG TOTAL-TIME MODULE-COUNT
#
awk '{x[$1]+=$3; cnt[$1]++}
     END{for(i in x){printf "%-4s %7.3f %4d\n", i, x[i], cnt[i]}}' \
     times0.txt >times1.txt
echo '================ Configuration Times ===================='
sort -k 2 -n times1.txt

# The times2.txt file contains summary statistics for configurations:
#
#     MODULE TOTAL-TIME AVG-TIME MODULE-COUNT
#
awk '{x[$2]+=$3; cnt[$2]++}
     END{for(i in x){printf "%-15s %7.3f %7.3f %4d\n", \
         i, x[i], x[i]/cnt[i], cnt[i]}}' \
     times0.txt >times2.txt
echo '=============== Selected Module Times ==================='
sort -k 2 -n times2.txt | tail -20
