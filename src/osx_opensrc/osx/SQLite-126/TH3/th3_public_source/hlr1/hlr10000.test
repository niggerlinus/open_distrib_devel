/*
** High-level requirements test cases
**
** MODULE_NAME:               hlr10000
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/
int hlr10000(th3state *p){
  
  th3testBegin(p, "1.1");
  /* H10010 */
  /* H10011 */
  /* H10014 */
  th3testAppendResult(p, 
    th3format(p, "%d.%d.%d",
       SQLITE_VERSION_NUMBER/1000000,
       (SQLITE_VERSION_NUMBER/1000)%1000,
       SQLITE_VERSION_NUMBER%1000
  ));
  th3testCheck(p, SQLITE_VERSION);

  th3testBegin(p, "1.2");
  /* H10020 */
  /* H10021 */
  if( sqlite3_libversion_number()==SQLITE_VERSION_NUMBER ){
    th3testOk(p);
  }else{
    th3testFailed(p, 0);
  }

  th3testBegin(p, "1.3");
  /* H10020 */
  /* H10022 */
  th3testAppendResult(p, sqlite3_version);
  th3testCheck(p, SQLITE_VERSION);

  th3testBegin(p, "1.4");
  /* H10020 */
  /* H10023 */
  th3testAppendResult(p, sqlite3_libversion());
  th3testCheck(p, SQLITE_VERSION);

  th3testBegin(p, "2.1");
  /* H10100 */
  (void)sqlite3_threadsafe();
  th3testOk(p);

  th3testBegin(p, "2.2");
  /* H10160 */
  assert( SQLITE_CONFIG_SINGLETHREAD==1 );
  assert( SQLITE_CONFIG_MULTITHREAD ==2 );
  assert( SQLITE_CONFIG_SERIALIZED  ==3 );
  assert( SQLITE_CONFIG_MALLOC      ==4 );
  assert( SQLITE_CONFIG_GETMALLOC   ==5 );
  assert( SQLITE_CONFIG_SCRATCH     ==6 );
  assert( SQLITE_CONFIG_PAGECACHE   ==7 );
  assert( SQLITE_CONFIG_HEAP        ==8 );
  assert( SQLITE_CONFIG_MEMSTATUS   ==9 );
  assert( SQLITE_CONFIG_MUTEX      ==10 );
  assert( SQLITE_CONFIG_GETMUTEX   ==11 );
  assert( SQLITE_CONFIG_LOOKASIDE  ==13 );
  assert( SQLITE_CONFIG_PCACHE     ==14 );
  assert( SQLITE_CONFIG_GETPCACHE  ==15 );
  th3testOk(p);

  th3testBegin(p, "2.3");
  /* H10170 */
  assert( SQLITE_DBCONFIG_LOOKASIDE==1001 );
  th3testOk(p);

  th3testBegin(p, "2.4");
  /* H10200 H10201 H10202 */
  {
    volatile sqlite_int64 i64a;
    volatile sqlite3_int64 i64b;
    volatile sqlite_uint64 u64a;
    volatile sqlite3_uint64 u64b;

    assert( sizeof(i64a)==8 );
    assert( sizeof(i64b)==8 );
    assert( sizeof(u64a)==8 );
    assert( sizeof(u64b)==8 );
    i64a = 1;
    i64b = i64a<<63;
    assert( i64b<0 );
    i64b = i64b>>63;
    assert( (i64b&1)==1 );
    i64b = 1;
    i64a = i64b<<63;
    assert( i64a<0 );
    i64a = i64a>>63;
    assert( (i64a&1)==1 );
    u64a = 1;
    u64b = u64a<<63;
    assert( u64b>0 );
    u64b = u64b>>63;
    assert( u64b==1 );
    u64a = u64b<<63;
    assert( u64a>0 );
    u64a = u64a>>63;
    assert( u64a==1 );
  }
  th3testOk(p);

  th3testBegin(p, "2.5");
  /* H10210 */
  assert( SQLITE_OK         ==0  );
  assert( SQLITE_ERROR      ==1  );
  assert( SQLITE_INTERNAL   ==2  );
  assert( SQLITE_PERM       ==3  );
  assert( SQLITE_ABORT      ==4  );
  assert( SQLITE_BUSY       ==5  );
  assert( SQLITE_LOCKED     ==6  );
  assert( SQLITE_NOMEM      ==7  );
  assert( SQLITE_READONLY   ==8  );
  assert( SQLITE_INTERRUPT  ==9  );
  assert( SQLITE_IOERR     ==10  );
  assert( SQLITE_CORRUPT   ==11  );
  assert( SQLITE_NOTFOUND  ==12  );
  assert( SQLITE_FULL      ==13  );
  assert( SQLITE_CANTOPEN  ==14  );
  assert( SQLITE_PROTOCOL  ==15  );
  assert( SQLITE_EMPTY     ==16  );
  assert( SQLITE_SCHEMA    ==17  );
  assert( SQLITE_TOOBIG    ==18  );
  assert( SQLITE_CONSTRAINT==19  );
  assert( SQLITE_MISMATCH  ==20  );
  assert( SQLITE_MISUSE    ==21  );
  assert( SQLITE_NOLFS     ==22  );
  assert( SQLITE_AUTH      ==23  );
  assert( SQLITE_FORMAT    ==24  );
  assert( SQLITE_RANGE     ==25  );
  assert( SQLITE_NOTADB    ==26  );
  assert( SQLITE_ROW       ==100 );
  assert( SQLITE_DONE      ==101 );
  th3testOk(p);

  th3testBegin(p, "2.6");
  /* H10220 */
  assert( SQLITE_IOERR_READ              == (10 | (1<<8)) );
  assert( SQLITE_IOERR_SHORT_READ        == (10 | (2<<8)) );
  assert( SQLITE_IOERR_WRITE             == (10 | (3<<8)) );
  assert( SQLITE_IOERR_FSYNC             == (10 | (4<<8)) );
  assert( SQLITE_IOERR_DIR_FSYNC         == (10 | (5<<8)) );
  assert( SQLITE_IOERR_TRUNCATE          == (10 | (6<<8)) );
  assert( SQLITE_IOERR_FSTAT             == (10 | (7<<8)) );
  assert( SQLITE_IOERR_UNLOCK            == (10 | (8<<8)) );
  assert( SQLITE_IOERR_RDLOCK            == (10 | (9<<8)) );
  assert( SQLITE_IOERR_DELETE            == (10 | (10<<8)) );
  assert( SQLITE_IOERR_BLOCKED           == (10 | (11<<8)) );
  assert( SQLITE_IOERR_NOMEM             == (10 | (12<<8)) );
  assert( SQLITE_IOERR_ACCESS            == (10 | (13<<8)) );
  assert( SQLITE_IOERR_CHECKRESERVEDLOCK == (10 | (14<<8)) );
  assert( SQLITE_IOERR_LOCK              == (10 | (15<<8)) );
  assert( SQLITE_IOERR_CLOSE             == (10 | (16<<8)) );
  assert( SQLITE_IOERR_DIR_CLOSE         == (10 | (17<<8)) );
  th3testOk(p);

  th3testBegin(p, "2.7");
  /* H10230 */
  assert( SQLITE_OPEN_READONLY        == 0x00000001 );
  assert( SQLITE_OPEN_READWRITE       == 0x00000002 );
  assert( SQLITE_OPEN_CREATE          == 0x00000004 );
  assert( SQLITE_OPEN_DELETEONCLOSE   == 0x00000008 );
  assert( SQLITE_OPEN_EXCLUSIVE       == 0x00000010 );
  assert( SQLITE_OPEN_MAIN_DB         == 0x00000100 );
  assert( SQLITE_OPEN_TEMP_DB         == 0x00000200 );
  assert( SQLITE_OPEN_TRANSIENT_DB    == 0x00000400 );
  assert( SQLITE_OPEN_MAIN_JOURNAL    == 0x00000800 );
  assert( SQLITE_OPEN_TEMP_JOURNAL    == 0x00001000 );
  assert( SQLITE_OPEN_SUBJOURNAL      == 0x00002000 );
  assert( SQLITE_OPEN_MASTER_JOURNAL  == 0x00004000 );
  assert( SQLITE_OPEN_NOMUTEX         == 0x00008000 );
  assert( SQLITE_OPEN_FULLMUTEX       == 0x00010000 );
  th3testOk(p);

  th3testBegin(p, "2.8");
  /* H10240 */
  assert( SQLITE_IOCAP_ATOMIC == 0x00000001 );
  assert( SQLITE_IOCAP_ATOMIC512 == 0x00000002 );
  assert( SQLITE_IOCAP_ATOMIC1K == 0x00000004 );
  assert( SQLITE_IOCAP_ATOMIC2K == 0x00000008 );
  assert( SQLITE_IOCAP_ATOMIC4K == 0x00000010 );
  assert( SQLITE_IOCAP_ATOMIC8K == 0x00000020 );
  assert( SQLITE_IOCAP_ATOMIC16K == 0x00000040 );
  assert( SQLITE_IOCAP_ATOMIC32K == 0x00000080 );
  assert( SQLITE_IOCAP_ATOMIC64K == 0x00000100 );
  assert( SQLITE_IOCAP_SAFE_APPEND == 0x00000200 );
  assert( SQLITE_IOCAP_SEQUENTIAL == 0x00000400 );
  th3testOk(p);

  th3testBegin(p, "2.9");
  /* H10250 */
  assert( SQLITE_LOCK_NONE == 0 );
  assert( SQLITE_LOCK_SHARED == 1 );
  assert( SQLITE_LOCK_RESERVED == 2 );
  assert( SQLITE_LOCK_PENDING == 3 );
  assert( SQLITE_LOCK_EXCLUSIVE == 4 );
  th3testOk(p);

  th3testBegin(p, "2.10");
  /* H10260 */
  assert( SQLITE_SYNC_NORMAL == 0x00002 );
  assert( SQLITE_SYNC_FULL == 0x00003 );
  assert( SQLITE_SYNC_DATAONLY == 0x00010 );
  th3testOk(p);

  th3testBegin(p, "2.11");
  /* H10265 */
  assert( SQLITE_INTEGER == 1 );
  assert( SQLITE_FLOAT == 2 );
  assert( SQLITE_BLOB == 4 );
  assert( SQLITE_NULL == 5 );
  assert( SQLITE_TEXT == 3 );
  assert( SQLITE3_TEXT == 3 );
  th3testOk(p);

  th3testBegin(p, "2.12");
  /* H10267 */
  assert( SQLITE_UTF8 == 1 );
  assert( SQLITE_UTF16LE == 2 );
  assert( SQLITE_UTF16BE == 3 );
  assert( SQLITE_UTF16 == 4 );
  assert( SQLITE_ANY == 5 );
  assert( SQLITE_UTF16_ALIGNED == 8 );
  th3testOk(p);

  th3testBegin(p, "2.13");
  /* H10280 */
  assert( SQLITE_STATIC == ((sqlite3_destructor_type)0) );
  assert( SQLITE_TRANSIENT == ((sqlite3_destructor_type)-1) );
  th3testOk(p);

  th3testBegin(p, "2.14");
  /* H10310 */
  {
    char *zTemp;
    assert( &zTemp!=&sqlite3_temp_directory );
  }
  th3testOk(p);

  th3testBegin(p, "3.1");
  /* H11190 */
  assert( SQLITE_ACCESS_EXISTS == 0 );
  assert( SQLITE_ACCESS_READWRITE == 1 );
  assert( SQLITE_ACCESS_READ == 2 );
  th3testOk(p);

  th3testBegin(p, "3.2");
  /* H11310 */
  assert( SQLITE_FCNTL_LOCKSTATE == 1 );
  assert( SQLITE_GET_LOCKPROXYFILE == 2 );
  assert( SQLITE_SET_LOCKPROXYFILE == 3 );
  assert( SQLITE_LAST_ERRNO == 4 );
  th3testOk(p);

  th3testBegin(p, "3.3");
  /* H11410 */
  assert( SQLITE_TESTCTRL_PRNG_SAVE == 5 );
  assert( SQLITE_TESTCTRL_PRNG_RESTORE == 6 );
  assert( SQLITE_TESTCTRL_PRNG_RESET == 7 );
  assert( SQLITE_TESTCTRL_BITVEC_TEST == 8 );
  assert( SQLITE_TESTCTRL_FAULT_INSTALL == 9 );
  assert( SQLITE_TESTCTRL_BENIGN_MALLOC_HOOKS == 10 );
  th3testOk(p);

  th3testBegin(p, "3.4");
  /* H12550 */
  assert( SQLITE_CREATE_INDEX == 1 );
  assert( SQLITE_CREATE_TABLE == 2 );
  assert( SQLITE_CREATE_TEMP_INDEX == 3 );
  assert( SQLITE_CREATE_TEMP_TABLE == 4 );
  assert( SQLITE_CREATE_TEMP_TRIGGER == 5 );
  assert( SQLITE_CREATE_TEMP_VIEW == 6 );
  assert( SQLITE_CREATE_TRIGGER == 7 );
  assert( SQLITE_CREATE_VIEW == 8 );
  assert( SQLITE_DELETE == 9 );
  assert( SQLITE_DROP_INDEX == 10 );
  assert( SQLITE_DROP_TABLE == 11 );
  assert( SQLITE_DROP_TEMP_INDEX == 12 );
  assert( SQLITE_DROP_TEMP_TABLE == 13 );
  assert( SQLITE_DROP_TEMP_TRIGGER == 14 );
  assert( SQLITE_DROP_TEMP_VIEW == 15 );
  assert( SQLITE_DROP_TRIGGER == 16 );
  assert( SQLITE_DROP_VIEW == 17 );
  assert( SQLITE_INSERT == 18 );
  assert( SQLITE_PRAGMA == 19 );
  assert( SQLITE_READ == 20 );
  assert( SQLITE_SELECT == 21 );
  assert( SQLITE_TRANSACTION == 22 );
  assert( SQLITE_UPDATE == 23 );
  assert( SQLITE_ATTACH == 24 );
  assert( SQLITE_DETACH == 25 );
  assert( SQLITE_ALTER_TABLE == 26 );
  assert( SQLITE_REINDEX == 27 );
  assert( SQLITE_ANALYZE == 28 );
  assert( SQLITE_CREATE_VTABLE == 29 );
  assert( SQLITE_DROP_VTABLE == 30 );
  assert( SQLITE_FUNCTION == 31 );
  assert( SQLITE_COPY == 0 );
  th3testOk(p);

  th3testBegin(p, "3.5");
  /* H12590 */
  assert( SQLITE_DENY == 1 );
  assert( SQLITE_IGNORE == 2 );
  th3testOk(p);

  th3testBegin(p, "3.6");
  /* H12790 */
  assert( SQLITE_LIMIT_LENGTH == 0 );
  assert( SQLITE_LIMIT_SQL_LENGTH == 1 );
  assert( SQLITE_LIMIT_COLUMN == 2 );
  assert( SQLITE_LIMIT_EXPR_DEPTH == 3 );
  assert( SQLITE_LIMIT_COMPOUND_SELECT == 4 );
  assert( SQLITE_LIMIT_VDBE_OP == 5 );
  assert( SQLITE_LIMIT_FUNCTION_ARG == 6 );
  assert( SQLITE_LIMIT_ATTACHED == 7 );
  assert( SQLITE_LIMIT_LIKE_PATTERN_LENGTH == 8 );
  assert( SQLITE_LIMIT_VARIABLE_NUMBER == 9 );
  th3testOk(p);

  th3testBegin(p, "3.7");
  /* H17001 */
  assert( SQLITE_MUTEX_FAST == 0 );
  assert( SQLITE_MUTEX_RECURSIVE == 1 );
  assert( SQLITE_MUTEX_STATIC_MASTER == 2 );
  assert( SQLITE_MUTEX_STATIC_MEM == 3 );
  assert( SQLITE_MUTEX_STATIC_MEM2 == 4 );
  assert( SQLITE_MUTEX_STATIC_PRNG == 5 );
  assert( SQLITE_MUTEX_STATIC_LRU == 6 );
  assert( SQLITE_MUTEX_STATIC_LRU2 == 7 );
  th3testOk(p);

  th3testBegin(p, "3.8");
  /* H17250 */
  assert( SQLITE_STATUS_MEMORY_USED == 0 );
  assert( SQLITE_STATUS_PAGECACHE_USED == 1 );
  assert( SQLITE_STATUS_PAGECACHE_OVERFLOW == 2 );
  assert( SQLITE_STATUS_SCRATCH_USED == 3 );
  assert( SQLITE_STATUS_SCRATCH_OVERFLOW == 4 );
  assert( SQLITE_STATUS_MALLOC_SIZE == 5 );
  assert( SQLITE_STATUS_PARSER_STACK == 6 );
  assert( SQLITE_STATUS_PAGECACHE_SIZE == 7 );
  assert( SQLITE_STATUS_SCRATCH_SIZE == 8 );
  th3testOk(p);

  th3testBegin(p, "3.9");
  /* H17520 */
  assert( SQLITE_DBSTATUS_LOOKASIDE_USED == 0 );
  th3testOk(p);

  th3testBegin(p, "3.10");
  /* H17570 */
  assert( SQLITE_STMTSTATUS_FULLSCAN_STEP == 1 );
  assert( SQLITE_STMTSTATUS_SORT == 2 );
  th3testOk(p);

  th3testBegin(p, "3.11");
  /* H18100 */
  assert( SQLITE_INDEX_CONSTRAINT_EQ == 2 );
  assert( SQLITE_INDEX_CONSTRAINT_GT == 4 );
  assert( SQLITE_INDEX_CONSTRAINT_LE == 8 );
  assert( SQLITE_INDEX_CONSTRAINT_LT == 16 );
  assert( SQLITE_INDEX_CONSTRAINT_GE == 32 );
  assert( SQLITE_INDEX_CONSTRAINT_MATCH == 64 );
  th3testOk(p);

  return 0;
}
