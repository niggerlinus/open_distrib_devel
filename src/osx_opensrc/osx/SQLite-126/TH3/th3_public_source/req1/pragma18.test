/*
** This module contains tests for the foreign_key pragma
**
** SCRIPT_MODULE_NAME:        req1_pragma18
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     FOREIGN_KEYS
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-09033-01493 As of SQLite version 3.6.19, the default
** setting for foreign key enforcement is OFF.
*/
--testcase 100
PRAGMA foreign_keys;
--result 0

/* EVIDENCE-OF: R-18160-43138 PRAGMA foreign_keys; PRAGMA foreign_keys =
** boolean; Query, set, or clear the enforcement of foreign key
** constraints.
*/
--testcase 110
PRAGMA foreign_keys=on;
PRAGMA foreign_keys;
--result 1
--testcase 111
PRAGMA foreign_keys=off;
PRAGMA foreign_keys;
--result 0

/* EVIDENCE-OF: R-53547-37067 This pragma is a no-op within a
** transaction; foreign key constraint enforcement may only be enabled or
** disabled when there is no pending BEGIN or SAVEPOINT.
*/
--testcase 120
PRAGMA foreign_keys;
BEGIN;
PRAGMA foreign_keys=ON;
PRAGMA foreign_keys;
COMMIT;
--result 0 0
--testcase 121
PRAGMA foreign_keys=ON;
PRAGMA foreign_keys;
BEGIN;
PRAGMA foreign_keys=Off;
PRAGMA foreign_keys;
COMMIT;
--result 1 1
--testcase 122
PRAGMA foreign_keys=Off;
PRAGMA foreign_keys;
SAVEPOINT xyz;
PRAGMA foreign_keys=On;
PRAGMA foreign_keys;
RELEASE xyz;
--result 0 0

/* EVIDENCE-OF: R-54105-45069 Changing the foreign_keys setting affects
** the execution of all statements prepared using the database
** connection, including those prepared before the setting was changed.
*/
--testcase 200
CREATE TABLE t1(x INTEGER PRIMARY KEY);
CREATE TABLE t2(y INTEGER REFERENCES t1, z);
--result
--stmt-cache 10
--testcase 201
INSERT INTO t2 VALUES(1,'one');
DELETE FROM t2;
SELECT * FROM t2;
--result
--testcase 202
PRAGMA foreign_keys=ON;
INSERT INTO t2 VALUES(1,'one');
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 203
PRAGMA foreign_keys=OFF;
INSERT INTO t2 VALUES(1,'one');
SELECT * FROM t2;
--result 1 one
