/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys09
** REQUIRED_PROPERTIES:       FOREIGN_KEYS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

/* EV: R-15741-50893 The child key index does not have to be (and usually
** will not be) a UNIQUE index.
**
** EV: R-14553-34013
**
** The complete database schema for efficient implementation of the
** foreign key constraint might be: ....
*/
--testcase 100
CREATE TABLE artist(
  artistid    INTEGER PRIMARY KEY, 
  artistname  TEXT
);
CREATE TABLE track(
  trackid     INTEGER,
  trackname   TEXT, 
  trackartist INTEGER REFERENCES artist
);
CREATE INDEX trackindex ON track(trackartist);
--result

/* EVIDENCE-OF: R-43879-08025 Attaching a "REFERENCES <parent-table>"
** clause to a column definition creates a foreign
** key constraint that maps the column to the primary key of
** <parent-table>.
*/
--testcase 110
INSERT INTO artist VALUES(123,'one-two-three');
INSERT INTO track VALUES(1,'track-one',122);
--result SQLITE_CONSTRAINT {foreign key constraint failed}
--testcase 111
INSERT INTO track VALUES(1,'track-one',123);
--result
