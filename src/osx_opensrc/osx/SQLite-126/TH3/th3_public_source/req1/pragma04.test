/*
** This module contains tests for pragma documentation; for the
** locking_mode pragma.
**
** SCRIPT_MODULE_NAME:        req1_pragma04
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     LOCKMODE_EXCLUSIVE SHARED_CACHE MEMDB
** DISALLOWED_PROPERTIES:     JOURNAL_WAL ZIPVFS
** MINIMUM_HEAPSIZE:          100000
** INCOMPATIBLE_WITH:         dotlock nolock
**
*/

/* EVIDENCE-OF: R-28634-61063 PRAGMA locking_mode; PRAGMA locking_mode =
** NORMAL | EXCLUSIVE
**
** EVIDENCE-OF: R-39608-49000 This pragma sets or queries the database
** connection locking-mode.
**
** EVIDENCE-OF: R-12374-37755 The locking-mode is either NORMAL or
** EXCLUSIVE.
*/
--testcase 100
PRAGMA locking_mode;
PRAGMA locking_mode=NORMAL;
PRAGMA locking_mode=EXCLUSIVE;
PRAGMA locking_mode(normal);
PRAGMA main.locking_mode;
--result normal normal exclusive normal normal

/* EVIDENCE-OF: R-43797-56749 In NORMAL locking-mode (the default unless
** overridden at compile-time using SQLITE_DEFAULT_LOCKING_MODE), a
** database connection unlocks the database file at the conclusion of
** each read or write transaction.
*/
--new test.db
--testcase 110
PRAGMA locking_mode;
--result normal
--testcase 111
BEGIN;
CREATE TABLE t1(x);
INSERT INTO t1 VALUES(5);
--result
--db 1
--open test.db
--testcase 112
CREATE TABLE t2(y);
--result SQLITE_BUSY {database is locked}
--db 0
--testcase 113
COMMIT;
SELECT name FROM sqlite_master;
--result t1
--db 1
--open test.db
--testcase 114
CREATE TABLE t2(y);
SELECT name FROM sqlite_master ORDER BY 1;
--result t1 t2
--db 0
--testcase 115
SELECT name FROM sqlite_master;
--result t1 t2

/* EVIDENCE-OF: R-29276-00741 When the locking-mode is set to EXCLUSIVE,
** the database connection never releases file-locks.
**
** EVIDENCE-OF: R-07178-42569 The first time the database is read in
** EXCLUSIVE mode, a shared lock is obtained and held.
**
** There are three reasons to set the locking-mode to EXCLUSIVE. 
** Reason number 1:
**
** EVIDENCE-OF: R-43609-64396 The application wants to prevent other
** processes from accessing the database file.
*/
--testcase 120
PRAGMA locking_mode=EXCLUSIVE;
--result exclusive

/* db-1 can still access the database since db-0 has not yet acquired
** the lock */
--db 1
--testcase 121
INSERT INTO t2 VALUES(200);
SELECT * FROM t2;
--result 200

/* The db-0 lock escalates to a read-lock */
--db 0
--testcase 122
SELECT * FROM t2;
--result 200

/* db-1 can still read the database but it cannot write the database
** since db-0 now has a read-lock.  Except in u1 which does not support
** concurrent access */
--db 1
--if $configuration<>'u1'
--testcase 123
SELECT * FROM t2;
--result 200
--endif
--testcase 124
INSERT INTO t2 VALUES(201);
--result SQLITE_BUSY {database is locked}

/* The db-0 lock escalates to a write-lock
**
** EVIDENCE-OF: R-39464-36566 The first time the database is written, an
** exclusive lock is obtained and held.
*/
--db 0
--testcase 125
INSERT INTO t2 VALUES(202);
SELECT y FROM t2 ORDER BY Y;
--result 200 202

/* db-1 can still read the database but it cannot write the database
** since db-0 now has a read-lock. */
--db 1
--testcase 126
SELECT * FROM t2;
--result SQLITE_BUSY {database is locked}

/* EVIDENCE-OF: R-50610-58991 Database locks obtained by a connection in
** EXCLUSIVE mode may be released either by closing the database
** connection, or by setting the locking-mode back to NORMAL using this
** pragma and then accessing the database file (for read or write).
**
** Case 1:  drop the lock by closing the connection.
*/
--close 0
--testcase 130
INSERT INTO t2 VALUES(203);
SELECT y FROM t2 ORDER BY y;
--result 200 202 203

/* Case 2 for R-50610-58991:  drop the exclusive lock using
** locking_mode=NORMAL.  Also add:
**
** EVIDENCE-OF: R-30183-02411 Simply setting the locking-mode to NORMAL
** is not enough - locks are not be released until the next time the
** database file is accessed.
*/
--db 0
--open test.db
--testcase 131
PRAGMA locking_mode=EXCLUSIVE;
DELETE FROM t2 WHERE y>200;
SELECT y FROM t2;
--result exclusive 200
--db 1
--testcase 132
SELECT * FROM t2;
--result SQLITE_BUSY {database is locked}
--db 0
--testcase 133
PRAGMA locking_mode=NORMAL;
PRAGMA main.locking_mode;
--result normal normal
--db 1
--testcase 134
SELECT * FROM t2;
--result SQLITE_BUSY {database is locked}
--db 0
--testcase 135
SELECT y FROM t2;
--result 200
--db 1
--testcase 136
SELECT y FROM t2;
--result 200

/* EVIDENCE-OF: R-35311-10820 When the locking_mode pragma specifies a
** particular database, for example: PRAGMA main.locking_mode=EXCLUSIVE;
** Then the locking mode applies only to the named database.
*/
--db 0
--new-filename test2.db
--testcase 200
ATTACH :filename AS db2;
PRAGMA db2.locking_mode=EXCLUSIVE;
CREATE TABLE db2.t3(z);
INSERT INTO t3 VALUES(300);
PRAGMA main.locking_mode;
PRAGMA db2.locking_mode;
--result exclusive normal exclusive

--db 1
--testcase 201
INSERT INTO t2 VALUES(204);
SELECT y FROM t2 ORDER BY y;
--result 200 204

--db 2
--open test2.db
--testcase 202
SELECT z FROM t3;
--result SQLITE_BUSY {database is locked}

--db 0
--testcase 203
PRAGMA db2.locking_mode=NORMAL;
SELECT z FROM t3;
--result normal 300

--db 2
--testcase 204
SELECT z FROM t3;
--result 300


/* EVIDENCE-OF: R-46677-03283 If no database name qualifier precedes the
** "locking_mode" keyword then the locking mode is applied to all
** databases, including any new databases added by subsequent ATTACH
** commands.
*/
--db 0
--open test.db
--convert-filename test2.db
--testcase 210
ATTACH :filename AS db2;
PRAGMA locking_mode=EXCLUSIVE;
INSERT INTO t2 VALUES(206);
INSERT INTO t3 VALUES(301);
--result exclusive
--new-filename test3.db
--testcase 211
ATTACH :filename AS db3;
CREATE TABLE db3.t4(w);
PRAGMA main.locking_mode;
PRAGMA db2.locking_mode;
PRAGMA db3.locking_mode;
--result exclusive exclusive exclusive

--db 1
--testcase 212
SELECT y FROM t2 ORDER BY y;
--result SQLITE_BUSY {database is locked}

--db 2
--testcase 213
SELECT z FROM t3;
--result SQLITE_BUSY {database is locked}

--db 3
--open test3.db
--testcase 214
SELECT w FROM t4;
--result SQLITE_BUSY {database is locked}

--db 0
--testcase 215
PRAGMA db2.locking_mode=NORMAL;
SELECT x FROM t1;
SELECT y FROM t2 ORDER BY y;
SELECT z FROM t3 ORDER BY z;
SELECT w FROM t4;
PRAGMA main.locking_mode;
PRAGMA db2.locking_mode;
PRAGMA db3.locking_mode;
--result normal 5 200 204 206 300 301 exclusive normal exclusive

--db 1
--testcase 216
SELECT y FROM t2 ORDER BY y;
--result SQLITE_BUSY {database is locked}

--db 2
--testcase 217
SELECT z FROM t3 ORDER BY z;
--result 300 301

--db 3
--testcase 218
SELECT w FROM t4;
--result SQLITE_BUSY {database is locked}

--db 0
--testcase 219
PRAGMA locking_mode=NORMAL;
SELECT x FROM t1;
SELECT y FROM t2 ORDER BY y;
SELECT z FROM t3 ORDER BY z;
SELECT w FROM t4;
PRAGMA main.locking_mode;
PRAGMA db2.locking_mode;
PRAGMA db3.locking_mode;
--result normal 5 200 204 206 300 301 normal normal normal

--db 1
--testcase 220
SELECT y FROM t2 ORDER BY y;
--result 200 204 206

--db 2
--testcase 221
SELECT z FROM t3 ORDER BY z;
--result 300 301

--db 3
--testcase 222
SELECT w FROM t4;
--result

/* EVIDENCE-OF: R-05196-58733 The "temp" database (in which TEMP tables
** and indices are stored) and in-memory databases always uses exclusive
** locking mode.
**
** EVIDENCE-OF: R-30526-43432 The locking mode of temp and in-memory
** databases cannot be changed.
*/
--db 0
--testcase 300
PRAGMA temp.locking_mode;
--result exclusive
--testcase 301
PRAGMA temp.locking_mode=NORMAL;
PRAGMA temp.locking_mode;
--result exclusive exclusive

--open :memory:
--testcase 310
PRAGMA main.locking_mode;
--result exclusive
--testcase 311
PRAGMA main.locking_mode=NORMAL;
PRAGMA main.locking_mode;
--result exclusive exclusive

/* EVIDENCE-OF: R-28307-32767 All other databases use the normal locking
** mode by default and are affected by this pragma.
*/
--new test.db
--testcase 320
PRAGMA main.locking_mode;
--result normal
--testcase 321
PRAGMA main.locking_mode=EXCLUSIVE;
PRAGMA main.locking_mode=NORMAL;
PRAGMA main.locking_mode;
--result exclusive normal normal
