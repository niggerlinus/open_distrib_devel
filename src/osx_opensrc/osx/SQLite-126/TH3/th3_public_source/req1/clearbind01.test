/*
** This module contains tests for the sqlite3_clear_bindings() interface.
**
** MODULE_NAME:               req1_clearbind01
** REQUIRED_PROPERTIES:
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/
int req1_clearbind01(th3state *p){
  sqlite3_stmt *pStmt;
  char zBuf[20];

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  pStmt = th3dbPrepare(p, 0, "SELECT ?,?,?");
  sqlite3_bind_int(pStmt, 1, 123);

  /* EVIDENCE-OF: R-31518-37056 If the destructor argument is
  ** SQLITE_STATIC, it means that the content pointer is constant and will
  ** never change.
  **
  ** By changing the value after binding and showing that the return is
  ** the new value we demonstrate SQLite continues to use the old pointer.
  ** Not that this only works if the encoding is UTF8.  Otherwise, SQLite
  ** makes its own copy in order to convert to UTF16.
  **
  ** EVIDENCE-OF: R-15807-22461 The SQLITE_TRANSIENT value means that the
  ** content will likely change in the near future and that SQLite should
  ** make its own private copy of the content before returning.
  **
  ** By changing the value after binding and showing that the original
  ** value is returned, we demonstrate that SQLite made its own copy.
  */
  if( p->config.maskProp & (TH3_UTF16LE|TH3_UTF16BE) ){
    th3strcpy(zBuf, "xyzzy");
  }else{
    th3strcpy(zBuf, "hello");
  }
  sqlite3_bind_text(pStmt, 2, zBuf, 5, SQLITE_STATIC);
  th3strcpy(zBuf, "abcde");
  sqlite3_bind_text(pStmt, 3, zBuf, 5, SQLITE_TRANSIENT);
  th3strcpy(zBuf, "xyzzy");

  sqlite3_step(pStmt);
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 0));
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 1));
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 2));
  th3testCheck(p, "123 xyzzy abcde");

  /* EVIDENCE-OF: R-00203-32193 Contrary to the intuition of many,
  ** sqlite3_reset() does not reset the bindings on a prepared statement.
  **
  ** EVIDENCE-OF: R-41719-58677 Any SQL statement variables that had values
  ** bound to them using the sqlite3_bind_*() API retain their values.
  **
  ** EVIDENCE-OF: R-55638-04244 The sqlite3_reset(S) interface does not
  ** change the values of any bindings on the prepared statement S.
  */
  th3testBegin(p, "110");
  sqlite3_reset(pStmt);
  sqlite3_step(pStmt);
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 0));
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 1));
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 2));
  th3testCheck(p, "123 xyzzy abcde");

  /* EVIDENCE-OF: R-56899-62057 Use this routine to reset all host
  ** parameters to NULL.
  */
  th3testBegin(p, "120");
  sqlite3_reset(pStmt);
  sqlite3_clear_bindings(pStmt);
  sqlite3_step(pStmt);
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 0));
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 1));
  th3testAppendResultTerm(p, (char*)sqlite3_column_text(pStmt, 2));
  sqlite3_finalize(pStmt);
  th3testCheck(p, "nil nil nil");

  return 0;
}
