/*
** This module contains tests for cache_size and default_cache_size PRAGMAs.
**
** SCRIPT_MODULE_NAME:        req1_pragma09
** REQUIRED_PROPERTIES:       TEST_VFS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** EVIDENCE-OF: R-20801-24767 PRAGMA cache_size; PRAGMA cache_size =
** Number-of-pages; Query or change the suggested maximum number of
** database disk pages that SQLite will hold in memory at once per open
** database file.
**
** EVIDENCE-OF: R-14357-33295 The default suggested cache size is 2000.
** (we're using a default cache size of 500)
*/
--raw-new test.db
--testcase 100
PRAGMA cache_size;
/* --result 2000 */
#ifdef TARGET_OS_EMBEDDED
--result 250
#else
--result 500
#endif
--testcase 101
PRAGMA cache_size=100;
PRAGMA cache_size;
--result 100
--testcase 102
PRAGMA cache_size=1;
PRAGMA cache_size;
--result 1
--testcase 103
PRAGMA cache_size=0;
PRAGMA cache_size;
--result 0

/* EVIDENCE-OF: R-43071-59205 The suggested cache size is set to the
** absolute value of the Number-of-pages argument given to this pragma.
*/
--testcase 110
PRAGMA cache_size(-300);
PRAGMA cache_size;
--result 300


/* EVIDENCE-OF: R-41134-27890 When you change the cache size using the
** cache_size pragma, the change only endures for the current session.
**
** EVIDENCE-OF: R-48331-62427 The cache size reverts to the default value
** when the database is closed and reopened.
*/
--raw-open test.db
--testcase 120
PRAGMA cache_size;
/* --result 2000 */
#ifdef TARGET_OS_EMBEDDED
--result 250
#else
--result 500
#endif

/* EVIDENCE-OF: R-27652-53388 PRAGMA default_cache_size; PRAGMA
** default_cache_size = Number-of-pages; This pragma queries or sets the
** suggested maximum number of pages of disk cache that will be allocated
** per open database file.
*/
--testcase 200
PRAGMA default_cache_size;
/* --result 2000 */
#ifdef TARGET_OS_EMBEDDED
--result 250
#else
--result 500
#endif
--testcase 201
PRAGMA default_cache_size=123;
PRAGMA default_cache_size;
--result 123
--testcase 202
PRAGMA default_cache_size=1;
PRAGMA default_cache_size;
--result 1
--testcase 203
PRAGMA default_cache_size = -234;
PRAGMA default_cache_size;
--result 234

/* EVIDENCE-OF: R-52058-53560 The difference between this pragma and
** cache_size is that the value set here persists across database
** connections.
*/
--raw-open test.db
--testcase 210
PRAGMA default_cache_size;
--result 234

/* EVIDENCE-OF: R-61141-39803 The value of the default cache size is
** stored in the 4-byte big-endian integer located at offset 48 in the
** header of the database file.
*/
--if (NOT $property_ZIPVFS) AND $property_CLEARTEXT
--testcase 220
seek 48 read32
--edit test.db
--result 234
--endif
