/*
** This module contains tests for datatypes as described in the
** datatype3.html document.
**
** SCRIPT_MODULE_NAME:        req1_datatype3_12
** REQUIRED_PROPERTIES:       TEST_VFS CLEARTEXT
** DISALLOWED_PROPERTIES:     MEMDB AUTOVACUUM INCRVACUUM
** MINIMUM_HEAPSIZE:          100000
*/
#ifndef SQLITE_OMIT_FLOATING_POINT

/* EVIDENCE-OF: R-31757-07599 As an internal optimization, small floating
** point values with no fractional component and stored in columns with
** REAL affinity are written to disk as integers in order to take up less
** space and are automatically converted back into floating point as the
** value is read out.
**
** EVIDENCE-OF: R-52422-13996 This optimization is completely invisible
** at the SQL level and can only be detected by examining the raw bits of
** the database file.
**
** We will test by writing candidate floating point numbers into a database
** then examining their representation.
*/
--testcase 100
CREATE TABLE t1(x REAL, y BLOB);
INSERT INTO t1(rowid,x,y) VALUES(64, 123.0, 123.0);
SELECT x, typeof(x), y, typeof(y) FROM t1;
--result 123.0 real 123.0 real
--checkpoint

--testcase 110
PRAGMA page_size;
--store $pgsz
seek $pgsz move $pgsz move -14 read 14
--edit test.db
--result 0c400301077b405ec00000000000
/*
** 0c is the size of the record
** 40 is the rowid
** 03 is the header size.  0107 is the header.
**     01 means 1-byte integer
**     07 means 8-byte floating point
** 7b is 123 in hex
** 405ec00000000000 is 123.0
**
** Hence we see that columns with REAL affinity can be stored as integers.
** QED.
*/
#endif /* !defined(SQLITE_OMIT_FLOATING_POINT) */
