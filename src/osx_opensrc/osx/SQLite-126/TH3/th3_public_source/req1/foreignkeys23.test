/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_foreignkeys23
** REQUIRED_PROPERTIES:       FOREIGN_KEYS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

#ifndef SQLITE_OMIT_FLOATING_POINT
/*
** EV: R-24728-13230 SQLite parses MATCH clauses (i.e. does not report a
** syntax error if you specify one), but does not enforce them.
**
** EV: R-24450-46174 All foreign key constraints in SQLite are handled as
** if MATCH SIMPLE were specified.
*/
--testcase 100
CREATE TABLE parent(a,b,c,PRIMARY KEY(a,b,c));
INSERT INTO parent VALUES(1,'one',1.0);

CREATE TABLE child0(x, y, z,
   FOREIGN KEY(x,y,z) REFERENCES parent
);
CREATE TABLE child1(x, y, z,
   FOREIGN KEY(x,y,z) REFERENCES parent MATCH SIMPLE
);
CREATE TABLE child2(x, y, z,
   FOREIGN KEY(x,y,z) REFERENCES parent MATCH PARTIAL
);
CREATE TABLE child3(x, y, z,
   FOREIGN KEY(x,y,z) REFERENCES parent MATCH FULL
);
--result
--testcase 110
INSERT INTO child0 VALUES(1,'one',1.0);
INSERT INTO child0 VALUES(2,'two',null);
INSERT INTO child0 VALUES(2,null,2.0);
INSERT INTO child0 VALUES(2,null,null);
INSERT INTO child0 VALUES(null,'two',2.0);
INSERT INTO child0 VALUES(null,'two',null);
INSERT INTO child0 VALUES(null,null,2.0);
INSERT INTO child0 VALUES(null,null,null);
--result
--testcase 120
INSERT INTO child1 SELECT * FROM child0;
--result
--testcase 130
INSERT INTO child2 SELECT * FROM child0;
--result
--testcase 140
INSERT INTO child3 SELECT * FROM child0;
--result
#endif /* SQLITE_OMIT_FLOATING_POINT */

/*
** EV: R-21599-16038 In SQLite, a foreign key constraint is permanently
** marked as deferred or immediate when it is created.
*/
--testcase 200
SET CONSTRAINT all IMMEDIATE;
--result SQLITE_ERROR {near "SET": syntax error}
--testcase 210
SET CONSTRAINT all DEFERRED;
--result SQLITE_ERROR {near "SET": syntax error}
