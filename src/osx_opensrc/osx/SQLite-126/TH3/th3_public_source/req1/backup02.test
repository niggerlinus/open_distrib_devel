/*
** This module contains tests for sqlite3_backup_init()
**
** MODULE_NAME:               req1_backup02
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     MEMDB SHARED_CACHE
** MINIMUM_HEAPSIZE:          100000
*/
int req1_backup02(th3state *p){
  sqlite3 *db0;
  sqlite3 *db1;
  sqlite3_backup *pBackup;
  int rc, i;
  const char *zErr8;
  const short int *pErr16;
  char zBuf[200];

  th3testBegin(p, "100");
  th3dbNew(p, 0, "test.db");
  th3dbNew(p, 2, "aux1.db"); th3dbClose(p, 2);
  th3convertFilename(p, "aux1.db", zBuf);
  th3bindText(p, "$aux1", zBuf);
  th3dbEval(p, 0, 
    "CREATE TABLE t1(a,b,c);"
    "INSERT INTO t1 VALUES(1,2,3);"
    "CREATE TEMP TABLE t2(x,y,z);"
    "INSERT INTO t2 VALUES(7,8,9);"
    "ATTACH $aux1 AS aux1;"
    "CREATE TABLE aux1.t3(p,q,r);"
    "INSERT INTO t3 VALUES(123,456,789);"
    "SELECT * FROM t1, t2, t3;"
  );
  th3testCheck(p, "1 2 3 7 8 9 123 456 789");

  th3testBegin(p, "110");
  th3dbNew(p, 1, "test2.db");
  th3dbNew(p, 2, "aux2.db"); th3dbClose(p, 2);
  th3convertFilename(p, "aux2.db", zBuf);
  th3bindText(p, "$aux2", zBuf);
  th3dbEval(p, 1, 
    "CREATE TABLE t4(d,e,f);"
    "INSERT INTO t4 VALUES(11,22,33);"
    "CREATE TEMP TABLE t5(u,v,w);"
    "INSERT INTO t5 VALUES(77,88,99);"
    "ATTACH $aux2 AS aux2;"
    "CREATE TABLE aux2.t6(m,n,o);"
    "INSERT INTO t6 VALUES(321,654,987);"
    "SELECT * FROM t4, t5, t6;"
  );
  th3testCheck(p, "11 22 33 77 88 99 321 654 987");

  /* EVIDENCE-OF: R-25824-45489 The D and N arguments to
  ** sqlite3_backup_init(D,N,S,M) are the database connection associated
  ** with the destination database and the database name, respectively.
  **
  ** EVIDENCE-OF: R-03718-56652 The database name is "main" for the main
  ** database, "temp" for the temporary database, or the name specified
  ** after the AS keyword in an ATTACH statement for an attached database.
  **
  ** EVIDENCE-OF: R-17313-07189 The S and M arguments passed to
  ** sqlite3_backup_init(D,N,S,M) identify the database connection and
  ** database name of the source database, respectively.
  **
  ** EVIDENCE-OF: R-18669-12610 A successful call to sqlite3_backup_init()
  ** returns a pointer to an sqlite3_backup object.
  **
  ** EVIDENCE-OF: R-47106-26961 The sqlite3_backup object may be used with
  ** the sqlite3_backup_step() and sqlite3_backup_finish() functions to
  ** perform the specified backup operation.
  */
  th3testBegin(p, "120");
  db0 = th3dbPointer(p, 0);
  db1 = th3dbPointer(p, 1);
  pBackup = sqlite3_backup_init(db1, "main", db0, "main");
  rc = sqlite3_backup_step(pBackup, -1);
  th3testCheckInt(p, SQLITE_DONE, rc);
  sqlite3_backup_finish(pBackup);
  th3dbEval(p, 1, "SELECT * FROM main.t1, temp.t5, aux2.t6");
  th3testCheck(p, "1 2 3 77 88 99 321 654 987");
  th3testBegin(p, "121");
  pBackup = sqlite3_backup_init(db1, "temp", db0, "aux1");
  rc = sqlite3_backup_step(pBackup, -1);
#if SQLITE_TEMP_STORE==3 || defined(SQLITE_HAS_CODEC)
  /* If the temp database is in memory, then a pagesize change might
  ** prevent the backup from occurring. */
  th3testCheckTrue(p, rc==SQLITE_DONE || rc==SQLITE_READONLY);
#else
  /* Must succeed if TEMP store is no memory */
  th3testCheckInt(p, SQLITE_DONE, rc);
#endif
  sqlite3_backup_finish(pBackup);
  if( rc==SQLITE_READONLY ) return 0;
  th3dbEval(p, 1, "SELECT * FROM main.t1, temp.t3, aux2.t6");
  th3testCheck(p, "1 2 3 123 456 789 321 654 987");
  th3testBegin(p, "122");
  pBackup = sqlite3_backup_init(db1, "aux2", db0, "temp");
  rc = sqlite3_backup_step(pBackup, -1);
  th3testCheckInt(p, SQLITE_DONE, rc);
  sqlite3_backup_finish(pBackup);
  th3dbEval(p, 1, "SELECT * FROM main.t1, aux2.t2, temp.t3");
  th3testCheck(p, "1 2 3 7 8 9 123 456 789");

  /* Reset the destination database */
  th3testBegin(p, "130");
  th3dbNew(p, 1, "test2.db");
  th3dbNew(p, 2, "aux2.db"); th3dbClose(p, 2);
  th3convertFilename(p, "aux1.db", zBuf);
  th3bindText(p, "$aux2", zBuf);
  th3dbEval(p, 1, 
    "CREATE TABLE t4(d,e,f);"
    "INSERT INTO t4 VALUES(11,22,33);"
    "CREATE TEMP TABLE t5(u,v,w);"
    "INSERT INTO t5 VALUES(77,88,99);"
    "ATTACH $aux2 AS aux2;"
    "CREATE TABLE aux2.t6(m,n,o);"
    "INSERT INTO t6 VALUES(321,654,987);"
    "SELECT * FROM t4, t5, t6;"
  );
  th3testCheck(p, "11 22 33 77 88 99 321 654 987");
  db1 = th3dbPointer(p, 1);

  /* EVIDENCE-OF: R-56075-46525 The source and destination database
  ** connections (parameters S and D) must be different or else
  ** sqlite3_backup_init(D,N,S,M) will fail with an error.
  **
  ** EVIDENCE-OF: R-17817-46062 If an error occurs within
  ** sqlite3_backup_init(D,N,S,M), then NULL is returned and an error code
  ** and error message are stored in the destination database connection D.
  **
  ** EVIDENCE-OF: R-31338-54830 The error code and message for the failed
  ** call to sqlite3_backup_init() can be retrieved using the
  ** sqlite3_errcode(), sqlite3_errmsg(), and/or sqlite3_errmsg16()
  ** functions.
  */
  th3testBegin(p, "140");
  pBackup = sqlite3_backup_init(db0, "temp", db0, "main");
  th3testCheckTrue(p, pBackup==0 );
  th3testBegin(p, "141");
  rc = sqlite3_errcode(db0);
  th3testCheckInt(p, SQLITE_ERROR, rc);
  th3testBegin(p, "142");
  zErr8 = sqlite3_errmsg(db0);
  th3testCheckString(p, "source and destination must be distinct", zErr8);
  th3testBegin(p, "143");
  pErr16 = sqlite3_errmsg16(db0);
  for(i=0; (zBuf[i] = (char)pErr16[i])!=0; i++){}
  th3testCheckString(p, "source and destination must be distinct", zBuf);
  th3testBegin(p, "150");
  th3dbOpen(p, 0, "test.db", 0);
  db0 = th3dbPointer(p, 0);
  pBackup = sqlite3_backup_init(db1, "xyzzy", db0, "main");
  th3testCheckTrue(p, pBackup==0 );
  th3testBegin(p, "151");
  rc = sqlite3_errcode(db0);
  th3testCheckInt(p, SQLITE_OK, rc);
  th3testBegin(p, "152");
  rc = sqlite3_errcode(db1);
  th3testCheckInt(p, SQLITE_ERROR, rc);
  th3testBegin(p, "153");
  zErr8 = sqlite3_errmsg(db1);
  th3testCheckString(p, "unknown database xyzzy", zErr8);
  th3testBegin(p, "154");
  pErr16 = sqlite3_errmsg16(db1);
  for(i=0; (zBuf[i] = (char)pErr16[i])!=0; i++){}
  th3testCheckString(p, "unknown database xyzzy", zBuf);


  return 0;
}
