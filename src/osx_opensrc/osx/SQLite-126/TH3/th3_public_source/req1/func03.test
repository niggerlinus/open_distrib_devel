/*
** This module contains tests for lang_func.html
**
** SCRIPT_MODULE_NAME:        req1_func03
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:
** MINIMUM_HEAPSIZE:          100000
*/

/* EVIDENCE-OF: R-60099-10331 The multi-argument max() function returns
** the argument with the maximum value, or return NULL if any argument is
** NULL.
*/
--testcase 100
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b ANY COLLATE binary,
  c ANY COLLATE nocase
);
INSERT INTO t1 VALUES(1, 'abc', 'DEF');
INSERT INTO t1 VALUES(2, 'BCD', 'def');
INSERT INTO t1 VALUES(3, null, 'xyz');
INSERT INTO t1 VALUES(4, 'xyz', null);
SELECT a, max(b,c) FROM t1 ORDER BY a;
--result 1 abc 2 def 3 nil 4 nil
--testcase 111
SELECT a, max(b,c,'pqr',b,c,'wxy') FROM t1 ORDER BY a;
--result 1 wxy 2 wxy 3 nil 4 nil

/* EVIDENCE-OF: R-41246-06467 The multi-argument max() function searches
** its arguments from left to right for an argument that defines a
** collating function and uses that collating function for all string
** comparisons.
*/
--testcase 110
SELECT a, max(b,c,'pqr',b,c,'WXY') FROM t1 ORDER BY a;
--result 1 pqr 2 pqr 3 nil 4 nil
--testcase 111
SELECT a, max(c,b,c,'pqr',b,c,'WXY') FROM t1 ORDER BY a;
--result 1 WXY 2 WXY 3 nil 4 nil
--testcase 112
SELECT a, max('abc','def','pqr','WXY',c,b) FROM t1 ORDER BY a;
--result 1 WXY 2 WXY 3 nil 4 nil
--testcase 113
SELECT a, max('abc','DEF','pqr','WXY',b,c) FROM t1 ORDER BY a;
--result 1 pqr 2 pqr 3 nil 4 nil

/* EVIDENCE-OF: R-63138-42446 If none of the arguments to max() define a
** collating function, then the BINARY collating function is used.
*/
--testcase 114
SELECT a, max('abc','DEF','pqr','WXY','b','c') FROM t1 ORDER BY a;
--result 1 pqr 2 pqr 3 pqr 4 pqr

/* EVIDENCE-OF: R-02811-30532 The multi-argument min() function returns
** the argument with the minimum value.
*/
--testcase 120
DELETE FROM t1;
INSERT INTO t1 VALUES(1, 'abc', 'DEF');
INSERT INTO t1 VALUES(2, 'BCD', 'def');
INSERT INTO t1 VALUES(3, null, 'xyz');
INSERT INTO t1 VALUES(4, 'xyz', null);
SELECT a, min(b,c) FROM t1 ORDER BY a;
--result 1 DEF 2 BCD 3 nil 4 nil
--testcase 121
SELECT a, min(b,c,'pqr',b,c,'wxy') FROM t1 ORDER BY a;
--result 1 DEF 2 BCD 3 nil 4 nil

/* EVIDENCE-OF: R-17066-08509 The multi-argument min() function searches
** its arguments from left to right for an argument that defines a
** collating function and uses that collating function for all string
** comparisons.
*/
--testcase 130
SELECT a, min(b,c,'pqr',b,c,'WXY') FROM t1 ORDER BY a;
--result 1 DEF 2 BCD 3 nil 4 nil
--testcase 131
SELECT a, min(c,b,c,'pqr',b,c,'WXY') FROM t1 ORDER BY a;
--result 1 abc 2 BCD 3 nil 4 nil
--testcase 132
SELECT a, min('aaa','def','pqr','WXY',c,b) FROM t1 ORDER BY a;
--result 1 aaa 2 aaa 3 nil 4 nil
--testcase 133
SELECT a, min('aaa','DEF','pqr','WXY',b,c) FROM t1 ORDER BY a;
--result 1 DEF 2 BCD 3 nil 4 nil

/* EVIDENCE-OF: R-47421-29468 If none of the arguments to min() define a
** collating function, then the BINARY collating function is used.
*/
--testcase 134
SELECT a, min('aaa','DEF','pqr','WXY') FROM t1 ORDER BY a;
--result 1 DEF 2 DEF 3 DEF 4 DEF

/* EVIDENCE-OF: R-29649-49812 The nullif(X,Y) function returns its first
** argument if the arguments are different and NULL if the arguments are
** the same.
*/
--testcase 200
SELECT th3_load_normal_extension();
--run 0
DELETE FROM t1;
INSERT INTO t1 VALUES(1, 'abc', 'ABC');
INSERT INTO t1 VALUES(2, 'DEF', 'def');
INSERT INTO t1 VALUES(3, null, 'xyz');
INSERT INTO t1 VALUES(4, 'xyz', null);
SELECT a, nullif(b,c) FROM t1;
--result 1 abc 2 DEF 3 nil 4 xyz

/* EVIDENCE-OF: R-64107-03133 The nullif(X,Y) function searches its
** arguments from left to right for an argument that defines a collating
** function and uses that collating function for all string comparisons.
*/
--testcase 210
SELECT a, nullif(b,c), nullif(c,b) FROM t1;
--result 1 abc nil 2 DEF nil 3 nil xyz 4 xyz nil

/* EVIDENCE-OF: R-00862-05326 If neither argument to nullif() defines a
** collating function then the BINARY is used.
*/
--testcase 220
SELECT nullif('abc','ABC');
--result abc

/* EVIDENCE-OF: R-13846-35797 The random() function returns a
** pseudo-random integer between -9223372036854775808 and
** +9223372036854775807.
**
** Note:  There is, in theory, a very small chance that this test will fail
** since the same random value could appear twice in a row.  But the
** chances of that are vanishingly small.
*/
--testcase 300
SELECT random() BETWEEN -9223372036854775808 and 9223372036854775807,
       random()==random(),
       typeof(random());
--result 1 0 integer

/* EVIDENCE-OF: R-45939-02717 The randomblob(N) function return an N-byte
** blob containing pseudo-random bytes.
*/
--testcase 400
SELECT length(randomblob(5)),typeof(randomblob(5)),randomblob(5)=randomblob(5);
--result 5 blob 0
--testcase 401
SELECT length(randomblob(1)),typeof(randomblob(1));
--result 1 blob

/* EVIDENCE-OF: R-11061-58747 If N is less than 1 then a 1-byte random
** blob is returned.
*/
--testcase 410
SELECT length(randomblob(0)), typeof(randomblob(0));
--result 1 blob
--testcase 411
SELECT length(randomblob(-9223372036854775808)),
       typeof(randomblob(-9223372036854775808));
--result 1 blob

/* EVIDENCE-OF: R-21182-58169 The replace(X,Y,Z) function returns a
** string formed by substituting string Z for every occurrence of string
** Y in string X.
*/
--testcase 500
SELECT replace('abcdcbcddccbbabc','abc','x');
--result xdcbcddccbbx
--testcase 501
SELECT replace('abcdcbcddccbbabc','bc','xxxxx');
--result axxxxxdcxxxxxddccbbaxxxxx
--testcase 502
SELECT replace('abcdcbcdddccbbabc','dd','xxxxxxxxxxxxxx');
--result abcdcbcxxxxxxxxxxxxxxdccbbabc
--testcase 503
SELECT replace('abcdcbcddccbbabc','a','');
--result bcdcbcddccbbbc
--testcase 504
SELECT replace('abcdcbcddccbbabc','b','');
--result acdccddccac

/* EVIDENCE-OF: R-24454-61742 The BINARY collating sequence is used for
** comparisons.
*/
--testcase 510
SELECT replace('abcdcbcddccbbabc' COLLATE nocase,'BCD','x');
--result abcdcbcddccbbabc

/* EVIDENCE-OF: R-50678-57776 If Y is an empty string then return X
** unchanged.
*/
--testcase 520
SELECT replace('abcdcbcddccbbabc','','xyzzy');
--result abcdcbcddccbbabc

/* EVIDENCE-OF: R-42393-33810 If Z is not initially a string, it is cast
** to a UTF-8 string prior to processing.
*/
--testcase 530
SELECT replace(123454321, 34, 'x');
--result 12x54321
#ifndef SQLITE_OMIT_FLOATING_POINT
--testcase 531
SELECT replace(12345432, 43, 'e+')+1;
--result 1234501.0
#endif

/* EVIDENCE-OF: R-23317-43079 The rtrim(X,Y) function returns a string
** formed by removing any and all characters that appear in Y from the
** right side of X.
*/
--testcase 600
SELECT rtrim('abcdefghijk','abcijkde');
--result abcdefgh
--testcase 601
SELECT rtrim('abcdefghijkkjijkiijkii','abcixjykde');
--result abcdefgh
--testcase 602
SELECT rtrim('abcdefgijkhijkkjijkiijkii','abcixjykde');
--result abcdefgijkh

/* EVIDENCE-OF: R-58312-28618 If the Y argument is omitted, rtrim(X)
** removes spaces from the right side of X.
*/
--testcase 610
SELECT rtrim('abcdefghijk');
--result abcdefghijk
--testcase 611
SELECT rtrim('   abcdefghijk   ');
--result {   abcdefghijk}

/* EVIDENCE-OF: R-50629-09283 The trim(X,Y) function returns a string
** formed by removing any and all characters that appear in Y from both
** ends of X.
*/
--testcase 620
SELECT trim('jkabcdefghijk','abcijkde');
--result fgh
--testcase 621
SELECT trim('abcdefghijkkjijkiijkii','abcixjykde');
--result fgh
--testcase 622
SELECT trim('abcdefgijkhijkkjijkiijkii','abcixjykde');
--result fgijkh

/* EVIDENCE-OF: R-42511-45809 If the Y argument is omitted, trim(X)
** removes spaces from both ends of X.
*/
--testcase 630
SELECT trim('abcdefghijk');
--result abcdefghijk
--testcase 631
SELECT trim('   abcdefghijk   ');
--result abcdefghijk

#if !defined(SQLITE_OMIT_DATETIME_FUNCS) && !defined(SQLITE_OMIT_FLOATING_POINT)
/* EVIDENCE-OF: R-64181-14613 The string returned by sqlite_source_id()
** begins with the date and time that the source code was checked in and
** is follows by an SHA1 hash that uniquely identifies the source tree.
*/
--testcase 700
CREATE TABLE t700(x);
INSERT INTO t700 VALUES(substr(sqlite_source_id(),1,19));
SELECT datetime(x)=x FROM t700;
--result 1
--if NOT $property_TEST_VFS
--testcase 701a
SELECT julianday(x) BETWEEN julianday('2010-01-01') AND julianday('now')
  FROM t700;
--result 1
--endif
--testcase 701b
SELECT julianday(x)>julianday('2010-01-01')
  FROM t700;
--result 1
--testcase 702
DELETE FROM t700;
INSERT INTO t700 VALUES(substr(sqlite_source_id(),21,40));
SELECT th3eval('SELECT length(x''' || x || ''');') FROM t700;
--result 20
#endif
