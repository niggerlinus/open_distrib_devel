/*
** This module contains tests for foreign key functionality and the source
** code in fkey.c.
**
** SCRIPT_MODULE_NAME:        req1_pragma02
** REQUIRED_PROPERTIES:       FOREIGN_KEYS
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

--testcase 100
/* EV: R-48270-44282 Foreign key ON DELETE and ON UPDATE clauses are used
** to configure actions that take place when deleting rows from the parent
** table (ON DELETE), or modifying the parent key values of existing rows
** (ON UPDATE). 
*/
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b REFERENCES t1 ON UPDATE CASCADE ON DELETE CASCADE
);
INSERT INTO t1 VALUES(1, null);
INSERT INTO t1 VALUES(2, 1);
INSERT INTO t1 VALUES(3, 1);
INSERT INTO t1 VALUES(4, 2);
INSERT INTO t1 VALUES(5, 2);
INSERT INTO t1 VALUES(6, 3);
INSERT INTO t1 VALUES(7, 3);
--result
--testcase 101
PRAGMA count_changes = ON;
UPDATE t1 SET a=22 WHERE a=2;
--result 1
--testcase 102
SELECT b FROM t1 WHERE a IN (4,5);
--result 22 22
--testcase 103
DELETE FROM t1 WHERE a=3;
--result 1
--testcase 104
SELECT b FROM t1 WHERE a IN (6,7);
--result

--testcase 110
PRAGMA count_changes(off);
DROP TABLE t1;
/* EV: R-48124-63225 A single foreign key constraint may have different
** actions configured for ON DELETE and ON UPDATE.
*/
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b DEFAULT 1 REFERENCES t1 ON UPDATE SET NULL ON DELETE SET DEFAULT
);
INSERT INTO t1 VALUES(1, null);
INSERT INTO t1 VALUES(2, 1);
INSERT INTO t1 VALUES(3, 1);
INSERT INTO t1 VALUES(4, 2);
INSERT INTO t1 VALUES(5, 2);
INSERT INTO t1 VALUES(6, 3);
INSERT INTO t1 VALUES(7, 3);
--result
--testcase 111
PRAGMA count_changes = ON;
UPDATE t1 SET a=22 WHERE a=2;
--result 1
--testcase 112
SELECT b FROM t1 WHERE a IN (4,5);
--result nil nil
--testcase 113
DELETE FROM t1 WHERE a=3;
--result 1
--testcase 114
SELECT b FROM t1 WHERE a IN (6,7);
--result 1 1


--testcase 120
/* EV: R-48124-63225 A single foreign key constraint may have different
** actions configured for ON DELETE and ON UPDATE.
*/
PRAGMA count_changes = false;
DROP TABLE t1;
CREATE TABLE t1(
  a INTEGER PRIMARY KEY,
  b DEFAULT 1 REFERENCES t1 ON UPDATE SET DEFAULT ON DELETE SET NULL
);
INSERT INTO t1 VALUES(1, null);
INSERT INTO t1 VALUES(2, 1);
INSERT INTO t1 VALUES(3, 1);
INSERT INTO t1 VALUES(4, 2);
INSERT INTO t1 VALUES(5, 2);
INSERT INTO t1 VALUES(6, 3);
INSERT INTO t1 VALUES(7, 3);
--result
--testcase 121
PRAGMA count_changes = ON;
UPDATE t1 SET a=22 WHERE a=2;
--result 1
--testcase 122
SELECT b FROM t1 WHERE a IN (4,5);
--result 1 1
--testcase 123
DELETE FROM t1 WHERE a=3;
--result 1
--testcase 102
SELECT b FROM t1 WHERE a IN (6,7);
--result nil nil

--testcase 130
PRAGMA count_changes = OFF;
CREATE TABLE t2(x);
CREATE TABLE t2log(y);
CREATE TRIGGER t2r1 AFTER INSERT ON t2 BEGIN
  INSERT INTO t2log VALUES(new.x);
  DELETE FROM t2log WHERE y=new.x/2;
  UPDATE t2log SET y=y+1 WHERE y=new.x*2;
END;
--result
--testcase 131
PRAGMA count_changes([true]);
INSERT INTO t2 VALUES(16);
--result 1
--testcase 132
SELECT * FROM t2log ORDER BY y;
--result 16
--testcase 133
INSERT INTO t2 VALUES(32);
--result 1
--testcase 134
SELECT * FROM t2log ORDER BY y;
--result 32
--testcase 135
INSERT INTO t2 VALUES(16);
--result 1
--testcase 136
SELECT * FROM t2log ORDER BY y;
--result 16 33
