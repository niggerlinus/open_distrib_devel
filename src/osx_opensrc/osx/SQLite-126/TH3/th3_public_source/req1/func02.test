/*
** This module contains tests for lang_func.html
**
** MODULE_NAME:               req1_func02
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     THREADS
** MINIMUM_HEAPSIZE:          100000
*/

/*
** Global variables used to control and record the substitute GLOB/LIKE
** function implementation given below.
*/
static struct {
  int typeReturn;    /* SQLITE_INTEGER, SQLITE_FLOAT, SQLITE_TEXT, ... */
  int iReturn;       /* Value to return for SQLITE_INTEGER */
  double rReturn;    /* Value to return for SQLITE_FLOAT */
  char zReturn[100]; /* Value for return for SQLITE_TEXT or SQLITE_BLOB */
  int nReturn;       /* Number of bytes in SQLITE_BLOB return */
  int nCall;         /* Incremented with each call */
  int nArg;          /* Number of arguments on most recent call */
  char azArg[3][10]; /* Copy of arguments for most recent call */
} req1_func02_g;

/*
** This routine is a substitute GLOB or LIKE function implementation.
**
** The value returned is determined by settings in the req1_func02_g
** structure.  Statistics about this call are collected into that
** same structure.  Because req1_func02_g is a global structure and
** no locking is used, this test module is not threadsafe.
**
** EVIDENCE-OF: R-24505-23230 A pointer to an sqlite3_context object is
** always first parameter to application-defined SQL functions.
*/
static void req1_func02_globlike(
  sqlite3_context *context,
  int argc,
  sqlite3_value **argv
){
  int i;
  req1_func02_g.nCall++;
  req1_func02_g.nArg = argc;
  for(i=0; i<argc && i<3; i++){
    sqlite3_snprintf(10, req1_func02_g.azArg[i], "%s", 
                     sqlite3_value_text(argv[i]));
  }
  switch( req1_func02_g.typeReturn ){
    case SQLITE_INTEGER: {
      sqlite3_result_int(context, req1_func02_g.iReturn);
      break;
    }
    case SQLITE_FLOAT: {
      sqlite3_result_double(context, req1_func02_g.rReturn);
      break;
    }
    case SQLITE_TEXT: {
      sqlite3_result_text(context, req1_func02_g.zReturn, -1, SQLITE_TRANSIENT);
      break;
    }
    case SQLITE_BLOB: {
      sqlite3_result_blob(context, req1_func02_g.zReturn, req1_func02_g.nReturn,
                          SQLITE_TRANSIENT);
      break;
    }
    case SQLITE_NULL: {
      sqlite3_result_null(context);
      break;
    }
    default: {
      sqlite3_result_error(context, req1_func02_g.zReturn, -1);
      sqlite3_result_error_code(context, req1_func02_g.iReturn);
      break;
    }
  }
}

/*
** Test implementation
*/
int req1_func02(th3state *p){
  th3dbNew(p, 0, "test.db");

  /* EVIDENCE-OF: R-45676-18204 If the sqlite3_create_function() interface
  ** is used to override the glob(X,Y) function with an alternative
  ** implementation then the GLOB operator will invoke the alternative
  ** implementation.
  */
  th3testBegin(p, "100");
  sqlite3_create_function(th3dbPointer(p, 0), "glob", 2, SQLITE_UTF8,
                          0, req1_func02_globlike, 0, 0);
  memset(&req1_func02_g, 0, sizeof(req1_func02_g));
  req1_func02_g.typeReturn = SQLITE_INTEGER;
  req1_func02_g.iReturn = 123;
  th3dbEval(p, 0, "SELECT 'abc' GLOB 'ab?'");
  th3testCheck(p, "123");
  th3testBegin(p, "101");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "102");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"ab?")==0, 1);
  th3testBegin(p, "103");
  th3testCheckInt(p, req1_func02_g.nArg, 2);

#ifndef SQLITE_OMIT_FLOATING_POINT
  th3testBegin(p, "110");
  memset(&req1_func02_g, 0, sizeof(req1_func02_g));
  req1_func02_g.typeReturn = SQLITE_FLOAT;
  req1_func02_g.rReturn = 123.5;
  th3dbEval(p, 0, "SELECT 'abc' GLOB 'ab*'");
  th3testCheck(p, "123.5");
  th3testBegin(p, "111");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "112");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"ab*")==0, 1);
  th3testBegin(p, "113");
  th3testCheckInt(p, req1_func02_g.nArg, 2);
#endif

  th3testBegin(p, "120");
  memset(&req1_func02_g, 0, sizeof(req1_func02_g));
  req1_func02_g.typeReturn = SQLITE_TEXT;
  strcpy(req1_func02_g.zReturn,"string value [120]");
  th3dbEval(p, 0, "SELECT 'abc' GLOB 'a*c'");
  th3testCheck(p, "{string value [120]}");
  th3testBegin(p, "121");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "122");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"a*c")==0, 1);
  th3testBegin(p, "123");
  th3testCheckInt(p, req1_func02_g.nArg, 2);

  th3testBegin(p, "130");
  memset(&req1_func02_g, 0, sizeof(req1_func02_g));
  req1_func02_g.typeReturn = SQLITE_BLOB;
  strcpy(req1_func02_g.zReturn,"string value [120]");
  req1_func02_g.nReturn = 4;
  th3dbEval(p, 0, "SELECT quote('abc' GLOB 'a*c')");
  th3testCheck(p, "X'73747269'");
  th3testBegin(p, "131");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "132");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"a*c")==0, 1);
  th3testBegin(p, "133");
  th3testCheckInt(p, req1_func02_g.nArg, 2);

  th3testBegin(p, "140");
  memset(&req1_func02_g, 0, sizeof(req1_func02_g));
  req1_func02_g.typeReturn = SQLITE_NULL;
  th3dbEval(p, 0, "SELECT 'abc' GLOB 'a*c'");
  th3testCheck(p, "nil");
  th3testBegin(p, "141");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "142");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"a*c")==0, 1);
  th3testBegin(p, "143");
  th3testCheckInt(p, req1_func02_g.nArg, 2);

  th3testBegin(p, "150");
  req1_func02_g.typeReturn = -1;
  strcpy(req1_func02_g.zReturn,"error text");
  req1_func02_g.iReturn = SQLITE_TOOBIG;
  th3dbEval(p, 0, "SELECT 'abc' GLOB 'a*c'");
  th3testCheck(p, "SQLITE_TOOBIG {error text}");
  th3testBegin(p, "151");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "152");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"a*c")==0, 1);
  th3testBegin(p, "153");
  th3testCheckInt(p, req1_func02_g.nArg, 2);

  /* EVIDENCE-OF: R-47394-08393 The like() function is used to implement
  ** the "Y LIKE X [ESCAPE Z]" expression.
  **
  ** EVIDENCE-OF: R-29639-49338 The sqlite3_create_function() interface can
  ** be used to override the like() function and thereby change the
  ** operation of the LIKE operator.
  */
  th3testBegin(p, "200");
  sqlite3_create_function(th3dbPointer(p, 0), "like", 2, SQLITE_UTF8,
                          0, req1_func02_globlike, 0, 0);
  sqlite3_create_function(th3dbPointer(p, 0), "like", 3, SQLITE_UTF8,
                          0, req1_func02_globlike, 0, 0);
  memset(&req1_func02_g, 0, sizeof(req1_func02_g));
  req1_func02_g.typeReturn = SQLITE_INTEGER;
  req1_func02_g.iReturn = 1;
  th3dbEval(p, 0, "SELECT 'abc' LIKE 'ab%'");
  th3testCheck(p, "1");
  th3testBegin(p, "201");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "202");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"ab%")==0, 1);
  th3testBegin(p, "203");
  th3testCheckInt(p, req1_func02_g.nArg, 2);

  /* EVIDENCE-OF: R-38973-52184 If the optional ESCAPE clause is present,
  ** then the like() function is invoked with three arguments.
  **
  ** EVIDENCE-OF: R-24738-46982 Otherwise, it is invoked with two arguments
  ** only.
  */
  th3testBegin(p, "210");
  th3dbEval(p, 0, "SELECT 'abc' LIKE 'ab%' ESCAPE 'xyz'");
  th3testCheck(p, "1");
  th3testBegin(p, "211");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[1],"abc")==0, 1);
  th3testBegin(p, "212");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[0],"ab%")==0, 1);
  th3testBegin(p, "213");
  th3testCheckInt(p, req1_func02_g.nArg, 3);
  th3testBegin(p, "214");
  th3testCheckInt(p, strcmp(req1_func02_g.azArg[2],"xyz")==0, 1);


  return 0;
}
