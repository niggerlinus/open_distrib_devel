/*
** This module contains tests for the foreign_key_list pragma
**
** SCRIPT_MODULE_NAME:        req1_pragma17
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
**
** EVIDENCE-OF: R-25198-00929 PRAGMA foreign_key_list(table-name); This
** pragma returns one row for each foreign key that references a column
** in the argument table.
*/
--testcase 100
CREATE TABLE t1(a INTEGER PRIMARY KEY, b, c, UNIQUE(b,c));
PRAGMA foreign_key_list(t1);
--result

--testcase 110
CREATE TABLE t2(x INTEGER UNIQUE REFERENCES t1);
PRAGMA foreign_key_list(t1);
--result

--testcase 120
PRAGMA foreign_key_list(t2);
--result 0 0 t1 x nil {NO ACTION} {NO ACTION} NONE

--testcase 130
CREATE TABLE t3(
  p INTEGER REFERENCES t1(a) ON DELETE cascade ON UPDATE restrict,
  q INTEGER REFERENCES t2(x),
  r, s,
  FOREIGN KEY(r,s) REFERENCES t1(b,c) ON DELETE set null
);
PRAGMA foreign_key_list(t3);
--result 0 0 t1 r b {NO ACTION} {SET NULL} NONE 0 1 t1 s c {NO ACTION} {SET NULL} NONE 1 0 t2 q x {NO ACTION} {NO ACTION} NONE 2 0 t1 p a RESTRICT CASCADE NONE
