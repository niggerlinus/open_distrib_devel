/*
** This module contains tests for lang_conflict.html
**
** SCRIPT_MODULE_NAME:        req1_conflict03
** REQUIRED_PROPERTIES:       
** DISALLOWED_PROPERTIES:     
** MINIMUM_HEAPSIZE:          100000
*/

# EVIDENCE-OF: R-55069-12282 The default conflict resolution algorithm
# is ABORT.
#
# EVIDENCE-OF: R-05239-12850 If no algorithm is specified anywhere, the
# ABORT algorithm is used.

/* INSERT  on a PRIMARY KEY */
--testcase 313
CREATE TABLE t1(x PRIMARY KEY);
CREATE TABLE data(x);
INSERT INTO data VALUES(1);
INSERT INTO data VALUES(2);
INSERT INTO data VALUES(3);
BEGIN;
INSERT INTO t1 VALUES(2);
INSERT INTO t1 SELECT x FROM data;
--result SQLITE_CONSTRAINT {column x is not unique}
--testcase 314
SELECT * FROM t1;
--result 2
--testcase 315
ROLLBACK;
--result

/* UPDATE on a PRIMARY KEY */
--testcase 316
DELETE FROM t1;
INSERT INTO t1 SELECT x FROM data;
BEGIN;
UPDATE t1 SET x=99 WHERE x=3;
UPDATE t1 SET x=100;
--result SQLITE_CONSTRAINT {column x is not unique}
--testcase 317
SELECT * FROM t1;
--result 1 2 99
--testcase 318
ROLLBACK;
--result


/* INSERT on a UNIQUE */
--testcase 323
CREATE TABLE t2(x UNIQUE);
BEGIN;
INSERT INTO t2 VALUES(2);
INSERT INTO t2 SELECT x FROM data;
--result SQLITE_CONSTRAINT {column x is not unique}
--testcase 324
SELECT * FROM t2;
--result 2
--testcase 325
ROLLBACK;
--result

/* UPDATE on a UNIQUE */
--testcase 326
DELETE FROM t2;
INSERT INTO t2 SELECT x FROM data;
BEGIN;
UPDATE t2 SET x=x+99 WHERE x=3;
UPDATE t2 SET x=x+100;
--result SQLITE_CONSTRAINT {column x is not unique}
--testcase 327
SELECT * FROM t2;
--result 1 2 102
--testcase 328
ROLLBACK;
--result

/* INSERT on a NOT NULL */
--testcase 333
DELETE FROM data;
INSERT INTO data VALUES(1);
INSERT INTO data VALUES(null);
INSERT INTO data VALUES(3);
CREATE TABLE t3(x NOT NULL);
BEGIN;
INSERT INTO t3 VALUES(99);
INSERT INTO t3 SELECT x FROM data;
--result SQLITE_CONSTRAINT {t3.x may not be NULL}
--testcase 334
SELECT * FROM t3;
--result 99
--testcase 335
ROLLBACK;
--result

/* UPDATE on a NOT NULL */
--testcase 336
DELETE FROM t3;
INSERT INTO t3 VALUES(1);
INSERT INTO t3 VALUES(2);
INSERT INTO t3 VALUES(3);
BEGIN;
UPDATE t3 SET x=99 WHERE x=3;
UPDATE t3 SET x=nullif(x+100,102);
--result SQLITE_CONSTRAINT {t3.x may not be NULL}
--testcase 337
SELECT * FROM t3;
--result 1 2 99
--testcase 338
ROLLBACK;
--result
