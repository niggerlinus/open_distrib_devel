This is the required README for the src.

This project is based on CLFS at this time.

========================================
NEEDED SOFTWARE
========================================
CURRENT		|	ALTERNATIVE
----------------------------------------
linux 		- 	linux
gnome		- 	enlightenment
gcc		- 	clang
binutils	- 	elftoolchain
python2		- 	python-dev
git		- 	git-dev
svn		- 	svn-nightly
nano/emacs	- 	pico
bash		-	korn/zsh
tcl		-	tcl
tk		-	tk
coreutils	-	coreutils (no suitable replacement)
diffutils
=======================================
WANTED SOFTWARE
=======================================
GNU-Free software
