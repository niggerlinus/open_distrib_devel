<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V4.2//EN" [
<!ENTITY % defs SYSTEM "X11/defs.ent"> %defs;
]>

<Article>

<articleinfo>

<Title>X.Org and XFree86 Version Numbering Schemes</Title>
<AUTHOR>
<FirstName>The XFree86 Project, Inc</FirstName>
</AUTHOR>
<author>
<FirstName>Updated for X11R&relvers; by Keith Packard and Kevin E. Martin</FirstName>
</author>
<PubDate>22 May 2006</PubDate>

<Abstract>

<Para>
X.Org has adopted the same basic numbering scheme used by the XFree86
Project, Inc. for their releases.  The actual numbers are different, but the
basic scheme is the same.  This document reflects the policy that X.Org uses.
The version numbering schemes used by XFree86 have changed from time to
time.
</Para>

</Abstract>

</articleinfo>

<Sect1>
<Title>Releases, Development Streams and Branches</Title>

<Para>
As of the release of version X11R6.7 in March 2004, X.Org has three
release branches.  First is the trunk of the CVS repository.  This is
the main development stream, where all new work and work for future
releases is done.
</Para>

<Para>
Second is the stable bugfix branch for the latest full release
(7.1.0).  It is created around the time of the release.  The
branch for the current release is called "<Literal remap="tt">XORG-7_1-branch</Literal>".
Fixes for bugs found in the release will be added to this branch (as
well as the trunk), and updates to this release (if any) will be cut
from this branch.  Similar stable branches are present for previous full
releases.
</Para>

<Para>
The X.Org Foundation is planning to make full releases from the main
development stream at regular intervals in the 6-12 month range.  The
feature freezes for these releases will usually be 2-3 months before the
release dates.  This general plan is a goal, not a binding commitment.
The actual release intervals and dates will depend to a large degree on
the resource available to X.Org.  Full releases consist of full source
code tarballs, plus full binary distributions for a range of supported
platforms.  Update/bugfix releases will be made on an as-required basis,
depending also on the availability of resources, and will generally be
limited to serious bug and security fixes.  New features will not
usually be added in update releases.  Update/bugfix releases will not be
full releases, and will consist of source code patches, plus binary
updates to be layered on top of the previous full release.
</Para>

<Para>
The next full release will be version 7.2.
</Para>

<Para>
Aside from actual releases, snapshots of the active release branches
are tagged in the CVS repository from time to time.  Each such snapshot
has an identifiable version number.
</Para>

</Sect1>

<Sect1>
<Title>Current (new) Version Numbering Scheme</Title>

<Para>
Starting with the main development branch after X11R6.7, the X.Org
versions are numbered according to the scheme outlined here.
</Para>

<Para>
The version numbering format is <Literal remap="tt">M.m.P.s</Literal>, where <Literal remap="tt">M</Literal> is
the major version number, <Literal remap="tt">m</Literal> is the minor version number,
<Literal remap="tt">P</Literal> is the patch level, and <Literal remap="tt">s</Literal> is the snapshot number.
Full releases have <Literal remap="tt">P</Literal> set to zero, and it is incremented for
each subsequent bug fix release on the post-release stable branch.  The
snapshot number <Literal remap="tt">s</Literal> is present only for between-release snapshots
of the development and stable branches.
</Para>

<Sect2>
<Title>Development Branch</Title>

<Para>
Immediately after forming a release stable branch, the patch level
number for the main development branch is bumped to 99, and the snapshot
number is reset.  The snapshot number is incremented for each tagged
development snapshot.  The CVS tag for snapshots is
"<Literal remap="tt">XORG-M&lowbar;m&lowbar;P&lowbar;s</Literal>".  When the development branch enters feature
freeze, the snapshot number is bumped to 900.  A stable branch may be
created for the next full release at any time after the feature freeze.
When it is, the branch is called "<Literal remap="tt">XORG-M&lowbar;m-branch</Literal>".  The
snapshot number is incremented from there until the release is
finalised.  Each of these snapshots is a "release candidate".  When the
release is finalised, the minor version is incremented, the patch level
is set to zero, and the snapshot number removed.
</Para>

<Para>
Here's an example which shows the version number sequence for the
development leading up to version 6.8:
</Para>

<Para>
<VariableList>

<VarListEntry>
<Term><Literal remap="tt">6.7.99.1</Literal></Term>
<ListItem>
<Para>
The first snapshot of the pre-6.8 development branch.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.7.99.23</Literal></Term>
<ListItem>
<Para>
The twenty-third snapshot of the pre-6.8 development branch.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.7.99.900</Literal></Term>
<ListItem>
<Para>
The start of the 6.8 feature freeze.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.7.99.903</Literal></Term>
<ListItem>
<Para>
The third 6.8 release candidate.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.8.0</Literal></Term>
<ListItem>
<Para>
The 6.8 release.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.8.99.1</Literal></Term>
<ListItem>
<Para>
The first pre-6.9 development snapshot, which  is the first main
branch snapshot after creating the 6.8 stable branch.
</Para>
</ListItem>
</VarListEntry>
</VariableList>
</Para>

</Sect2>

<Sect2>
<Title>Stable Branch</Title>

<Para>
After a full release, the stable branch for the release will be
maintained with bug fixes and important updates until the next full
release.  Any snapshots on this branch are considered "release
candidates", which is indicated by setting <Literal remap="tt">s</Literal> to a number above
900.  The snapshot number is incremented for each release candidate
until the update release is finalised.  The patch level value
(<Literal remap="tt">P</Literal>) is incremented for each update release.
</Para>

<Para>
Here's an example which shows a version number sequence for a 6.8.x
stable branch:
</Para>

<Para>
<VariableList>

<VarListEntry>
<Term><Literal remap="tt">6.8.0</Literal></Term>
<ListItem>
<Para>
The 6.8 release.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.8.0.901</Literal></Term>
<ListItem>
<Para>
The first pre 6.8.1 snapshot.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.8.0.903</Literal></Term>
<ListItem>
<Para>
The third pre 6.8.1 snapshot, also known as the third 6.8.1 release
candidate.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.8.1</Literal></Term>
<ListItem>
<Para>
The 6.8.1 release.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.8.1.901</Literal></Term>
<ListItem>
<Para>
The first pre 6.8.2 snapshot.
</Para>
</ListItem>
</VarListEntry>
<VarListEntry>
<Term><Literal remap="tt">6.8.2</Literal></Term>
<ListItem>
<Para>
The 6.8.2 release.
</Para>
</ListItem>
</VarListEntry>
</VariableList>
</Para>

</Sect2>

</Sect1>

<Sect1>
<Title>Finding the X.Org X Server Version From a Client</Title>

<Para>
The X.Org X servers report a <Literal remap="tt">VendorRelease</Literal> value that matches
the X.Org version number.  There have been some cases of releases where
this value wasn't set correctly.  The rules for interpreting this value
as well as the known exceptions are outlined here.
</Para>

<Para>
For post-6.7.0 development and release versions using the new numbering
scheme, the <Literal remap="tt">VendorRelease</Literal> value is <Literal remap="tt">MMmmPPsss</Literal>.  That
is, version <Literal remap="tt">M.m.P.s</Literal> has <Literal remap="tt">VendorRelease</Literal> set to
<Literal remap="tt">M&nbsp;*&nbsp;10000000&nbsp;+&nbsp;m&nbsp;*&nbsp;100000&nbsp;+&nbsp;P&nbsp;*&nbsp;1000&nbsp;+&nbsp;s</Literal>.
</Para>

<Para>
The following is a code fragment taken from <Literal remap="tt">xdpyinfo.c</Literal> that shows
how the <Literal remap="tt">VendorRelease</Literal> information can be interpreted.
</Para>

<Para>

<Screen>

    if (strstr(ServerVendor(dpy), "X.Org")) {
        int vendrel = VendorRelease(dpy);

        printf("X.Org version: ");
	printf("%d.%d.%d", vendrel / 10000000,
	       (vendrel /   100000) % 100,
	       (vendrel /     1000) % 100);
	if (vendrel % 1000) {
	    printf(".%d", vendrel % 1000);
	}
    }
</Screen>

</Para>

</Sect1>

</Article>
