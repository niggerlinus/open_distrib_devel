<!DOCTYPE article PUBLIC "-//OASIS//DTD DocBook V4.2//EN" [
<!ENTITY % defs SYSTEM "X11/defs.ent"> %defs;
]>

<Article>

<articleinfo>

<Title>X and DPS</Title>
<AUTHOR
>
<firstname>Juliusz</firstname><surname>Chroboczek</surname>
<email>jch@freedesktop.org</email>
</AUTHOR>
<PubDate>27 February 2001, updated 30 October 2006</PubDate>

</articleinfo>

<Para>
Updated by Jim Gettys and Juliusz Chroboczek.
</Para>

<Para>

</Para>

<Para>

</Para>

<Sect1>
<Title>Notice of Obsolescence and Removal from X.Org Releases</Title>

<Para>
DPS is now obsolete.
</Para>

<Para>
At the time when I started this project, there was no decent rendering
interface for X11 other than DPS.
</Para>

<Para>
Since then, there has been a large amount of work on a simple and
clean X server extension, Xrender, which provides the basis for just
such an interface.
</Para>

<Para>
Rendering libraries that are being built above Xrender include Xft, a
font rendering library built on FreeType; Cairo, a geometry rendering
library that provides PostScript-like rendering primitives but with
from-the-ground support for Porter-Duff compositing (transparency);
Pango, a high-level typesetting library.
</Para>

<Para>
If your application uses DPS, please consider porting it to the above libraries.
See <ULink
URL="http://dps.sourceforge.net"
>the DPS extension site</ULink
>
for more details
</Para>

<Para>
The client-side DPS software is included and built by default (except
as noted below) in the X.Org X11R6.8 release series.  The client-side
software will be included, but not built unless specifically
configured by the builder, in the X.Org X11R6.9 release series.  Most
of the client-side DPS software is no longer included in X11R7.0 or
later releases.
</Para>

<Para>
The server-side software is not included in any X.Org release for
licensing reasons; the code is still available
from <ULink
URL="http://dps.sourceforge.net"
>the DPS extension site</ULink
>; it is not known whether it still compiles.
</Para>

</Sect1>

<Sect1>
<Title>Introduction</Title>

<Para>
Display Postscript (or DPS for short) is a rendering extension for
X11.  DPS is slightly atypical in that it is based on <Emphasis remap="it">code
mobility</Emphasis>, the ability to send executable code from client to server.
Thus, a DPS client does not request that a line should be rendered;
instead, it uploads code to the server which, when executed, causes a
line to be drawn.
</Para>

<Para>
This document does not aim at teaching programming with DPS; it is
only a summary description of the DPS support code included with
X11R.  More information about DPS, including a DPS bibliography, is
available from
<ULink
URL="http://dps.sourceforge.net"
>the DPS extension site</ULink
>.
</Para>

<Para>

<Screen>
<Emphasis remap="it">Note: Adobe, PostScript and Display PostScript are trademarks of
Adobe Systems Incorporated which may be registered in certain
jurisdictions.</Emphasis>
</Screen>

</Para>

<Para>
As all X11 extensions, DPS consists of client-side and server-side
components.  The DPS client side consists of a number of libraries and
a number of tools for programming and configuration.  The DPS server
side may consist either of an X server extension, or of a client-side
process known as the ``DPS agent.''  In this latter case, the term
``server-side'' is somewhat misleading.
</Para>

<Para>
At the time of writing, only the client side is included with X11R;
the server side must be obtained separately.  Please see <XRef LinkEnd="sec-server-side"> later in this document for
more information.
</Para>

</Sect1>

<Sect1>
<Title>The DPS client side</Title>

<Para>
The DPS client side consists of four libraries and a number of
basic tools for programming and configuration; these are all included
with X11R.
</Para>

<Sect2>
<Title>Libraries</Title>

<Para>
The <Literal remap="tt">libpsres</Literal> library is a library for management of
<Emphasis remap="it">PostScript resources</Emphasis>, on-disk files representing PostScript data
structures such as fonts, font encodings, procsets, <Emphasis remap="it">etc.</Emphasis> It is
closely related to the <Literal remap="tt">makepsres</Literal> tool (see <XRef LinkEnd="sec-tools"> later in this document).
</Para>

<Para>
The basic DPS client library is <Literal remap="tt">libdps</Literal>.  This library contains a
number of functions for connection establishment, resource management,
as well as stubs for all standard PostScript operators.  Normally, all
DPS clients should link with <Literal remap="tt">libdps</Literal>; in addition, <Literal remap="tt">libdps</Literal> may
be used for printing by non-DPS clients (this is done, for example, by
Sun's JDK).  This library is documented in &lsqb;CLRM] and &lsqb;CLSX].
</Para>

<Para>
The <Literal remap="tt">libdpstk</Literal> library contains a number of additional utilities
for managing DPS contexts, user paths and user objects, and for
previewing EPS files.  It is documented in &lsqb;DPTX].
</Para>

<Para>
The <Literal remap="tt">libdpstkXm</Literal> library contains four Motif widgets.  The <Emphasis remap="it">DPS
Scrolling Widget</Emphasis> is a DPS drawing area that automatically manages
issues such as scrolling, scaling, client-side backing store,
incremental redisplay, <Emphasis remap="it">etc.</Emphasis> The <Emphasis remap="it">Font Selection Box</Emphasis>, and its
associated <Emphasis remap="it">Font Preview</Emphasis>, present a convenient and powerful
interface for choosing scalable fonts.  Finally, the <Emphasis remap="it">Color Picker</Emphasis>
presents an interface for choosing colours using either of the RGB or
HSV spaces.  The latter three widgets are documented in &lsqb;DPTX]; some
summary Scrolling Widget documentation is available in the <Literal remap="tt">doc</Literal>
subdirectory of the <Literal remap="tt">DPS.tar.gz</Literal> file, available from <ULink
URL="ftp://dps.sourceforge.net/pub/dps/DPS.tar.gz"
>ftp://dps.sourceforge.net/pub/dps/DPS.tar.gz</ULink
>.
</Para>

<Para>
The source code for <Literal remap="tt">libdpstkXm</Literal> is included with X11R; however,
as it depends on Motif, this library is not built by default.
A GTK-based library providing some of the functionality of
<Literal remap="tt">libdpstkXm</Literal> is available from
<ULink
URL="http://www.gyve.org/gtkDPS/"
>the gtkDPS site</ULink
>.
</Para>

<Sect3>
<Title>Libdps and Xt</Title>

<Para>
In X11R5, <Literal remap="tt">libdps</Literal> did not depend on <Literal remap="tt">libXt</Literal>.  In X11R6,
however, code was added to make the Xt main loop dispatch to sundry
code on DPS events; with this addition, all programs that link with
<Literal remap="tt">libdps</Literal> need to link with <Literal remap="tt">libXt</Literal>, whether they use Xt or not.
</Para>

<Para>
This state of affairs is unfortunately true of the version of
<Literal remap="tt">libdps</Literal> included with X11R.  We are currently considering
various solutions to this problem (including the use of weak linker
symbols or splitting off the guilty functions into a separate
library).
</Para>

</Sect3>

</Sect2>

<Sect2 id="sec-tools" xreflabel="client-side tools">
<Title>Client-side tools </Title>

<Para>
In addition to the libraries, the client side of DPS consists of two
utilities.
</Para>

<Para>
The <Literal remap="tt">makepsres</Literal> utility is used for managing PostScript resources.
Its basic operation consists in walking recursively a filesystem tree,
noting all resources, and then writing out a ``Unix PostScript
Resources,'' file, basically a directory of all the resources found.
This utility is documented in the <ULink
URL="makepsres.1.html"
>makepsres(1)</ULink
> manual page.
</Para>

<Para>
The <Literal remap="tt">pswrap</Literal> utility is a stub generator for PostScript clients.
Roughly speaking, it takes as its input textual PostScript code, and
generates a collection of C functions that transmit that code in
pre-tokenised form to the DPS extension.  <Literal remap="tt">Pswrap</Literal> is documented in
&lsqb;PSWRAP].
</Para>

</Sect2>

<Sect2>
<Title>Sample clients</Title>

<Para>
X11R contains three sample DPS clients, <Literal remap="tt">dpsinfo</Literal>,
<Literal remap="tt">dpsexec</Literal> and <Literal remap="tt">texteroids</Literal>.  They are documented in their
respective manual pages.
</Para>

<Para>
A number of sample clients that depend on Motif are available in <ULink
URL="ftp://dps.sourceforge.net/pub/ftp/DPS.tar.gz"
>ftp://dps.sourceforge.net/pub/ftp/DPS.tar.gz</ULink
>.  Additional
sample clients can be found as part of GtkDPS (see above).
</Para>

<Para>
The GNUstep environment can be compiled to use DPS for
all rendering; for more information, please see
<ULink
URL="http://www.gnustep.org"
>the GNUstep site</ULink
>.
</Para>

</Sect2>

</Sect1>

<Sect1 id="sec-server-side" xreflabel="server side">
<Title>The DPS server side </Title>

<Para>
In order to use DPS clients, you need to install a DPS server side,
which can be either a server extension (a ``DPS/X extension''), or a
separate process (referred to, variously, either as a ``DPS/NX agent''
or, rather confusingly, as ``Client-Side DPS'' (CSDPS).
</Para>

<Sect2>
<Title>Display Ghostscript</Title>

<Para>
Display Ghostscript (note the capitalisation), or DGS, is a
client-side implementation of DPS based on the Ghostscript
PostScript interpreter.  DGS is still in beta at the time of writing;
it does, however, provide a very usable implementation of DPS,
although it still has some problems with the semantics of multiple DPS
contexts.
</Para>

<Para>
DGS is available from
<ULink
URL="http://www.gnustep.org/resources/sources.html"
>the GNUstep download area</ULink
>.
</Para>

</Sect2>

<Sect2>
<Title>The DPS extension</Title>

<Para>
The DPS extension is a much younger project aiming at producing an
efficient server-side implementation of DPS.  The extension is
currently in a state best described as alpha; current versions are
known to crash the X server under some circumstances.
</Para>

<Para>
The DPS extension is available from
<ULink
URL="http://dps.sourceforge.net"
>the DPS extension site</ULink
>.
</Para>

</Sect2>

</Sect1>

<Sect1>
<Title>References</Title>

<Para>
Links to electronic versions of all of these, and more, are
available from <ULink
URL="http://dps.sourceforge.net"
>the DPS extension site</ULink
>.
</Para>

<Para>
&lsqb;PLRM2] PostScript language reference manual. Adobe Systems, 2nd ed. Addison-Wesley, 1990. ISBN 0-201-18127-4.
</Para>

<Para>
&lsqb;PLRM] PostScript language reference. Adobe Systems Incorporated, 3rd
ed. Addison-Wesley, 1999. ISBN 0-201-37922-8.
</Para>

<Para>
&lsqb;INTRO] Display PostScript System. Introduction: Perspective for
Software Developers. 15 April 1993.
</Para>

<Para>
&lsqb;CLRM] Display PostScript System. Client Library Reference Manual. 15
April 1993.
</Para>

<Para>
&lsqb;CLSX] Display PostScript System. Client Library Supplement for X. 15
April 1993.
</Para>

<Para>
&lsqb;DPTX] Display PostScript System. Display PostScript Toolkit for X. 15
April 1993.
</Para>

<Para>
&lsqb;PSWRAP] Display PostScript System. pswrap Reference Manual. 15 April
1993.
</Para>

</Sect1>

</Article>
